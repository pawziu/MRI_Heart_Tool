#import "CellState.h"

@implementation CellState

@synthesize LVEPIcontour;
@synthesize LVENDOcontour;
@synthesize RVEPIcontour;
@synthesize RVENDOcontour;
@synthesize pointSet;
@synthesize sectorsCreated;

//init with default settings
- (id) init{
    if (self = [super init]){
        LVEPIcontour = empty;
        LVENDOcontour = empty;
        RVEPIcontour = empty;
        RVENDOcontour = empty;
        pointSet = NO;
        sectorsCreated = NO;
    }
    return self;
}

- (NSString*) generateCellStateString{
    
    NSString *generatedString = @"";
    
    NSString *addedString = @"LVEPI:";
    switch (LVEPIcontour){
        case empty:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"e "]];
            break;
        case manual:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"m "]];
            break;
        case automatic:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"a "]];
            break;
        default:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"ERR "]];
            break;
    }
    
    addedString = @"LVENDO:";
    switch (LVENDOcontour){
        case empty:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"e "]];
            break;
        case manual:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"m "]];
            break;
        case automatic:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"a "]];
            break;
        default:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"ERR "]];
            break;
    }
    
    addedString = @"RVEPI:";
    switch (RVEPIcontour){
        case empty:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"e "]];
            break;
        case manual:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"m "]];
            break;
        case automatic:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"a "]];
            break;
        default:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"ERR "]];
            break;
    }
    
    addedString = @"RVENDO:";
    switch (RVENDOcontour){
        case empty:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"e "]];
            break;
        case manual:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"m "]];
            break;
        case automatic:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"a "]];
            break;
        default:
            generatedString = [generatedString stringByAppendingString:[addedString stringByAppendingString:@"ERR "]];
            break;
    }
    
    if (pointSet == YES){
        addedString = @"pointSet:y ";
        generatedString = [generatedString stringByAppendingString:addedString];
    }
    else{
        addedString = @"pointSet:n ";
        generatedString = [generatedString stringByAppendingString:addedString];
    }
    
    if (sectorsCreated == YES){
        addedString = @"sectorsCreated:y";
        generatedString = [generatedString stringByAppendingString:addedString];
    }
    else{
        addedString = @"sectorsCreated:n";
        generatedString = [generatedString stringByAppendingString:addedString];
    }
    
    return generatedString;
}



@end
