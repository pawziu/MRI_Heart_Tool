
#import <OptionWindowController.h>

@interface OptionWindowController ()


- (IBAction)regionGrowingStart:(id)sender;
- (IBAction)edgeDetectionStart:(id)sender;
- (IBAction)morphoClose:(id)sender;
- (IBAction)holeFilling:(id)sender;
- (IBAction)convertITKToBrush:(id)sender;
- (IBAction)convexHullStart:(id)sender;
- (IBAction)finalOptimalization:(id)sender;




@end


@implementation OptionWindowController

@synthesize windowVisible;
@synthesize operationHandler;
@synthesize itkHandler;


- (id) initWithWindow:(NSWindow *)window{
    self = [super initWithWindow:window];
    if (self){
        windowVisible = YES;
    }
    return self;
}

- (void)windowDidLoad {
    
    [super windowDidLoad];
    windowVisible = YES;

}

- (void) additionalInit{
    itkHandler.regionMultiplier = regionMultiplier;
    itkHandler.regionIterations = regionIterations;
    itkHandler.regionInitialRadius = regionInitialRadius;
    
    itkHandler.edgeTimeStep = edgeTimeStep;
    itkHandler.edgeIterations = edgeIterations;
    itkHandler.edgeConductance = edgeConductance;
    itkHandler.edgeSigma = edgeSigma;
    itkHandler.edgeAlpha = edgeAlpha;
    itkHandler.edgeBeta = edgeBeta;
    itkHandler.edgeStoppingValue = edgeStoppingValue;
    itkHandler.edgePropagationScalling = edgePropagationScalling;
    itkHandler.edgeCurvatuveScalling = edgeCurvatuveScalling;
    
    itkHandler.regionMultiplier.doubleValue = 3.0;
    itkHandler.regionIterations.integerValue = 1;
    itkHandler.regionInitialRadius.integerValue = 4;
    
    itkHandler.edgeTimeStep.doubleValue = 0.125;
    itkHandler.edgeIterations.integerValue = 5;
    itkHandler.edgeConductance.doubleValue = 9.0;
    itkHandler.edgeSigma.doubleValue = 1.0;
    itkHandler.edgeAlpha.doubleValue = -0.5;
    itkHandler.edgeBeta.doubleValue = 3.0;
    
    operationHandler.slicesFrom = slicesFrom;
    operationHandler.slicesTo = slicesTo;
    operationHandler.framesFrom = framesFrom;
    operationHandler.framesTo = framesTo;
    
    operationHandler.resultROIName = resultROIName;
    operationHandler.roiHandler.resultROIName = resultROIName;
    operationHandler.groundTruthROIName = groundTruthROIName;
    
    operationHandler.regionGrowingOptimalization = regionGrowingOptimalization;
    operationHandler.edgeDetectionEnable = edgeDetectionEnable;
    operationHandler.convexHullEnable = convexHullEnable;
    operationHandler.finalOptimalizationEnable = finalOptimalizationEnable;
    
    operationHandler.regionGrowingOptimalization.state = NSControlStateValueOn;
    operationHandler.edgeDetectionEnable.state = NSControlStateValueOn;
    operationHandler.convexHullEnable.state = NSControlStateValueOn;
    operationHandler.finalOptimalizationEnable.state = NSControlStateValueOn;
    
    [operationHandler additionalInit];
    
}

- (void) windowWillClose:(NSNotification *)notification{
    windowVisible = NO;
}


- (IBAction)regionGrowingStart:(id)sender {
    [operationHandler semiAutoSeg:regionMultiplier.doubleValue];
}

- (IBAction)edgeDetectionStart:(id)sender {
    [operationHandler shapeDetectionLevelSetFilter];
}

- (IBAction)morphoClose:(id)sender {
    [operationHandler morphoClose];
}

- (IBAction)holeFilling:(id)sender {
    [operationHandler holeFilling];
}

- (IBAction)convertITKToBrush:(id)sender {
    [operationHandler createBrushROIFromITKImages:0];
}

- (IBAction)convexHullStart:(id)sender {
    [operationHandler performConvexHull];
}

- (IBAction)finalOptimalization:(id)sender {
    [operationHandler finalOptimalization];
}
@end



