#import "Slice.h"

@interface Slice ()

- (void) addObject;


- (NSImage *) createEmptyImageWithSize: (NSSize) size;
- (NSImage *) createImageWithSize: (NSSize) size backgroundColor: (NSColor *) color title: (NSString *) title;
- (NSImage *) createImageFromCellState: (CellState*) cellState;
- (NSImage *) combineSixImages: (NSMutableArray *) imagesArray size: (NSSize) size;


@end

@implementation Slice

@synthesize framesNumber;
@synthesize thisSliceNumber;
@synthesize sliceContents;
@synthesize imageSize;

- (id) init{
    self = [super init];
    if (self){
        thisSliceNumber = 3;
    }
    return self;
}


- (id) initWithFramesNumber: (int) frames numberOfSlice: (int) sliceNumber{
    self = [super init];
    if (self){
        framesNumber = frames;
        thisSliceNumber = sliceNumber;
        sliceContents = [[NSMutableArray alloc] init];
        for (int i = 0; i < framesNumber; i++){
            [self addObject];
        }
    }
    return self;
}

- (NSImage *) createEmptyImageWithSize: (NSSize) size{
    
    NSImage *newImage = [[NSImage alloc] initWithSize:NSMakeSize(size.width, size.height)];
    [newImage lockFocus];
    [[NSColor colorWithCalibratedRed:0.5 green:0.5 blue:0.5 alpha:1] drawSwatchInRect:NSMakeRect(0, 0, size.width, size.height)];
    [newImage unlockFocus];
    
    return newImage;
}

- (NSImage *) createImageWithSize: (NSSize) size backgroundColor: (NSColor *) color title: (NSString *) title{
    
    NSImage *newImage = [[NSImage alloc] initWithSize:NSMakeSize(size.width, size.height)];
    [newImage lockFocus];
    [color drawSwatchInRect:NSMakeRect(0, 0, size.width, size.height)];
    [newImage unlockFocus];
    
    NSImage *textImage = [NSImage imageWithSize:newImage.size flipped:YES drawingHandler:^BOOL(NSRect dstRect) {
        [newImage drawInRect:dstRect];
        
        NSDictionary *attributes = @{NSFontAttributeName: [NSFont systemFontOfSize:8], NSForegroundColorAttributeName: [NSColor whiteColor]};
        [title drawAtPoint:NSMakePoint(0, 0) withAttributes:attributes];
        
        return YES;
    }];
    
    return textImage;
}

- (NSImage *) createImageFromCellState: (CellState*) cellState{
    
    NSString *cellStateString = [cellState generateCellStateString];
    NSMutableArray *imagesArray = [[NSMutableArray alloc] init];
    
    if ([cellStateString containsString:@"ERR"]){
        NSImage *generatedImage = [self createImageWithSize:NSMakeSize(15, 10) backgroundColor:[NSColor darkGrayColor] title:@"ERR"];
        return generatedImage;
    }
    
    NSArray *searchedString = [[NSArray alloc] initWithObjects:@"LVEPI:",@"LVENDO:",@"RVEPI:",@"RVENDO:",@"pointSet:",@"sectorsCreated:", nil];
    
    NSArray *colorArray = [[NSArray alloc] initWithObjects:
                           [NSColor colorWithCalibratedRed:0 green:0.5 blue:0 alpha:1],
                           [NSColor colorWithCalibratedRed:0.5 green:0 blue:0 alpha:1],
                           [NSColor colorWithCalibratedRed:1 green:0.5 blue:0 alpha:1],
                           [NSColor colorWithCalibratedRed:0 green:0 blue:0.8 alpha:1],
                           [NSColor colorWithCalibratedRed:0.5 green:0.5 blue:0.5 alpha:1],
                           [NSColor colorWithCalibratedRed:0.3 green:0.3 blue:0.3 alpha:1],
                           nil];
    
    for (int i = 0; i<6; i++){
        
        if ([cellStateString containsString:[searchedString objectAtIndex:i]]){
            
            if (i < 4){
                
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"e"]]){
                    NSImage *createdImage = [self createEmptyImageWithSize:NSMakeSize(15, 10)];
                    [imagesArray addObject:createdImage];
                }
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"a"]]){
                    NSImage *createdImage = [self createImageWithSize:NSMakeSize(15, 10) backgroundColor:[colorArray objectAtIndex:i] title:@"A"];
                    [imagesArray addObject:createdImage];
                }
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"m"]]){
                    NSImage *createdImage = [self createImageWithSize:NSMakeSize(15, 10) backgroundColor:[colorArray objectAtIndex:i] title:@"M"];
                    [imagesArray addObject:createdImage];
                }
            }
            else if (i == 4){
                
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"n"]]){
                    NSImage *createdImage = [self createEmptyImageWithSize:NSMakeSize(15, 10)];
                    [imagesArray addObject:createdImage];
                }
                
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"y"]]){
                    NSImage *createdImage = [self createImageWithSize:NSMakeSize(15, 10) backgroundColor:[colorArray objectAtIndex:i] title:@"P"];
                    [imagesArray addObject:createdImage];
                }
                
            }
            else{
                
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"n"]]){
                    NSImage *createdImage = [self createEmptyImageWithSize:NSMakeSize(15, 10)];
                    [imagesArray addObject:createdImage];
                }
                
                if ([cellStateString containsString:[[searchedString objectAtIndex:i] stringByAppendingString:@"y"]]){
                    NSImage *createdImage = [self createImageWithSize:NSMakeSize(15, 10) backgroundColor:[colorArray objectAtIndex:i] title:@"S"];
                    [imagesArray addObject:createdImage];
                }
            }
            
        }
    }
    
    NSImage *outputImage = [self combineSixImages:imagesArray size:NSMakeSize(30, 30)];
    
    return outputImage;
    
}

- (NSImage *) combineSixImages: (NSMutableArray *) imagesArray size: (NSSize) size{
    
    NSImage *combinedImage = [[NSImage alloc] initWithSize:size];
    
    if ([imagesArray count] != 6){
        return nil;
    }
    
    //CGRect firstImageRect = CGRectZero;
    
    for (int curImageNumber = 0; curImageNumber < [imagesArray count]; curImageNumber++){
        
        NSImage *curImage  = [imagesArray objectAtIndex:curImageNumber];
        
        //NSSize test;
        
        [combinedImage lockFocus];
        
        CGRect newImageRect = CGRectZero;
        newImageRect.size.height = curImage.size.height;
        newImageRect.size.width = curImage.size.width;
        
        switch (curImageNumber) {
                
            case 0:
                newImageRect.origin.x = 0;
                newImageRect.origin.y = (size.height / 3) * 2;
                break;
                
                //top right
            case 1:
                newImageRect.origin.x = size.width / 2;
                newImageRect.origin.y = (size.height / 3) * 2;
                break;
                
                // center left
            case 2:
                newImageRect.origin.x = 0;
                newImageRect.origin.y = size.height / 3;
                break;
                
                // center right
            case 3:
                newImageRect.origin.x = size.width / 2;
                newImageRect.origin.y = size.height / 3;
                break;
                
                // bottom left
            case 4:
                newImageRect.origin.x = 0;
                newImageRect.origin.y = 0;
                break;
                
                // bottom right
            case 5:
                newImageRect.origin.x = size.width / 2;
                newImageRect.origin.y = 0;
                break;
                
                
            default:
                break;
        }
        
        [curImage drawInRect:NSRectFromCGRect(newImageRect)];
        
        [combinedImage unlockFocus];
        
    }
    
    return combinedImage;
}

- (void) addObject{
    
    // SAMPLE DATA
    
    NSSize small = NSMakeSize(15, 10);

    NSMutableArray *testArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i <6; i++){
        NSImage *image = [self createEmptyImageWithSize:small];
        [testArray addObject:image];
    }
    
    NSImage *combinedImage = [self combineSixImages:testArray size:NSMakeSize(30, 30)];
    
    [sliceContents addObject:combinedImage];
}

- (void) setObject: (int) position cellState: (CellState*) cellState{
    
    NSImage *generatedImage = [self createImageFromCellState:cellState];
    
    [sliceContents replaceObjectAtIndex:position withObject:generatedImage];
    
}

@end
