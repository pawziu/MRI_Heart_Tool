#import <Foundation/Foundation.h>
#import <OsiriXAPI/PluginFilter.h>
#import <OsiriXAPI/DicomStudy.h>
#import <OsiriXAPI/DicomSeries.h>
#import <OsiriXAPI/DicomImage.h>
#import "DCMObject.h"
#import "DCMAttribute.h"
#import "DCMAttributeTag.h"
#import "CellState.h"

@interface ROIClass : NSObject{
    
    ROI *roi;
    int sliceNumber;
    int frameNumber;
    
}

@property (nonatomic, assign) ROI *roi;
@property (nonatomic, assign) int sliceNumber;
@property (nonatomic, assign) int frameNumber;

@end
