

#import "MainWindowController.h"

@interface MainWindowController ()

- (IBAction)myDrawROI:(id)sender;
- (IBAction)calcVolumes:(id)sender;
- (IBAction)autoSetESED:(id)sender;

- (IBAction)showTable:(id)sender;
- (void) fillTable;

- (IBAction)approximate:(id)sender;

- (IBAction)setPointForSegments:(id)sender;
- (IBAction)createSectors:(id)sender;

- (IBAction)createPlot:(id)sender;
- (void)updatePlot;

- (IBAction)deletePointROIs:(id)sender;
- (IBAction)deleteBrushROIs:(id)sender;
- (IBAction)deletePolygonROIs:(id)sender;
- (IBAction)deleteAllROIs:(id)sender;


- (IBAction)deleteApproximationROIs:(id)sender;

- (IBAction)importButton:(id)sender;
- (IBAction)exportButton:(id)sender;
- (IBAction)importTextButton:(id)sender;

- (IBAction)semiAutoSegmentation:(id)sender;

- (IBAction)setSegPoint:(id)sender;
- (IBAction)morphoClose:(id)sender;
- (IBAction)holeFilling:(id)sender;

- (IBAction)semiAutoSeg:(id)sender;
- (IBAction)shapeDetectionLevelSetFilter:(id)sender;

- (IBAction)countDice:(id)sender;
- (IBAction)showOptions:(id)sender;

@end

@implementation MainWindowController

@synthesize viewerController;
@synthesize viewerControllerSeg;
@synthesize operationHandler;

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (void) additionalInit{
    
    [viewerController setROIToolTag:tCPolygon];
    
    //operationHandler = [[OperationHandler alloc] initWithViewerController:viewerController];
    operationHandler = [[OperationHandler alloc] initWithViewerController:viewerController vcs:viewerControllerSeg];
    
    operationHandler.ED = ED;
    operationHandler.ES = ES;
    operationHandler.BasalFrom = BasalFrom;
    operationHandler.BasalTo = BasalTo;
    operationHandler.MidCavityFrom = MidCavityFrom;
    operationHandler.MidCavityTo = MidCavityTo;
    operationHandler.ApicalFrom = ApicalFrom;
    operationHandler.ApicalTo = ApicalTo;
    operationHandler.BSAField = BSAField;
    operationHandler.LVparams = LVparams;
    operationHandler.LVBSAparams = LVBSAparams;
    operationHandler.RVparams = RVparams;
    operationHandler.RVBSAparams = RVBSAparams;
    operationHandler.SegmentationMultiplier = SegmentationMultiplier;
    operationHandler.SegmentationFirstSlice = SegmentationFirstSlice;
    
    [operationHandler autoSetSegmentationLevels];
    [operationHandler autoSetESED];
    
    [operationHandler setMainWindow:self.window];
      
    NSString *BSAstring = [NSString stringWithFormat:@"%.2f", [operationHandler calcBSA]];
    [BSAField setStringValue: BSAstring];
    // get normal vectors,
    [LVparams setStringValue:@"LV:\n0\n0\n0\n0\n0\n"];
    [LVBSAparams setStringValue:@"NLV:\n0\n0\n0\n0\n0\n"];
    [RVparams setStringValue:@"RV:\n0\n0\n0\n0\n0\n"];
    [RVBSAparams setStringValue:@"NRV:\n0\n0\n0\n0\n0\n"];
    
    [approximationAlgorythm addItemWithObjectValue:@"ES->ED"];
    [approximationAlgorythm addItemWithObjectValue:@"ED->ES"];
    //[approximationAlgorythm addItemWithObjectValue:@"Combined"];
    
    [approximationAlgorythm setSelectable:NO];
    [approximationAlgorythm setEditable:NO];
    
    [approximationAlgorythm selectItemWithObjectValue:@"ES->ED"];
    
    operationHandler.SegmentationMultiplier.doubleValue = 3.0;
    operationHandler.SegmentationFirstSlice.intValue = 4;
    
    [self showOptions:NULL];
    
}

- (IBAction)myDrawROI:(id)sender {
    [operationHandler drawROI:sender];
    [self fillTable];
}

- (IBAction)calcVolumes:(id)sender {
    [operationHandler calcVolumes];
}

- (IBAction)autoSetESED:(id)sender {
    [operationHandler autoSetESED];
    [operationHandler autoSetSegmentationLevels];
}

- (IBAction)showTable:(id)sender {
    
    int framesNumber = [operationHandler getFramesCount];
    
    if (table.windowVisible == NO || table == nil){
        if (table){
            table = nil;
        }
        table = [[TableController alloc] initWithWindowNibName:@"TableController"];
        [table showWindow:self];
        
        for (int i = 0; i < framesNumber; i++){
            
            NSString *newColumnName = @"Frame";
            newColumnName = [newColumnName stringByAppendingFormat:@"%d",i+1];
            [table addColumn:newColumnName];
            
        }
        
        for (int j = 1; j <= [operationHandler getSlicesCount]; j++){
            
            [table addRow:framesNumber numberOfThisSlice:j];
            
        }
        
        table.operationHandler = operationHandler;
        
    }
    
    [self fillTable];
    
    // here the table view should resize
    
    
}

- (void) fillTable{
    
    if (table == NULL){
        return;
    }
    
    for (int curSlice = 0; curSlice < [operationHandler getSlicesCount]; curSlice++){
        for (int curFrame = 0; curFrame < [operationHandler getFramesCount]; curFrame++){
            CellState *newCellState = [operationHandler.roiHandler getCellState:curSlice frame:curFrame];
            [table setCellsState:curFrame sliceNumber:curSlice cellsState:newCellState];
        }
    }
    [table._table reloadData];
}

- (IBAction)approximate:(id)sender {
    [operationHandler approximate: approximationAlgorythm copyButton:approximationCopy];
    [self fillTable];
}

- (IBAction)setPointForSegments:(id)sender {
    [operationHandler setPointForSegments];
    [self fillTable];
}

- (IBAction)createSectors:(id)sender {
    [operationHandler createSectorsNew];
    [self fillTable];
}

- (IBAction)createPlot:(id)sender {
    
    if (plot.windowVisible == NO || plot == nil){
        if (plot){
            plot = nil;
        }
        plot = [[MFPController alloc] initWithWindowNibName:@"MFPController"];
        [plot showWindow:self];
    }
    [self updatePlot];
}

- (void)updatePlot {
    
    if (plot == nil){
        return;
    }
    
    
    int curFrameConst = [viewerController imageIndex];
    [operationHandler fillPlotVariables];
    
    //second time, because the first sets wrong max sector mean 
    [operationHandler fillPlotVariables];

    plot.numberOfTimeFrames = [operationHandler getFramesCount];
    plot.maxValue = operationHandler.maxSectorMean;
    plot.numberOfSectors = operationHandler.numberOfSectors;
    [plot setPlotPoints:operationHandler.sectorNewArray];
    
    [plot updateGraph];
    
    [operationHandler changeFrame:curFrameConst];
}

- (IBAction)deletePointROIs:(id)sender {
    [operationHandler deletePointROIs];
    [self fillTable];
}

- (IBAction)deleteBrushROIs:(id)sender {
    [operationHandler deleteAllBrushROIs];
    [self fillTable];
}

- (IBAction)deletePolygonROIs:(id)sender {
    [operationHandler deleteAlltcPolygonROIs];
    [self fillTable];
}

- (IBAction)deleteAllROIs:(id)sender {
    [operationHandler deletePointROIs];
    [operationHandler deleteAllBrushROIs];
    [operationHandler deleteAlltcPolygonROIs];
    [self fillTable];
}

- (IBAction)deleteApproximationROIs:(id)sender {
    [operationHandler deleteApproximationROIs];
    [self fillTable];
}

- (IBAction)importButton:(id)sender {
    [operationHandler importQmass];
}

- (IBAction)exportButton:(id)sender {
    [operationHandler exportQmass];
}

- (IBAction)importTextButton:(id)sender {
    [operationHandler importTextROI];
}

- (IBAction)semiAutoSegmentation:(id)sender {
    [operationHandler semiAutoSeg];
}

- (IBAction)setSegPoint:(id)sender {
    [operationHandler setSegPoint];
}

- (IBAction)morphoClose:(id)sender {
    [operationHandler morphoClose];
}

- (IBAction)holeFilling:(id)sender {
    [operationHandler holeFilling];
}

- (IBAction)semiAutoSeg:(id)sender {
    
    id waitWindow = [viewerController startWaitWindow:@"Working..."];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [operationHandler fullSemiAuto];
        });
    });
    [viewerController endWaitWindow: waitWindow];

}

- (IBAction)shapeDetectionLevelSetFilter:(id)sender {
    [operationHandler shapeDetectionLevelSetFilter];
}

- (IBAction)countDice:(id)sender {
    [operationHandler countDice];
}

- (IBAction)showOptions:(id)sender {
    if (options.windowVisible == NO || table == nil){
        if (options){
            options = nil;
        }
        options = [[OptionWindowController alloc] initWithWindowNibName:@"OptionWindow"];
        [options showWindow:self];

        options.operationHandler = operationHandler;
        options.itkHandler = operationHandler.itkHandler;
        
        [options additionalInit];
    }
    
    
}
- (IBAction)itkToBrushRoi:(id)sender {
    [operationHandler createBrushROIFromITKImages:0];
}


@end
