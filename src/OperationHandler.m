#import "OperationHandler.h"

@implementation OperationHandler

@synthesize viewerController;
@synthesize viewerControllerSeg;
@synthesize roiHandler;
@synthesize mathHandler;
@synthesize mainWindow;
@synthesize itkHandler;

@synthesize ED;
@synthesize ES;
@synthesize BasalFrom;
@synthesize BasalTo;
@synthesize MidCavityFrom;
@synthesize MidCavityTo;
@synthesize ApicalFrom;
@synthesize ApicalTo;
@synthesize LVparams;
@synthesize LVBSAparams;
@synthesize RVparams;
@synthesize RVBSAparams;
@synthesize BSAField;

@synthesize sectorNewArray;
@synthesize maxSectorMean;
@synthesize numberOfSectors;

@synthesize segStartPoint;
@synthesize SegmentationMultiplier;
@synthesize SegmentationFirstSlice;

@synthesize slicesFrom;
@synthesize slicesTo;
@synthesize framesFrom;
@synthesize framesTo;

@synthesize resultROIName;
@synthesize groundTruthROIName;

@synthesize regionGrowingOptimalization;
@synthesize edgeDetectionEnable;
@synthesize convexHullEnable;
@synthesize finalOptimalizationEnable;


- (id) initWithViewerController: (ViewerController*) pointer vcs:(ViewerController*) pointer2{
    
    if (self = [super init]){
        
        viewerController = pointer;
        viewerControllerSeg = pointer2;
        mathHandler = [[MathHandler alloc] init];
        //itkHandler = [[ITKHandler alloc] initWithViewerController:viewerController mathHandler:mathHandler];
        itkHandler = [[ITKHandler alloc] initWithViewerController:viewerController mathHandler:mathHandler vcs:viewerControllerSeg];
        
        roiHandler = [[ROIHandler alloc] initWithViewerController:viewerController mathHandler:mathHandler];
        sectorNewArray =(double*)malloc(6*[self getFramesCount]*sizeof(double));
        segStartPoint.x = 0;
        segStartPoint.y = 0;
        

        
    }
    return self;
}

- (void) additionalInit{
    
    
    resultROIName.stringValue = @"SEG-AUTO-CONVEX";
    groundTruthROIName.stringValue = @"icontour";
    
    slicesFrom.intValue = 1;
    slicesTo.intValue = [self getSlicesCount];
    framesFrom.intValue = 1;
    framesTo.intValue = [self getFramesCount];
    
}



- (void) drawROI:(id) sender{
    [roiHandler drawROI:sender];
}

- (int) getSlicesCount{
    
    int nslices = [viewerController maxMovieIndex];
    return nslices;
}

- (int) getFramesCount{
    
    NSMutableArray     *PixList = [viewerController pixList: 0];
    int ntimes = [PixList count];
    
    return ntimes;
}

- (void) autoSetSegmentationLevels{
    
    int slices = [self getSlicesCount];
    int base = slices * 0.35;
    int midcavity = slices * 0.35;
    
    BasalFrom.intValue = 1;
    BasalTo.intValue = BasalFrom.intValue + base - 1;
    MidCavityFrom.intValue = BasalTo.intValue + 1;
    MidCavityTo.intValue = MidCavityFrom.intValue + midcavity - 1;
    ApicalFrom.intValue = MidCavityTo.intValue +1;
    ApicalTo.intValue = slices;
    
}

-(void)autoSetESED{
    //if volumes are set
    NSMutableArray *volumesArrayLVENDO = [self calcVolumesNew:@"LVENDO"];
    
    NSNumber *mymax = [NSNumber numberWithDouble:-MAXFLOAT];
    NSNumber *mymin = [NSNumber numberWithDouble:MAXFLOAT];
    int mymaxIDX = 0;
    int myminIDX = 0;
    for (int i = 0; i < [volumesArrayLVENDO count]; i++)
    {
        NSNumber *temp = [volumesArrayLVENDO objectAtIndex:i];
        if (([temp doubleValue] < [mymin doubleValue]) && ([temp doubleValue]!=0)) {
            mymin = temp;
            myminIDX = i;
        }
        if ([temp doubleValue] > [mymax doubleValue]){
            mymax = temp;
            mymaxIDX = i;
        }
    }
    
    NSLog(@"Min:%@ %i Max:%@ %i",mymin,myminIDX,mymax,mymaxIDX);
    
    if (([mymin doubleValue]!=0) && ([mymax doubleValue]!=0)){
        ED.intValue = mymaxIDX  + 1;
        ES.intValue = myminIDX + 1;
    } else{
        ED.intValue = 1;
        ES.intValue = [[viewerController pixList] count]/2+1;
    }
}

-(float) calcBSA{
    float BSA = 0;
    
    @try{
        // get first image
        NSArray     *PixList = [viewerController pixList: 0];
        DCMPix      *curPix = [PixList objectAtIndex: 0];
        // get dicom object
        DCMObject   *dcmObj = [DCMObject objectWithContentsOfFile:[curPix sourceFile] decodingPixelData:NO];
        DCMAttributeTag *patientsWeightTag = [DCMAttributeTag tagWithName:@"PatientsWeight"];
        DCMAttributeTag *patientsSizeTag = [DCMAttributeTag tagWithName:@"PatientsSize"];
        // get data from dicom tags
        float patientsWeight = [[[[dcmObj attributeForTag:patientsWeightTag] value] description] doubleValue];
        float patientsSize   = [[[[dcmObj attributeForTag:patientsSizeTag] value] description] doubleValue];
        BSA=sqrt(patientsWeight*patientsSize)/6;
        
    } @catch (NSException *exception){
        NSLog(@"BSA calculation exception: %@",exception);
    }
    return BSA;
}

-(double) calcDistanceBetweenSlices:(int) islice0 and: (int) islice1{
    double sliceDistance = 0;
    // get PixList for given slices
    NSArray     *PixList0 = [viewerController pixList: islice0];
    NSArray     *PixList1 = [viewerController pixList: islice1];
    
    // get first image objects in selected slice
    DCMPix      *curPix0 = [PixList0 objectAtIndex: 0];
    DCMPix      *curPix1 = [PixList1 objectAtIndex: 0];
    
    // get orientation (to use myorientation1[6-8] as normal vector)
    double      myorientation1[9];
    [curPix1 orientationDouble: myorientation1];
    
    double      sliceOrigins0[3] = {[curPix0 originX],[curPix0 originY],[curPix0 originZ]};
    double      sliceOrigins1[3] = {[curPix1 originX],[curPix1 originY],[curPix1 originZ]};
    // substract orientation point vectors
    double      tempp[3] = {sliceOrigins0[0] - sliceOrigins1[0], sliceOrigins0[1] - sliceOrigins1[1], sliceOrigins0[2] - sliceOrigins1[2]};
    // get normal vector (slice1)
    double      tempnormal1[3] = {myorientation1[6],myorientation1[7],myorientation1[8]};
    
    sliceDistance = [mathHandler dotProduct: tempnormal1 and: tempp];
    //NSLog(@"!!!Distance to previous slice: %g",sliceDistance);
    
    return sliceDistance;
}

-(NSMutableArray *)calcVolumesNew:(NSString*) RoiName{
    // number of slices selected
    int nslices = [viewerController maxMovieIndex];
    // number of frames in first slice
    NSMutableArray     *PixList = [viewerController pixList: 0];
    int ntimes = [PixList count];
    
    // get areaArray
    NSMutableArray *areaArray;
    areaArray =[self getAreaArrayFor:RoiName];
    
    //get slice thicknesses
    double *sliceThickneses = malloc( nslices * sizeof(double) );
    for (int j = 0; j < nslices; j++)
    {
        NSArray     *PixList = [viewerController pixList: j];
        DCMPix      *curPix = [PixList objectAtIndex: 0];
        sliceThickneses[j]=[curPix sliceThickness];
    }
    
    // do I have to initialize an array like this? =/
    NSNumber *temp =[[NSNumber alloc] initWithDouble:0];
    NSMutableArray *myvolume = [[NSMutableArray alloc] initWithCapacity:ntimes ];
    for (int i = 0; i < ntimes; i++)
        [myvolume insertObject:temp atIndex:i];
    
    // calculate volumes
    for (int itime = 0; itime < ntimes; itime++)
    {
        double tempvolume = 0;
        // calculate how many non empty elements there are
        int slicesWithROIcounter=0;
        for (int islice = 0; islice < nslices; islice++){
            if([areaArray objectAtIndex:islice*ntimes+itime]!=0){
                slicesWithROIcounter++;
            }
        }
        // calculate slice with roi indexes
        int *sliceWithRoiIdx = malloc(slicesWithROIcounter*sizeof(int));
        int i = 0;
        for (int islice = 0; islice < nslices; islice++){
            if([areaArray objectAtIndex:islice*ntimes+itime]!=0){
                sliceWithRoiIdx[i++]=islice;
            }
        }
        
        // calculate volumes
        if (slicesWithROIcounter>0){
            int idxFirst = sliceWithRoiIdx[0];
            tempvolume = sliceThickneses[idxFirst] * [[areaArray objectAtIndex:idxFirst*ntimes+itime] floatValue]/2/10;
            for (int i = 0; i<slicesWithROIcounter-1; i++){
                int idx0 = sliceWithRoiIdx[i];
                int idx1 = sliceWithRoiIdx[i+1];
                double mydistance;
                mydistance = [self calcDistanceBetweenSlices:idx0 and:idx1];
                tempvolume = tempvolume + mydistance * [[areaArray objectAtIndex:idx0*ntimes+itime] floatValue]/2/10;
                tempvolume = tempvolume + mydistance * [[areaArray objectAtIndex:idx1*ntimes+itime] floatValue]/2/10;
            }
            int idxLast = sliceWithRoiIdx[slicesWithROIcounter-1];
            tempvolume = tempvolume + sliceThickneses[idxLast] * [[areaArray objectAtIndex:idxLast*ntimes+itime] floatValue]/2/10;
        }
        [myvolume insertObject:[[NSNumber alloc] initWithDouble:tempvolume] atIndex:itime];
        NSLog(@"Frame: %d Volume: %g",itime,tempvolume);
        free(sliceWithRoiIdx);
    }
    return myvolume;
}

-(NSMutableArray *)getAreaArrayFor:(NSString *) RoiName{
    
    // number of slices selected
    int nslices = [viewerController maxMovieIndex];
    // number of frames in first slice
    NSMutableArray     *PixList = [viewerController pixList: 0];
    int ntimes = [PixList count];
    
    // do I have to initialize an array like this? =/
    NSNumber *temp =[[NSNumber alloc] initWithDouble:0];
    NSMutableArray *areaArray = [[NSMutableArray alloc] initWithCapacity:nslices*ntimes ];
    for (int i = 0; i < nslices * ntimes; i++)
        [areaArray insertObject:temp atIndex:i];
    
    // get all Rois with name RoiName
    for (int islice = 0; islice < [viewerController maxMovieIndex]; islice++)
    {
        // All rois contained in the current series
        NSMutableArray  *roiSeriesList  = [viewerController roiList: islice];
        for (int itime = 0; itime < ntimes; itime++)
        {
            // All rois contained in the current image
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: itime];
            for (int i = 0; i < [roiImageList count]; i++)
            {
                ROI *curROI = [roiImageList objectAtIndex: i];
                if ([[curROI name] isEqual:RoiName]){
                    NSNumber *temp = [[NSNumber alloc] initWithFloat:[curROI roiArea]];
                    [areaArray insertObject:temp atIndex:islice*ntimes+itime];
                    NSLog(@"Found %@ ROI. Slice:%d Frame:%d Area:%@",RoiName,islice,itime,[areaArray objectAtIndex:islice*ntimes+itime]);
                }
            }
        }
    }
    return areaArray;
}


-(void)calcVolumes;
{
    NSMutableArray *volumesArrayLVEPI = [self calcVolumesNew:@"LVEPI"];
    NSMutableArray *volumesArrayLVENDO = [self calcVolumesNew:@"LVENDO"];
    NSMutableArray *volumesArrayRVEPI = [self calcVolumesNew:@"RVEPI"];
    NSMutableArray *volumesArrayRVENDO = [self calcVolumesNew:@"RVENDO"];
    // check if ES and ED makes sense
    int ntimes = [[viewerController pixList] count];
    int es = [ES intValue];
    int ed = [ED intValue];
    float bsa = [BSAField floatValue];
    if ((es<0) || (es>ntimes) || (ed<0) || (ed>ntimes)){
        NSAlert* msgBox = [[NSAlert alloc] init];
        [msgBox setMessageText: @"Problem with ES or ED"];
        [msgBox addButtonWithTitle: @"OK"];
        [msgBox runModal];
        return;
    }
    double LVESV = [[volumesArrayLVENDO objectAtIndex:es-1] doubleValue];
    double LVEDV = [[volumesArrayLVENDO objectAtIndex:ed-1] doubleValue];
    double LVSV  = LVEDV-LVESV;
    double LVEF  = 100*LVSV/LVEDV;
    double LVm   = 1.05*([[volumesArrayLVEPI objectAtIndex:ed-1] doubleValue] - [[volumesArrayLVENDO objectAtIndex:ed-1] doubleValue]);
    double RVESV = [[volumesArrayRVENDO objectAtIndex:es-1] doubleValue];
    double RVEDV = [[volumesArrayRVENDO objectAtIndex:ed-1] doubleValue];
    double RVSV  = RVEDV-RVESV;
    double RVEF  = 100*RVSV/RVEDV;
    double RVm   = 1.05*([[volumesArrayRVEPI objectAtIndex:ed-1] doubleValue] - [[volumesArrayRVENDO objectAtIndex:ed-1] doubleValue]);
    
    NSString *lv = [NSString stringWithFormat:@"LV:\n%.2f\n%.2f\n%.1f\n%.2f\n%.2f\n",LVESV,LVEDV,LVSV,LVEF,LVm];
    NSString *lvbsa = [NSString stringWithFormat:@"LV/BSA:\n%.2f\n%.2f\n%.1f\n\n%.2f\n",LVESV/bsa,LVEDV/bsa,LVSV/bsa,LVm/bsa];
    NSString *rv = [NSString stringWithFormat:@"RV:\n%.2f\n%.2f\n%.1f\n%.2f\n%.2f\n",RVESV,RVEDV,RVSV,RVEF,RVm];
    NSString *rvbsa = [NSString stringWithFormat:@"RV/BSA:\n%.2f\n%.2f\n%.1f\n\n%.2f\n",RVESV/bsa,RVEDV/bsa,RVSV/bsa,RVm/bsa];
    [LVparams setStringValue:lv];
    [LVBSAparams setStringValue:lvbsa];
    [RVparams setStringValue:rv];
    [RVBSAparams setStringValue:rvbsa];
}

- (void)fillSectorMeanArray{
    
    //double *sectorArrayLocal;
    int frames = [self getFramesCount];
    
    for (int i = 0; i < 6; i++){
        for (int j = 0; j < frames; j++){
            sectorNewArray[j*6+i] = 0;
        }
    }
    
    
    int curSlice =  [viewerController curMovieIndex];
    NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
    NSArray         *pixList = [viewerController pixList: curSlice];
    
    for(int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
        
        NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
        DCMPix          *curPix = [pixList objectAtIndex: curTimeFrame];
        
        
        for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
            
            ROI *curROI = [roiImageList objectAtIndex:curROINumber];
            
            for (int sectorNumber = 1; sectorNumber < 17; sectorNumber++){
                
                NSString *sectorName = [NSString stringWithFormat:@"SECTOR %i", sectorNumber];
                if ([[curROI name] isEqualTo:sectorName]){
                    
                    int newSectorNumber = sectorNumber;
                    
                    if (sectorNumber > 6 && sectorNumber < 13){
                        newSectorNumber = sectorNumber - 6;
                    }
                    if (sectorNumber > 12){
                        newSectorNumber = sectorNumber - 12;
                    }
                    
                    float       rmean, rtotal, rdev, rmin, rmax;
                    [curPix computeROI:curROI:&rmean :&rtotal :&rdev :&rmin :&rmax];
                    NSLog(@"%f",rmean);
                    
                    sectorNewArray[(newSectorNumber-1)*frames+curTimeFrame] = rmean;
                }
            }
        }
    }
}

- (double)calculateMaxSectorMean{
    double localSectorMax = 0;
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    int curSlice = [viewerController curMovieIndex];
    
    //for (int curSlice = 0; curSlice < slices; curSlice++){
    
    
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        NSArray         *pixList = [viewerController pixList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            DCMPix          *curPix = [pixList objectAtIndex: curTimeFrame];
            
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                
                for (int sectorNumber = 1; sectorNumber < 17; sectorNumber++){
                    
                    NSString *sectorName = [NSString stringWithFormat:@"SECTOR %i", sectorNumber];
                    
                    if ([[curROI name] isEqualTo:sectorName]){
                        
                        float       rmean, rtotal, rdev, rmin, rmax;
                        [curPix computeROI:curROI:&rmean :&rtotal :&rdev :&rmin :&rmax];
                        
                        
                        //double mean = [curROI mean];
                        if (rmean > localSectorMax){
                            localSectorMax = rmean;
                        }
                    }
                }
            }
        }
    //}
    return localSectorMax;
    
}

- (void) fillPlotVariables{
    
    maxSectorMean = [self calculateMaxSectorMean];
    
    int         curSlice =  [viewerController curMovieIndex] + 1;
    numberOfSectors = 0;
    if(curSlice > 0 && curSlice <= BasalTo.integerValue){
        numberOfSectors = 6;
    }
    else if (curSlice > BasalTo.integerValue && curSlice <= MidCavityTo.integerValue){
        numberOfSectors = 6;
    }
    else{
        numberOfSectors = 4;
    }
    
    [self fillSectorMeanArray];

}

-(void) createMasksForPolygons{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    for (int curSlice = 0; curSlice < slices; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            int sizeOfROIImageList = [roiImageList count];
            
            for (int curROINumber = 0; curROINumber < sizeOfROIImageList; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if (curROI.type == tCPolygon){
                    
                    ROI *newROI = [roiHandler convertPolygonToBrush:curROI];
                    [roiImageList addObject:newROI];
                    
                }
            }
        }
    }
}

-(void) createMYOMasks{
    
    NSArray *myROINames = @[@"LVEPI",@"LVENDO",@"LVEPI (a)",@"LVENDO (a)"];
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    for (int curSlice = 0; curSlice < slices; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            int sizeOfROIImageList = [roiImageList count];
            
            //create myo mask
            ROI *ROI1 = NULL;
            ROI *ROI2 = NULL;
            for (int curROINumber = 0; curROINumber < sizeOfROIImageList; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                
                //check if brush
                if (curROI.textureBuffer != nil){
                    
                    //EPI
                    if ([[curROI name] isEqualTo: myROINames[0]] || [[curROI name] isEqualTo: myROINames[2]]){
                        ROI1 = curROI;
                    }
                    if ([[curROI name] isEqualTo: myROINames[1]] || [[curROI name] isEqualTo: myROINames[3]]){
                        ROI2 = curROI;
                    }
                }
            }
            if (ROI1 != NULL && ROI2 != NULL){

                DCMPix *roiPix = [ROI1 pix];
                
                ROI *result = [roiHandler brushROIProduct:ROI1 and:ROI2 and:@"MYO" and:ROI1.pix.pwidth and:ROI1.pix.pheight and:ROI1.pixelSpacingX and:ROI1.pixelSpacingY and:ROI1.imageOrigin operation: XOR];
                
                [roiImageList addObject:result];
                NSLog(@"MYO created for slice %d, frame %d",curSlice, curTimeFrame);
            }
        }
    }
}

-(void) createSectorsNew{
    
    [self createMasksForPolygons];

    [self createMasks];

    [self createSectorsFromMask];
    
    NSLog(@"success?");
}



-(void) createMasks{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    NSArray *myROINames = @[@"LVEPI",@"LVENDO",@"LVEPI (a)",@"LVENDO (a)"];
    
    float PixHeightToRemember;
    float PixWidthToRemember;
    NSPoint imageOriginToRemember;
    float PixelSpacingXToRemember;
    float PixelSpacingYToRemember;
    
    for (int curSlice = 0; curSlice < slices; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            int sizeOfROIImageList = [roiImageList count];
            
            for (int curROINumber = 0; curROINumber < sizeOfROIImageList; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if (curROI.type == tCPolygon){
                    
                    PixHeightToRemember = curROI.pix.pheight;
                    PixWidthToRemember = curROI.pix.pwidth;
                    imageOriginToRemember = curROI.imageOrigin;
                    PixelSpacingXToRemember = curROI.pixelSpacingX;
                    PixelSpacingYToRemember = curROI.pixelSpacingY;
                    
                    NSLog(@"working...%d",curROINumber);
                
                }
                
            }
            
            //create myo mask
            ROI *ROI1 = NULL;
            ROI *ROI2 = NULL;
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                
                //check if brush
                if ([curROI textureBuffer] != NULL){
                    
                    //EPI
                    if ([[curROI name] isEqualTo: myROINames[0]] || [[curROI name] isEqualTo: myROINames[2]]){
                        ROI1 = curROI;
                    }
                    if ([[curROI name] isEqualTo: myROINames[1]] || [[curROI name] isEqualTo: myROINames[3]]){
                        ROI2 = curROI;
                    }
                }
            }
            if (ROI1 != NULL && ROI2 != NULL){
                ROI *result = [roiHandler brushROIProduct:ROI1 and:ROI2 and:@"MYO" and:PixWidthToRemember and:PixHeightToRemember and:PixelSpacingXToRemember and:PixelSpacingYToRemember and:imageOriginToRemember operation: XOR];
                [roiImageList addObject:result];
            }
        }
    }
    
    //start of creating sectors
    
    int width = PixWidthToRemember;
    int height = PixHeightToRemember;
    int widthHalf =  width / 2;
    int heightHalf = height / 2;
    
    double x[width];
    double y[height];
    double atanGrid[width][height];
    
    for (int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
        
        for (int curSlice = 0; curSlice < slices; curSlice++){
            
            NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            NSPoint centerPoint;
            NSPoint segPoint;
            int checksum = 0;
            
            int hasEPI = 0;
            int hasPoint = 0;
            
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                
                //EPI
                if (([[curROI name] isEqualTo: myROINames[1]] || [[curROI name] isEqualTo: myROINames[3]]) && [curROI type] == tCPolygon){
                    
                    centerPoint = [curROI centroid];
                    
                    checksum = checksum + 100;
                    hasEPI = 1;
                    
                    
                    widthHalf = centerPoint.x;
                    heightHalf = centerPoint.y;
                    
                    
                    
                }
                
                if([[curROI name] isEqualTo:@"SegPoint"]){
                    
                    segPoint = [curROI pointAtIndex:0];
                    checksum = checksum + 1;
                    hasPoint = 1;
                    
                }
            }
            
            //meshgrid
            for (int i = 0; i < width; i++){
                x[i] = i+1;
            }
            
            for (int i = 0; i < height; i++){
                y[i] = i+1;
            }
            
            for (int i = 0; i < width; i++){
                for (int j = 0; j < height; j++){
                    
                    atanGrid[i][j] = atan2((x[i] - widthHalf),(y[j] - heightHalf));
                    
                }
            }
            
            //here we have two starting points
            if (hasEPI == 1 && hasPoint ==1){
                
                int segmentsNumber = 0;
                int segmentNumbering = 0;
                
                if (curSlice >= BasalFrom.integerValue - 1 && curSlice <= BasalTo.intValue -1){
                    segmentsNumber = 6;
                    segmentNumbering = 0;
                }
                
                if (curSlice >= MidCavityFrom.integerValue - 1 && curSlice <= MidCavityTo.intValue -1){
                    segmentsNumber = 6;
                    segmentNumbering = 6;
                }
                if (curSlice >= ApicalFrom.integerValue - 1 && curSlice <= ApicalTo.intValue -1){
                    segmentsNumber = 4;
                    segmentNumbering = 12;
                }
                
                double a = (centerPoint.y - segPoint.y) / (centerPoint.x - segPoint.x);
                double angle = 0 - M_PI + ((M_PI / 2) - atan(a));
                
                
                double moveValue = (2* M_PI / segmentsNumber);
                
                
                for (int curSeg = 1; curSeg < ( segmentsNumber+1 ); curSeg ++){
                    
                    
                    unsigned char *mask;
                    mask = (unsigned char*)malloc((width*height)*sizeof(unsigned char));
                    
                    for (int i = 0; i < width; i++){
                        for (int j = 0; j < height; j++){
                            
                            // stary warunek: atanGrid[i][j] > angle && atanGrid[i][j] < (angle+firstMove)
                            
                            
                            
                            if (atanGrid[i][j] > angle && atanGrid[i][j] < (angle+moveValue)){
                                
                                mask[i + (j*width)] = 0xFF;
                            }
                            else{
                                
                                mask[i + (j*width)] = 0x00;
                            }
                            
                            if ( (angle + moveValue) > M_PI ){
                                
                                if (atanGrid[i][j] > (0 - M_PI) && atanGrid[i][j] < (0 - M_PI + ((M_PI / 2) - atan(a)))){
                                    
                                    mask[i + (j*width)] = 0xFF;
                                    
                                }
                            }
                            
                            
                        }
                    }
                    NSString *maskName = [NSString stringWithFormat:@"MASK %i", curSeg + segmentNumbering];
                    
                    ROI *newROI = [[ROI alloc] initWithTexture:mask textWidth:width textHeight:height textName:maskName positionX:0 positionY:0 spacingX:PixelSpacingXToRemember spacingY:PixelSpacingYToRemember imageOrigin:imageOriginToRemember];
                    //[newROI setColor:[curROI rgbcolor]];
                    [roiImageList addObject:newROI];
                    free(mask);
                    
                    angle = angle + moveValue;
                    
                }
            }
        }
    }
    
    [viewerController needsDisplayUpdate];

}

-(void) setPointForSegments{
    
    int         curTimeFrame =  [[viewerController imageView] curImage];
    int         curSlice =  [viewerController curMovieIndex];
    NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
    
    // change ROI tool to tcpolygon
    [viewerController setROIToolTag:t2DPoint];
    
    // settingTheName
    for (int i = 0; i < [roiImageList count]; i++)
    {
        
        [roiHandler setNameForSelectedRoiIfPointType:[roiImageList objectAtIndex: i] name:@"SegPoint"];
        
    }
    
    [roiHandler propagateSelectedPointROIOverSlices:curSlice curTimeFrame:curTimeFrame roiSeriesList:roiSeriesList roiImageList:roiImageList slicesNumber:[self getSlicesCount]];

}

-(void) createSectorsFromMask{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    maxSectorMean = 0;
    
    float pixWidth;
    float pixHeight;
    float pixelSpacingX;
    float pixelSpacingY;
    NSPoint imageOrigin;
    
    for (int curSlice = 0; curSlice < slices; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < frames; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            int sizeOfROIImageList = [roiImageList count];
            
            int hasMYO = 0;
            int hasSectors = 0;
            ROI *MYOROI;
            int indexOfMYO = 0;
            
            for (int curROINumber = 0; curROINumber < sizeOfROIImageList; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                
                if ([[curROI name] isEqualTo:@"MYO"]){
                    MYOROI = curROI;
                    hasMYO = 1;
                    indexOfMYO = curROINumber;
                }
                
                if ([[curROI name] isEqualTo:@"MASK 1"]){
                    hasSectors = 1;
                }
                if ([[curROI name] isEqualTo:@"MASK 7"]){
                    hasSectors = 1;
                }
                if ([[curROI name] isEqualTo:@"MASK 13"]){
                    hasSectors = 1;
                }
                if ([curROI type] == tCPolygon){
                    
                    DCMPix          *curPix = [curROI pix];
                    pixHeight = [curPix pheight];
                    pixWidth = [curPix pwidth];
                    pixelSpacingX = [curROI pixelSpacingX];
                    pixelSpacingY = [curROI pixelSpacingY];
                    imageOrigin = [curROI imageOrigin];
                    
                }
                
            }
            
            if (hasMYO == 1 && hasSectors == 1){
                
                
                for (int curROINumber = 0; curROINumber < sizeOfROIImageList; curROINumber++){
                    
                    ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                    
                    for (int maskNumber = 1; maskNumber < 17; maskNumber++){
                        
                        NSString *maskName = [NSString stringWithFormat:@"MASK %i", maskNumber];
                        
                        if ([[curROI name] isEqualTo:maskName]){
                            
                            NSString *newROIName = [NSString stringWithFormat:@"SECTOR %i", maskNumber];
                            
                            ROI *newROI = [roiHandler brushROIProduct:curROI and: MYOROI and: newROIName and:pixWidth and:pixHeight and:pixelSpacingX and:pixelSpacingY and:imageOrigin operation: AND];
                            [roiImageList addObject:newROI];
                            
                            if ([newROI mean] > maxSectorMean){
                                maxSectorMean = [newROI mean];
                            }
                            
                            
                            
                        }
                        
                    }
                    
                    
                }
                
                //deleting unused rois
                
                int curNumber = 0;
                
               while (curNumber < [roiImageList count]){
                    
                    ROI *curROI = [roiImageList objectAtIndex:curNumber];
                    
                    if ([[curROI name] isEqualTo:@"MYO"]){
                        [roiImageList removeObjectAtIndex:curNumber];
                        curNumber--;
                    }
                    if (curROI.type == tPlain && ([[curROI name] isEqualTo:@"LVEPI"] || [[curROI name] isEqualTo:@"LVENDO"] || [[curROI name] isEqualTo:@"LVEPI (a)"] || [[curROI name] isEqualTo:@"LVENDO (a)"])){
                        [roiImageList removeObjectAtIndex:curNumber];
                        curNumber--;
                    }
                    curNumber++;
                }
                
                
                
                for (int maskNumber = 1; maskNumber < 17; maskNumber++){
                    
                    NSString *maskName = [NSString stringWithFormat:@"MASK %i", maskNumber];
                    
                    for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                        
                        ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                        
                        if ([[curROI name] isEqualTo:maskName])
                        {
                            [roiImageList removeObjectAtIndex:curROINumber];
                        }
                    }
                }
                
                
                
                
            }
            
        }
    }

}

-(void) approximateBetweenTwoROIs: (ROIClass*) ROI1 secondROI: (ROIClass*) ROI2 directionsNumber: (int) directions{
    
    if (ROI1.sliceNumber ==! ROI2.sliceNumber){
    // Program should never enter here
        return;
    }
    
    int directionsNumber;
    int directionsStart;
    
    if (directions == 0){
        directionsStart = 0;
        directionsNumber = 1;
    }
    else if(directions == 1){
        directionsStart = 1;
        directionsNumber = 2;
    }
    else{
        directionsStart = 0;
        directionsNumber = 2;
    }
    
    int numberOfApproximationPoints = abs(ROI2.frameNumber - ROI1.frameNumber) - 1;
    BOOL changeOrder = NO;
    
    if ([ROI1.roi roiArea] > [ROI2.roi roiArea]){
        changeOrder = YES;
        ROIClass *tempROI = ROI1;
        ROI1 = ROI2;
        ROI2 = tempROI;
    }
    
    //names are confusing - actually ES means smaller and ED means bigger
    NSMutableArray *approximationEStoED = [[NSMutableArray alloc] init];
    NSMutableArray *approximationEDtoES = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < numberOfApproximationPoints; i++){
        [approximationEStoED addObject:[[ROI alloc] initWithType:tCPolygon :[ROI1.roi pixelSpacingX]:[ROI1.roi imageOrigin]]];
        [approximationEDtoES addObject:[[ROI alloc] initWithType:tCPolygon :[ROI1.roi pixelSpacingX]:[ROI1.roi imageOrigin]]];
    }
    
    for (int curDirectionOfApproximation = directionsStart; curDirectionOfApproximation < directionsNumber; curDirectionOfApproximation++){
        
        ROI *curESorED;
        ROI *otherESorED;
        int curLocalESorED;
        int otherLocalESorED;
        double maxDistanceBetweenPoints = 0;
        
        // w zaleznosci od kierunku aproksymacji przypisujemy pomocnicze zmienne
        if (curDirectionOfApproximation == 0){
            curESorED = ROI1.roi;
            otherESorED = ROI2.roi;
            curLocalESorED = ROI1.frameNumber;
            otherLocalESorED = ROI2.frameNumber;
        }
        else{
            curESorED = ROI2.roi;
            otherESorED = ROI1.roi;
            curLocalESorED = ROI2.frameNumber;
            otherLocalESorED = ROI1.frameNumber;
        }
        NSPoint centerPointGlobal = [ROI2.roi centroid];
        
        for (int curROIPointNumber=0; curROIPointNumber < [[ROI2.roi points] count]; curROIPointNumber++){
            //tu liczymy MAX odleglosc srodek - punkt
            
            NSPoint workingPoint = [ROI2.roi pointAtIndex:curROIPointNumber];
            
            
            double distance = sqrt(((centerPointGlobal.x-workingPoint.x)*(centerPointGlobal.x-workingPoint.x))+((centerPointGlobal.y-workingPoint.y)*(centerPointGlobal.y-workingPoint.y)));
            
            if (distance>maxDistanceBetweenPoints){
                maxDistanceBetweenPoints = distance;
            }
        }
        
        
        for (int curROIPointNumber=0; curROIPointNumber < [[curESorED points] count]; curROIPointNumber++){
            
            double newPointX;
            double newPointY;
            
            NSPoint edgePoint = [curESorED pointAtIndex:curROIPointNumber];
            
            NSPoint centerPoint = [curESorED centroid];
            
            NSPoint newPoint = [roiHandler createPointForApproximationFromROI:otherESorED edgePoint:&edgePoint centerPoint:&centerPoint directionNumber:curDirectionOfApproximation];
            
            newPointX = newPoint.x;
            newPointY = newPoint.y;
            
            double distance = sqrt(((centerPointGlobal.x-newPoint.x)*(centerPointGlobal.x-newPoint.x))+((centerPointGlobal.y-newPoint.y)*(centerPointGlobal.y-newPoint.y)));
            
            if (distance <= maxDistanceBetweenPoints){
                
                NSMutableArray *arrayOfPoints;
                
                if (curDirectionOfApproximation == 0){
                    arrayOfPoints = [mathHandler calculatePointsbetweenEdgeAndNewPoint:&edgePoint newPoint:&newPoint numberOfPoints:numberOfApproximationPoints];
                }
                else{
                    arrayOfPoints = [mathHandler calculatePointsbetweenEdgeAndNewPoint:&newPoint newPoint:&edgePoint numberOfPoints:numberOfApproximationPoints];
                }
                
                //zapelnianie ROIow ED - ES
                for (int j = 0; j < numberOfApproximationPoints; j++){
                    
                    NSValue *workingValue =[arrayOfPoints objectAtIndex:j];
                    NSPoint workingPoint = [workingValue pointValue];
                    
                    if (curDirectionOfApproximation == 0){
                        [[approximationEStoED objectAtIndex:j] addPoint:workingPoint];
                    }
                    else{
                        [[approximationEDtoES objectAtIndex:j] addPoint:workingPoint];
                    }
                    
                }
            }
        }
    }

    for (int i = 0; i <numberOfApproximationPoints; i++){
        
        ROI *roiToAdd = nil;
        
        if (directions == 0){
            roiToAdd = [approximationEStoED objectAtIndex:numberOfApproximationPoints-i-1];
        }
        else if (directions == 1){
            roiToAdd = [approximationEDtoES objectAtIndex:numberOfApproximationPoints-i-1];
        }
        else{
            //porzucone
            ROI *brushFirst = [[ROI alloc] init];
            ROI *brushSecond = [[ROI alloc] init];
            
            brushFirst = [roiHandler convertPolygonToBrush:[approximationEStoED objectAtIndex:numberOfApproximationPoints-i-1]];
            brushSecond = [roiHandler convertPolygonToBrush:[approximationEDtoES objectAtIndex:numberOfApproximationPoints-i-1]];
            
            ROI *product = [roiHandler brushROIProduct:brushFirst and:brushSecond and:@"nic" and: [[brushFirst pix] pwidth] and:[[brushFirst pix] pheight] and:[brushFirst pixelSpacingX] and:[brushFirst pixelSpacingY] and:[brushFirst imageOrigin] operation:XOR];
            
            int pointsNumber = [[[approximationEStoED objectAtIndex:numberOfApproximationPoints-i-1] points] count] + [[[approximationEDtoES objectAtIndex:numberOfApproximationPoints-i-1] points] count];
            
            roiToAdd = [viewerController convertBrushROItoPolygon:product numPoints:pointsNumber];
            
        }
        
        NSString *newName = [[ROI1.roi name] stringByAppendingString:@" (a)"];
        ROI* newROI = [roiHandler createROIWithName:newName type:[ROI1.roi type] color:[ROI1.roi rgbcolor] points:[roiToAdd points] pixelSpacingX:[ROI1.roi pixelSpacingX] imageOrigin:[ROI1.roi imageOrigin]];
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: ROI1.sliceNumber];
        NSMutableArray *roiImageList;
        if (changeOrder == YES){
            roiImageList = [roiSeriesList objectAtIndex: (ROI1.frameNumber-i-1)];
        }
        else{
            roiImageList = [roiSeriesList objectAtIndex: (ROI1.frameNumber+i+1)];
        }
        [roiImageList addObject:newROI];
    }

    
}

-(void) approximationMainFunction: (int) curSlice whereIsROI:(NSMutableArray*) whereIsROI roiNameNumber: (int) curROINameNumber directionsNumber: (int) directions{
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
    
    int localES = ES.integerValue - 1;
    int localED = ED.integerValue - 1;
    
    int approximationLastFrame = [self getFramesCount]-1-localED;
    
    // ZABEZPIECZENIE CZY NA ED I ES SA ROIE
    NSMutableArray *roiImageListED = [roiSeriesList objectAtIndex:localED];
    ROI *approxED = [roiHandler searchROIWithROINameNumberInFrame:curROINameNumber image:roiImageListED];
    
    NSMutableArray *roiImageListES = [roiSeriesList objectAtIndex:localES];
    ROI *approxES = [roiHandler searchROIWithROINameNumberInFrame:curROINameNumber image:roiImageListES];
    
    if (approxES == NULL){
        
        NSString *alertString = [NSString stringWithFormat:@"There is no ROI type %@ on slice number %d on ES frame. Approximation aborted on this slice.",[[roiHandler myROINames] objectAtIndex:curROINameNumber], curSlice+1];
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Approximation aborted!"];
        [alert setInformativeText:alertString];
        [alert setAlertStyle:NSWarningAlertStyle];
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            
            // OK clicked
        }
        return;
    }
    
    if (approxED == NULL){
        
        NSString *alertString = [NSString stringWithFormat:@"There is no ROI type %@ on slice number %d on ED frame. Approximation aborted on this slice.",[[roiHandler myROINames] objectAtIndex:curROINameNumber], curSlice+1];
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"Approximation aborted!"];
        [alert setInformativeText:alertString];
        [alert setAlertStyle:NSWarningAlertStyle];
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            
            // OK clicked
        }
        return;
        
    }
    
    
    NSMutableArray *roiImageListLastApproxFrame = [roiSeriesList objectAtIndex:approximationLastFrame];
    
    ROI *foundROI = [roiHandler searchROIWithROINameNumberInFrame:curROINameNumber image:roiImageListLastApproxFrame];
    if (foundROI == NULL){
        ROIClass *lastFrame = [[ROIClass alloc] init];
        lastFrame.roi = [roiHandler copyROIToSliceNumberAndFrameNumber:[[whereIsROI objectAtIndex:0] roi] sliceNumber:curSlice frameNumber:approximationLastFrame];
        lastFrame.roi.name = [lastFrame.roi.name stringByAppendingString:@" (a)"];
        lastFrame.sliceNumber = curSlice;
        lastFrame.frameNumber = approximationLastFrame;
        [whereIsROI addObject:lastFrame];
    }

    // KONIEC ZABEZPIECZEN
    
    // GLOWNA PETLA
    for (int curROI = 1; curROI < [whereIsROI count]; curROI++){
        
        [self approximateBetweenTwoROIs:[whereIsROI objectAtIndex:(curROI-1)] secondROI:[whereIsROI objectAtIndex:curROI] directionsNumber:directions];
        
    }
}

-(void) approximate: (NSComboBox*) comboBox copyButton: (NSButton*) copyButton{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];

    //-----------APPROXIMATION
    for (int curSlice = 0;curSlice<slices;curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for (int curROINameNumber = 0;  curROINameNumber < 4;    curROINameNumber++){
            
            NSMutableArray *whereIsROI = [roiHandler searchCurSliceForROISWithNameNumber:curROINameNumber curSlice:curSlice];
            int numberOfROIs = [whereIsROI count];
            
            /*
             jesli 2 i wiecej (default)
             wez pierwszy, wylicz mu srodek, poprowadz ze srodka proste do jego punktow i przetni je ze srodkiem punktow drugiego
             wez drugi, wylicz mu srodek, poprowadz ze srodka proste i przetni je w druga strone
             z nastepnymi rob to samo (zawsze bierz pary)
             */
            
            switch(numberOfROIs){
                    
                case 0:{
                    //nic nie rob
                    NSLog(@"Slice No. %d has no ROI type %d",curSlice, curROINameNumber);
                    break;
                }
                
                case 1:{
                    
                    NSLog(@"Slice No. %d has 1 ROI type %d",curSlice, curROINameNumber);
                        
                        // OK clicked
                    if (copyButton.state == 1){
                        [self copyROIToEveryOtherFrame:[whereIsROI objectAtIndex:0]];
                    }
                    
                    break;
                }
                default:{
                    NSLog(@"Slice No. %d has %d ROI type %d",curSlice,numberOfROIs, curROINameNumber);
                    [self approximationMainFunction:curSlice whereIsROI:whereIsROI roiNameNumber:curROINameNumber directionsNumber:[comboBox indexOfSelectedItem]];
                    break;
                }
            }

            [viewerController needsDisplayUpdate];
        }
        
    }
    
}

- (void) copyROIToEveryOtherFrame: (ROIClass*) roi{
    
    ROI *curROI = roi.roi;
    NSMutableArray  *roiSeriesList  = [viewerController roiList: roi.sliceNumber];
    
    for (int curTimeFrame = 0;  curTimeFrame<[self getFramesCount];   curTimeFrame++){
        
        NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
        if([roiHandler checkIfImageHasROI:curROI series:roiImageList] == NO){
            
            ROI *newROI = [roiHandler createROIWithName:[curROI.name stringByAppendingString:@" (a)"] type:tCPolygon color:curROI.rgbcolor points:curROI.points pixelSpacingX:curROI.pixelSpacingX imageOrigin:curROI.imageOrigin];
            
            [roiHandler addROI:newROI sliceNumber:roi.sliceNumber frameNumber:curTimeFrame];
            
        }
    }
    
    
    
    
}

- (void) deletePointROIs{
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < [self getFramesCount]; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            NSMutableArray *objectsToRemove = [[NSMutableArray alloc] init];
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if ([curROI type] == t2DPoint){
                    [objectsToRemove addObject:curROI];
                }
            }
            
            [roiImageList removeObjectsInArray:objectsToRemove];
        }
    }
    [viewerController needsDisplayUpdate];
}
- (void) deleteAllBrushROIs{
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < [self getFramesCount]; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            NSMutableArray *objectsToRemove = [[NSMutableArray alloc] init];
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if ([curROI type] == tPlain){
                    [objectsToRemove addObject:curROI];
                }
            }

            [roiImageList removeObjectsInArray:objectsToRemove];
        }
    }
    [viewerController needsDisplayUpdate];
}

- (void) deleteAlltcPolygonROIs{
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < [self getFramesCount]; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            NSMutableArray *objectsToRemove = [[NSMutableArray alloc] init];
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if ([curROI type] == tCPolygon){
                    [objectsToRemove addObject:curROI];
                }
            }
            
            [roiImageList removeObjectsInArray:objectsToRemove];
        }
    }
    [viewerController needsDisplayUpdate];
}

- (void) deleteApproximationROIs{
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < [self getFramesCount]; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            NSMutableArray *objectsToRemove = [[NSMutableArray alloc] init];
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if ([[curROI name] containsString:@" (a)"]){
                    [objectsToRemove addObject:curROI];
                }
            }
            
            [roiImageList removeObjectsInArray:objectsToRemove];
        }
    }
    [viewerController needsDisplayUpdate];
}
- (void) deleteAllROIsWithName: (NSString*) string{
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        
        NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
        
        for(int curTimeFrame = 0; curTimeFrame < [self getFramesCount]; curTimeFrame++){
            
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
            NSMutableArray *objectsToRemove = [[NSMutableArray alloc] init];
            for (int curROINumber = 0; curROINumber < [roiImageList count]; curROINumber++){
                
                ROI *curROI = [roiImageList objectAtIndex:curROINumber];
                if ([curROI name] == string){
                    [objectsToRemove addObject:curROI];
                    [viewerController deleteROI:curROI];
                }
            }
            
            [roiImageList removeObjectsInArray:objectsToRemove];
        }
    }
    [viewerController needsDisplayUpdate];
    
}


- (void) changeSlice: (int) slice{
    [viewerController setMovieIndex:slice];
}
- (void) changeFrame: (int) frame{
    [viewerController setImageIndex:frame];
}

- (void) importQmass{
    
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    
    panel.title = @"Choose a contour file";
    panel.showsResizeIndicator = YES;
    panel.showsHiddenFiles = NO;
    panel.canChooseDirectories = NO;
    panel.canCreateDirectories = YES;
    panel.allowsMultipleSelection = NO;
    [panel setAllowedFileTypes:[NSArray arrayWithObjects:@"con",nil]];
    
    [panel beginSheetModalForWindow:mainWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            
            NSURL* theDoc = [[panel URLs] objectAtIndex:0];
            NSLog(@"%@",theDoc);
            
            NSString *filePath = [theDoc.path stringByResolvingSymlinksInPath];
            
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            if (fileExists){
                [self createROIsFromURL:filePath];
            }
            
        }
        
    }];
    
}

- (void) importTextROI{
    
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    
    panel.title = @"Choose a textfile with ROIs";
    panel.showsResizeIndicator = YES;
    panel.showsHiddenFiles = NO;
    panel.canChooseDirectories = NO;
    panel.canCreateDirectories = YES;
    panel.allowsMultipleSelection = YES;
    [panel setAllowedFileTypes:[NSArray arrayWithObjects:@"txt",nil]];
    
    [panel beginSheetModalForWindow:mainWindow completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            for (int i = 0; i<[[panel URLs] count];i++){
                NSURL* theDoc = [[panel URLs] objectAtIndex:i];
                NSLog(@"%@",theDoc);
                
                NSString *filePath = [theDoc.path stringByResolvingSymlinksInPath];
                
                int start = [filePath rangeOfString:@"IM-0001"].location;
                NSString *frameString = [filePath substringFromIndex:start+8];
                frameString = [frameString substringToIndex:4];
                
                BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                if (fileExists){
                    [self createROIsFromTEXT:filePath frame:[frameString integerValue]-1];
                }
            }
            
        }
        
    }];
    
}

- (void) createROIsFromTEXT: (NSString*) filePath frame:(int) frame{
    
    @try
    {
        NSFileHandle * fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
        NSString* input = [[NSString alloc] initWithData: [fileHandle availableData] encoding: NSUTF8StringEncoding];
        NSArray* lines = [input componentsSeparatedByString: @"\n"];
        NSMutableArray  *points = [[NSMutableArray alloc] init];
        
        for (int iLine = 0; iLine < [lines count]-1; iLine++)
        {
            NSArray *components1 = [[lines objectAtIndex:iLine] componentsSeparatedByString:@" "];
                
            float x = [[components1 objectAtIndex:0] floatValue];
            float y = [[components1 objectAtIndex:1] floatValue];
            [points addObject: [viewerController newPoint: x : y]];
        }
        
        int slice = frame/[self getFramesCount];
        NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
        NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: (frame - slice*frame)];
        
        ROI *newROI = [roiHandler createNewROIWithCopyPoints:points type:tCPolygon nameNumber:12 colorNumber:0];
        
        if([filePath containsString:@"icontour"]){
            [newROI setName:@"icontour"];
        }else if([filePath containsString:@"ocontour"]){
            [newROI setName:@"ocontour"];
        }
        else if([filePath containsString:@"p1contour"]){
            [newROI setName:@"p1contour"];
        }else{
            [newROI setName:@"p2contour"];
        }
        
        
        [roiImageList addObject: newROI];
        
        NSLog(@"Imported");
    }
    @catch (NSException * e)
    {
        NSLog(@"Problem loading file");
    }
    
}


- (void) createROIsFromURL: (NSString*) filePath{
    
    @try
    {
        NSFileHandle * fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
        NSString* input = [[NSString alloc] initWithData: [fileHandle availableData] encoding: NSUTF8StringEncoding];
        NSArray* lines = [input componentsSeparatedByString: @"\n"];
        
        for (int iLine = 0; iLine < [lines count]; iLine++)
        {
            if ([[lines objectAtIndex:iLine] hasPrefix: @"[XYCONTOUR]"])
            {
                NSArray *components1 = [[lines objectAtIndex:iLine+1] componentsSeparatedByString:@" "];
                int iSlice = [viewerController maxMovieIndex]-[[components1 objectAtIndex:0] integerValue]-1;
                int iTime = [[components1 objectAtIndex:1] integerValue];
                int ROItype = [[components1 objectAtIndex:2] integerValue];
                int nPoints = [[lines objectAtIndex:iLine+2] integerValue];
                NSMutableArray  *points = [[NSMutableArray alloc] initWithCapacity:nPoints];
                
                for (int iPoint = 0; iPoint < nPoints; iPoint++)
                {
                    NSArray *components2 = [[lines objectAtIndex:iLine+3+iPoint] componentsSeparatedByString:@" "];
                    float x = [[components2 objectAtIndex:0] floatValue];
                    float y = [[components2 objectAtIndex:1] floatValue];
                    [points addObject: [viewerController newPoint: x : y]];
                }
                
                int myROINamesIDX;
                
                switch (ROItype){
                    case 0: myROINamesIDX = 1; break;
                    case 1: myROINamesIDX = 0; break;
                    case 2: myROINamesIDX = 2; break; 
                    case 5: myROINamesIDX = 3; break;
                }
                
                NSMutableArray  *roiSeriesList  = [viewerController roiList: iSlice];
                NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: iTime];
                
                // check if given ROI exists. If so, delete it
                [roiHandler checkIfROIExistsAndIfSoDeleteItOnFrame:roiImageList nameNumber:myROINamesIDX];
                
                ROI *newROI = [roiHandler createNewROISimpleWithPoints:points type:tCPolygon nameNumber:myROINamesIDX colorNumber:myROINamesIDX];
                
                [roiImageList addObject: newROI];
                
                NSLog(@"[XYCONTOUR] in line %d in Slice %d in Time %d ROI %d nPoints %d",iLine,iSlice,iTime,ROItype,nPoints);
            }
        }
        NSLog(@"Imported");
    }
    @catch (NSException * e)
    {
        NSLog(@"Problem loading file");
    }

}


- (void) exportQmass{
    
    // create the save panel
    NSSavePanel *panel = [NSSavePanel savePanel];
    
    // set a new file name
    [panel setNameFieldStringValue:@"ExportedFile.con"];
    [panel setShowsTagField:NO];
    [panel setCanCreateDirectories:YES];
    [panel setAllowedFileTypes:[NSArray arrayWithObjects:@"con",nil]];
    [panel setAllowsOtherFileTypes:NO];
    
    // display the panel
    //[panel beginWithCompletionHandler:^(NSInteger result) {
    [panel beginSheetModalForWindow:mainWindow completionHandler:^(NSInteger result) {
        
        if (result == NSFileHandlingPanelOKButton) {
            
            // create a file namaner and grab the save panel's returned URL
            //NSFileManager *manager = [NSFileManager defaultManager];
            NSURL *saveURL = [panel URL];
            
            NSArray *dataArray = [self exportCreateArrayOfData];
            NSString *outputString = @"";
            
            NSLog(@"%@",outputString);
            
            outputString = [dataArray componentsJoinedByString:@"\n"];
            
            [outputString writeToURL:saveURL atomically:YES encoding:NSStringEncodingConversionAllowLossy error:nil];
            
            
        }
    }];
    
}

- (NSArray*) exportCreateArrayOfData{
    
    NSMutableArray* header = [[NSMutableArray alloc] initWithObjects:
                                @"Version = 2.2",
                                @"[MAIN]",
                                @"Package=MASS",
                                @"Version=7.6",
                                @"Build=32",
                                nil];
    NSArray* newArray = header;
    
    NSMutableArray* studyInformation = [[NSMutableArray alloc] initWithObjects:
                                @"[STUDY_INFORMATION]",
                                [NSString stringWithFormat:@"Patient_name=%@",[roiHandler getDicomDataForTag:@"PatientsName"]],
                                @"Patient_gender=",
                                [NSString stringWithFormat:@"Birth_date=%@",[roiHandler getDicomDataForTag:@"PatientsBirthDate"]],
                                [NSString stringWithFormat:@"Study_date=%@",[roiHandler getDicomDataForTag:@"StudyDate"]],
                                [NSString stringWithFormat:@"Patient_id=%@",[roiHandler getDicomDataForTag:@"PatientID"]],
                                [NSString stringWithFormat:@"Study_UID=%@",[roiHandler getDicomDataForTag:@"StudyInstanceUID"]],
                                [NSString stringWithFormat:@"Study_id=%@",[roiHandler getDicomDataForTag:@"StudyID"]],
                                [NSString stringWithFormat:@"Study_description=%@",[roiHandler getDicomDataForTag:@"StudyDescription"]],
                                [NSString stringWithFormat:@"Series=%@",[roiHandler getDicomDataForTag:@"SeriesNumber"]],
                                [NSString stringWithFormat:@"Series_UID=%@",[roiHandler getDicomDataForTag:@"SeriesInstanceUID"]],
                                [NSString stringWithFormat:@"Series_description=%@",[roiHandler getDicomDataForTag:@"SeriesDescription"]],
                                [NSString stringWithFormat:@"Acquisition=%@",[roiHandler getDicomDataForTag:@"AcquisitionNumber"]],
                                [NSString stringWithFormat:@"Reconstruction_number=%@",[roiHandler getDicomDataForTag:@"ReconstructionMethod"]],
                                [NSString stringWithFormat:@"Field_of_view=%@",[roiHandler getDicomDataForTag:@"FieldOfViewDimensions"]],
                                [NSString stringWithFormat:@"Image_resolution=%@",[roiHandler getDicomDataForTag:@"SpatialResolution"]],
                                [NSString stringWithFormat:@"Slicethickness=%@",[roiHandler getDicomDataForTag:@"SliceThickness"]],
                                [NSString stringWithFormat:@"Slicegap=%@",[roiHandler getDicomDataForTag:@"SpacingBetweenSlices"]],
                                [NSString stringWithFormat:@"Interphase delay=%@",[roiHandler getDicomDataForTag:@"PhaseDelay"]],
                                [NSString stringWithFormat:@"First_delay=%@",[roiHandler getDicomDataForTag:@"ImageTriggerDelay"]],
                                [NSString stringWithFormat:@"Heart_rate=%@",[roiHandler getDicomDataForTag:@"HeartRate"]],
                                @"Blood_type=",
                                [NSString stringWithFormat:@"Patient_weight=%@",[roiHandler getDicomDataForTag:@"PatientsWeight"]],
                                @"Patient_height=",
                                @"Series_finding_label=",
                                nil];
    newArray = [newArray arrayByAddingObjectsFromArray:studyInformation];
    
    NSMutableArray* userInformation = [[NSMutableArray alloc] initWithObjects:
                                @"[USER_INFORMATION]",
                                @"User_login=",
                                @"User_fullname=",
                                @"User_title=",
                                @"User_email=",
                                nil];
    newArray = [newArray arrayByAddingObjectsFromArray:userInformation];
    
    
    NSMutableArray* quantification = [[NSMutableArray alloc] initWithObjects:
                                @"[QUANTIFICATION]",
                                [NSString stringWithFormat:@"slice_orientation=%@",[roiHandler getDicomDataForTag:@"ImageOrientation"]],
                                @"volume.method=",
                                @"functional_segmentation_bloodmuscle=",
                                @"normalvalues.setName=",
                                @"normalvalues.ageMin=",
                                @"normalvalues.ageMax=",
                                @"normalvalues.fieldStrength=",
                                @"normalvalues.gender=",
                                @"StudyType=",
                                @"perfusion.t0_phase=",
                                @"perfusion.tend_phase=",
                                @"segmentation_autoCopy=",
                                @"transmurality_threshold=",
                                @"dsi_analysis_type=",
                                @"dsi_threshold_calc_method=",
                                @"dsi_num_standard_dev=",
                                @"dsi_num_standard_dev_grayzone=",
                                @"t2star_cutoff=",
                                @"t2star_autocopy=",
                                @"t1_autocopy=",
                                @"t1_acquisition_type=",
                                nil];
    newArray = [newArray arrayByAddingObjectsFromArray:quantification];
    
    // CONTOUR DETECTION
    NSMutableArray* contourDetection = [[NSMutableArray alloc] initWithObjects:
                                @"[CONTOURDETECTION]",
                                @"selection.use_matrix=1",
                                nil];
    
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        NSString* addedString = @"selection.slice_";
        if (curSlice<9){
            addedString = [addedString stringByAppendingString:@"000"];
        }
        else if (curSlice >=9 && curSlice < 99){
            addedString = [addedString stringByAppendingString:@"00"];
        }
        else if (curSlice>=99 && curSlice < 999){
            addedString = [addedString stringByAppendingString:@"0"];
        }
        else{
            
        }
        
        addedString = [addedString stringByAppendingFormat:@"%d=",curSlice+1];
        
        if ([roiHandler checkIfSliceHasAnyContourAtAnyFrame:curSlice]){
            for (int i = 0; i <[self getFramesCount]; i++){
                addedString = [addedString stringByAppendingString:@"1"];
            }
        }
        else{
            for (int i = 0; i <[self getFramesCount]; i++){
                addedString = [addedString stringByAppendingString:@"1"];
            }
            //addedString = [addedString stringByAppendingString:@"0111111111111111111111111"];
        }
        [contourDetection addObject:addedString];
    }
    
    newArray = [newArray arrayByAddingObjectsFromArray:contourDetection];

    //DISK SUMATION LABELS
    NSMutableArray* diskSummationLabels = [[NSMutableArray alloc] initWithObjects:
                                @"[DISK_SUMMATION_LABELS]",
                                @"ROI1=Left Atrium",
                                @"ROI2=Right Atrium",
                                @"ROI3=Custom Volume 3",
                                @"ROI4=Custom Volume 4",
                                nil];
    newArray = [newArray arrayByAddingObjectsFromArray:diskSummationLabels];
    
    // XY CONTOUR
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        for (int curFrame = 0; curFrame < [self getFramesCount]; curFrame++){
            NSMutableArray *tempArray = [roiHandler roiPointsForQmassExportSlice:[self getSlicesCount]-curSlice-1 frame:curFrame];
            newArray = [newArray arrayByAddingObjectsFromArray:tempArray];
        }
    }
    
    //POINTS
    for (int curSlice = 0; curSlice < [self getSlicesCount]; curSlice++){
        for (int curFrame = 0; curFrame < [self getFramesCount]; curFrame++){
            NSMutableArray *tempArray = [roiHandler roiSegPointForQmassExportSlice:curSlice frame:curFrame];
            newArray = [newArray arrayByAddingObjectsFromArray:tempArray];
        }
    }
    
    return newArray;
}

- (void) semiAutoSeg: (double) multiplier slice:(int) s frame:(int) t{
    
    [itkHandler performSemiAutoSEG:s frame:t point:segStartPoint multiplierValue:multiplier];
    
    [viewerController needsDisplayUpdate];
    
}

- (void) semiAutoSeg: (double) multiplier{

    [viewerController needsDisplayUpdate];
    
    //Init
    [itkHandler performSemiAutoSEG:0 frame:0 point:segStartPoint multiplierValue:multiplier];
    
    //Loop over all slices and time frames
    for(int s = slicesFrom.intValue-1; s<slicesTo.intValue; s++){
        for(int t=framesFrom.intValue-1; t<framesTo.intValue;t++){
            [self semiAutoSeg:multiplier slice:s frame:t];
        }
    }
    
}

- (void) setSegPoint{
    int         curTimeFrame =  [[viewerController imageView] curImage];
    int         curSlice =  [viewerController curMovieIndex];
    NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
    
    // change ROI tool to point
    [viewerController setROIToolTag:t2DPoint];
    
    // settingTheName
    for (int i = 0; i < [roiImageList count]; i++)
    {
        
        ROI* roi = [roiHandler setNameForSelectedRoiIfPointTypeAndGet:[roiImageList objectAtIndex: i] name:@"SegStart"];
        if (roi!=NULL){
            
            segStartPoint.x = [roi pointAtIndex:0].x;
            segStartPoint.y = [roi pointAtIndex:0].y;
        }
        
    }
    
}

- (void) morphoClose{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    for(int s=0; s<slices; s++){
        for(int t=0; t<frames;t++){
            [itkHandler morphoClose:s frame:t];
        }
    }
    
    [viewerController needsDisplayUpdate];

    
}

- (void) holeFilling{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    for(int s=0; s<slices; s++){
        for(int t=0; t<frames;t++){
            [itkHandler holeFilling:s frame:t];
        }
    }
    [viewerController needsDisplayUpdate];
    
}

- (void) createBrushROIFromITKImages:(int) s frame:(int) t{

        NSMutableArray *pixList = [viewerControllerSeg pixList:s];
        [self changeSlice:s];
    
        DCMPix *curPix = [pixList objectAtIndex:t];
            
        float           PixHeight = [curPix pheight];
        float           PixWidth = [curPix pwidth];
        float           SumOfPixel = PixWidth * PixHeight;
            
        unsigned char *textureBuffer;
            
        float sizeOfTextureBuffer = SumOfPixel*sizeof(unsigned char);
            
        textureBuffer = (unsigned char*)malloc(sizeOfTextureBuffer);
            
        for (int x=0; x< SumOfPixel; x++) textureBuffer[x] = 0x00;
        for (int x=0; x< SumOfPixel; x++)
        {
            textureBuffer[x] = 0x00;
        }
            
        int curPos = [curPix pheight] * [curPix pwidth];
            
        float *fImage = [curPix fImage];
            
        while ( curPos--> 0 )
        {
            float GreyValue;
                
            // Reading Pixel
            GreyValue = fImage[curPos];
                
            if (GreyValue>0){
                textureBuffer[curPos] = 0xFF;
            }
                
        }
            
        [self changeFrame:t];
            
        [viewerController addPlainRoiToCurrentSliceFromBuffer:textureBuffer withName:@"SEG-AUTO"];
        free(textureBuffer);
    
}


- (void) createBrushROIFromITKImages:(int) mode{
    
    
    for (int curSlice = slicesFrom.intValue-1; curSlice < slicesTo.intValue; curSlice++){
        
        NSMutableArray *pixList = [viewerControllerSeg pixList:curSlice];
        [self changeSlice:curSlice];
        
        for (int curFrame = framesFrom.intValue-1; curFrame < framesTo.intValue; curFrame++){
            
            DCMPix *curPix = [pixList objectAtIndex:curFrame];
            
            float           PixHeight = [curPix pheight];
            float           PixWidth = [curPix pwidth];
            float           SumOfPixel = PixWidth * PixHeight;
            
            unsigned char *textureBuffer;
            
            float sizeOfTextureBuffer = SumOfPixel*sizeof(unsigned char);
            
            textureBuffer = (unsigned char*)malloc(sizeOfTextureBuffer);
            
            for (int x=0; x< SumOfPixel; x++) textureBuffer[x] = 0x00;
            for (int x=0; x< SumOfPixel; x++)
            {
                textureBuffer[x] = 0x00;
            }
            
            int curPos = [curPix pheight] * [curPix pwidth];
            
            float *fImage = [curPix fImage];
            
            while ( curPos--> 0 )
            {
                float GreyValue;
                
                // Reading Pixel
                GreyValue = fImage[curPos];
                if (mode == 0){
                    if (GreyValue>0){
                        textureBuffer[curPos] = 0xFF;
                    }
                }
                if (mode == 1){
                    if (GreyValue<100){
                        textureBuffer[curPos] = 0xFF;
                    }
                }
                if (mode == 2){
                    if (GreyValue>20){
                        textureBuffer[curPos] = 0xFF;
                    }
                }
                
                
                
            }
            
            [self changeFrame:curFrame];
            
            [viewerController addPlainRoiToCurrentSliceFromBuffer:textureBuffer withName:@"SEG-AUTO"];
            free(textureBuffer);
            
        }
    }
    
    
}

- (void) performConvexHull{
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    for (int s = 0; s < slices; s++){
        for (int f = 0; f < frames; f++){
            @try{
                [roiHandler convexHull:s frame:f];
            }
            @catch(NSException* e){
                int a = 10;
                a = 10+10;
            }
        }
    }
}
- (NSMutableArray*) searchAllSlicesForROIClassesWithName:(NSString*) name{
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    NSMutableArray *segmentationROIArray = [NSMutableArray new];
    
    for(int s=0; s<slices; s++){
        for(int t=0; t<frames;t++){
            ROI *currentROI = [roiHandler searchROIWithNameInSliceInFrame:name slice:s frame:t];
            
            if (currentROI!=NULL || currentROI!=nil || [currentROI textureBuffer] != NULL){
                ROIClass *currentROIClass = [[ROIClass alloc] init];
                [currentROIClass setRoi:currentROI];
                [currentROIClass setSliceNumber:s];
                [currentROIClass setFrameNumber:t];
                [segmentationROIArray addObject:currentROIClass];
            }
        }
    }
    
    return segmentationROIArray;
    
}

- (void) finalOptimalization{
    
    NSMutableArray *segmentationROIArray = [self searchAllSlicesForROIClassesWithName:resultROIName.stringValue];
    NSMutableArray *resultsArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for(int iteration = 0; iteration < [segmentationROIArray count]; iteration++){
        NSMutableArray *aproxROIS = [[NSMutableArray alloc] initWithCapacity:0];
        ROIClass *curROI = [segmentationROIArray objectAtIndex:iteration];
        ROIClass *nextROI;
        int newcounter = 0;
        if (iteration == [segmentationROIArray count] - 1){
            //nextROI = [segmentationROIArray objectAtIndex:newcounter];
            break;
        }else{
            newcounter = iteration+1;
            nextROI = [segmentationROIArray objectAtIndex:iteration+1];
        }
        double division = nextROI.roi.roiArea/curROI.roi.roiArea;
        if (division >= 1.6 && division < 20.0){
            [aproxROIS addObject:curROI];
            ROIClass *newNextROI;
            while (division >=1.6){
                newcounter++;
                if (newcounter == [segmentationROIArray count]){
                    newcounter = 0;
                }
                newNextROI= [segmentationROIArray objectAtIndex:newcounter];
                division = newNextROI.roi.roiArea/curROI.roi.roiArea;
            }
            [aproxROIS addObject:newNextROI];
            [resultsArray addObject:aproxROIS];
            if (newcounter > iteration ){
                iteration = newcounter-1;
            }
            
        }
        
    }
    
    for (int iteration = 0; iteration<[resultsArray count];iteration++){
        NSMutableArray *aproxArray = [resultsArray objectAtIndex:iteration];
        if ([aproxArray count] == 2){
            ROIClass *one = [aproxArray objectAtIndex:0];
            ROIClass *two = [aproxArray objectAtIndex:1];
            if (one != nil && two != nil && two.frameNumber>one.frameNumber+1 && two.frameNumber<one.frameNumber+16){
                one.roi = [viewerController convertBrushROItoPolygon:one.roi numPoints:20];
                two.roi = [viewerController convertBrushROItoPolygon:two.roi numPoints:20];
                
                [self approximateBetweenTwoROIs:one secondROI:two directionsNumber:0];
            }

        }
    }
    
    // Aproksymacja udana
    NSMutableArray *approxedROIs = [self searchAllSlicesForROIClassesWithName:@"Unnamed (a)"];
    for (int object = 0; object<[approxedROIs count];object++){
        ROIClass *newROI = [approxedROIs objectAtIndex:object];
        ROI *roiToDelete = [roiHandler searchROIWithNameInSliceInFrame:resultROIName.stringValue slice:newROI.sliceNumber frame:newROI.frameNumber];
        if (roiToDelete != NULL){
            [viewerController deleteROI:roiToDelete];
            ROI *brush = [viewerController convertPolygonROItoBrush:newROI.roi];
            [brush setName:resultROIName.stringValue];
            [roiHandler addROI:brush sliceNumber:newROI.sliceNumber frameNumber:newROI.frameNumber];
        }
       
    }
    
    
}


- (void) searchForBadSegmentation{
    
    NSMutableArray *segmentationROIArray = [self searchAllSlicesForROIClassesWithName:@"SEG-AUTO"];
    NSMutableArray *segmentationBadROIArray = [NSMutableArray new];

    
    //Median
    float tooBigArea = 0;
    NSMutableArray *areasArray = [NSMutableArray new];
    if ([segmentationROIArray count] > 0){
        for (int i = 0; i < [segmentationROIArray count]; i++){
            ROIClass* currentROIClassTwo = [segmentationROIArray objectAtIndex:i];
            ROI *currentROITwo = currentROIClassTwo.roi;
            [areasArray addObject:[NSNumber numberWithFloat:[currentROITwo roiArea]]];

        }
        NSArray *sorted = [areasArray sortedArrayUsingSelector:@selector(compare:)];
        NSUInteger middle = [sorted count] / 2;
        NSNumber *median = [sorted objectAtIndex:middle];
        tooBigArea = median.floatValue*2;

        for (int i = 0; i < [segmentationROIArray count]; i++){
            ROIClass* currentROIClassThree = [segmentationROIArray objectAtIndex:i];
            ROI *currentROIThree = currentROIClassThree.roi;
            if (currentROIThree.roiArea >tooBigArea){
                [segmentationBadROIArray addObject:currentROIClassThree];
            }
        }
    }
    

    //Optimize until the results are better
    float multiplier = itkHandler.regionMultiplier.doubleValue;
    while ([segmentationBadROIArray count] > 0){
        multiplier = multiplier*0.9;
        for (int curROI = 0; curROI < [segmentationBadROIArray count]; curROI++){
            
            ROIClass *curROIClassFour = [segmentationBadROIArray objectAtIndex:curROI];
            [self semiAutoSeg:multiplier slice:curROIClassFour.sliceNumber frame:curROIClassFour.frameNumber];
            [self createBrushROIFromITKImages:curROIClassFour.sliceNumber frame:curROIClassFour.frameNumber];
            [viewerController deleteROI:curROIClassFour.roi];
            ROI* currentROIFour = [roiHandler searchROIWithNameInSliceInFrame:@"SEG-AUTO" slice:curROIClassFour.sliceNumber frame:curROIClassFour.frameNumber];
            if (currentROIFour == NULL){
                break;
            }
            if (currentROIFour.roiArea < tooBigArea){
                [segmentationBadROIArray removeObjectAtIndex:curROI];
            }
            else{
                [viewerController deleteROI:currentROIFour];
            }
            
        }
        if (multiplier <=1){
            break;
        }

    }
}

- (float) getPointMeanValue{
    
    NSArray *arrayOfPoints = [viewerController roisWithName:@"SegStart"];
    if ([arrayOfPoints count] == 0){
        return 0.0;
    }
    ROI *pointROI = [arrayOfPoints objectAtIndex:0];
    MyPoint *pointFromROI = [pointROI.points objectAtIndex:0];
    float pointX = pointFromROI.x;
    float pointY = pointFromROI.y;
    
    int size = 4;
    NSMutableArray *arrayOfPointValues = [NSMutableArray new];
    
    for (int x = 0; x < size; x++){
        for (int y= 0; y < size; y++){
            CGFloat newX = (pointX - (size/2) + x);
            CGFloat newY = (pointY - (size/2) + y);
            NSPoint newPointcur = NSPointFromCGPoint(CGPointMake(newX,newY));
            MyPoint *pointToAdd = [MyPoint point:newPointcur];
            NSMutableArray *newArrayForROI = [NSMutableArray arrayWithObject:pointToAdd];
            [pointROI setPoints:newArrayForROI];
            [arrayOfPointValues addObject:[NSNumber numberWithFloat:pointROI.mean]];
        }
    }
    
    //powrot do wlasciwego
    NSMutableArray *newArray = [NSMutableArray new];
    NSPoint newPoint = NSPointFromCGPoint(CGPointMake(pointX, pointY));
    NSValue *newPointValue = [NSValue valueWithPoint:newPoint];
    [newArray addObject: newPointValue];
    [pointROI setPoints:newArray];
    
    NSArray *sorted = [arrayOfPointValues sortedArrayUsingSelector:@selector(compare:)];
    NSUInteger middle = [sorted count] / 2;
    NSNumber *median = [sorted objectAtIndex:middle];
    
    return [median floatValue];
    
}

- (void) shapeDetectionLevelSetFilter{
    
    [viewerController needsDisplayUpdate];
    
    int slices = [self getSlicesCount];
    int frames = [self getFramesCount];
    
    [itkHandler ShapeDetectionLevelSetFilter:0 frame:0 point:segStartPoint];
    
    for(int s = 0; s<slices; s++){
        for(int t=0; t<frames;t++){
            [itkHandler ShapeDetectionLevelSetFilter:s frame:t point:segStartPoint];
        }
    }
    [self createBrushROIFromITKImages:2];
    
}

- (void) exportDiceResults: (NSMutableArray*) diceResults{
    
    // create the save panel
    NSSavePanel *panel = [NSSavePanel savePanel];
    
    // set a new file name
    [panel setNameFieldStringValue:@"DiceResults.csv"];
    [panel setShowsTagField:NO];
    [panel setCanCreateDirectories:YES];
    [panel setAllowedFileTypes:[NSArray arrayWithObjects:@"csv",nil]];
    [panel setAllowsOtherFileTypes:NO];

    [panel beginSheetModalForWindow:mainWindow completionHandler:^(NSInteger result) {
        
        if (result == NSFileHandlingPanelOKButton) {
            
            NSURL *saveURL = [panel URL];
            
            NSArray *dataArray = diceResults;
            NSString *outputString = @"";
            
            outputString = [outputString stringByAppendingString:@"Total;Union(jaccard);Mean(dice);Volume sim;False Negative;False Positive;Slice number;Frame number;\n"];
            
            for (int object=0; object <[dataArray count];object++){
                NSArray *row = [dataArray objectAtIndex:object];
                for (int column=0; column<8; column++){
                    NSNumber *number = [row objectAtIndex:column];
                    outputString = [outputString stringByAppendingString:[number stringValue]];
                    outputString = [outputString stringByAppendingString:@";"];
                }
                outputString = [outputString stringByAppendingString:@"\n"];
            }
            
            [outputString writeToURL:saveURL atomically:YES encoding:NSStringEncodingConversionAllowLossy error:nil];
        }
    }];
    
}

- (void) countDice{
    
    //int slices = [self getSlicesCount];
    //int frames = [self getFramesCount];
    
    NSMutableArray *allResults = [NSMutableArray arrayWithCapacity:1];
    
    for (int curSlice = slicesFrom.intValue-1; curSlice < slicesTo.intValue; curSlice++){
        for (int curFrame = framesFrom.intValue-1; curFrame < framesTo.intValue; curFrame++){
            NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
            NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curFrame];
            ROI* sourceROI = NULL;
            ROI* targetROI = NULL;
            for(int obj = 0; obj < [roiImageList count];obj++){
                ROI* testedROI = [roiImageList objectAtIndex:obj];
                if(testedROI == NULL || testedROI == nil){
                    break;
                }
                if([[testedROI name]isEqualToString:groundTruthROIName.stringValue]){
                    targetROI = testedROI;
                }
                if([[testedROI name]isEqualToString:resultROIName.stringValue]){
                    sourceROI = testedROI;
                }
            }
            if (sourceROI!=NULL && targetROI!=NULL){
                ROI* newTargetROI = [roiHandler convertPolygonToBrush:targetROI];
                NSLog(@"\n #########################DICE RESULTS FOR SLICE: %d FRAME: %d##########################\n",curSlice,curFrame+1);
                DCMPix *pixSource = [sourceROI pix];
                NSMutableArray *results = [itkHandler getLabelOverlapInfo:sourceROI targetROI:newTargetROI pwidth:pixSource.pwidth  pheight:pixSource.pheight];
                [results addObject:[NSNumber numberWithInt:curSlice]];
                [results addObject:[NSNumber numberWithInt:curFrame]];
                [allResults addObject:results];
            }
        }
    }
    [self exportDiceResults: allResults];
    
}

- (void) fullSemiAuto{

    if (segStartPoint.x == 0 && segStartPoint.y == 0){
        return;
    }
    //Run Region Growing with default parameter (multiplier from options menu)
    [self semiAutoSeg:0];
    //Create Brush ROIs
    [self createBrushROIFromITKImages:0];
    
    //Region Growing Optimalization
    if (regionGrowingOptimalization.state == NSControlStateValueOn){
        [self searchForBadSegmentation];
    }
    
    [viewerController refresh];
    [viewerControllerSeg refresh];
    
    
    //----------------Region growing save---------
    NSMutableArray *regionGrowingROIsToCopy = [self searchAllSlicesForROIClassesWithName:@"SEG-AUTO"];
    for (int curROI = 0; curROI < [regionGrowingROIsToCopy count];curROI++){
        ROIClass *roiToCopy = [regionGrowingROIsToCopy objectAtIndex:curROI];
        ROI *newROI = [roiHandler createBrushROISimiliarToBrushROIWithTextureBuffer:roiToCopy.roi];
        [newROI setName:@"SEG-AUTO-RG-COPY"];
        [roiHandler addROI:newROI sliceNumber:roiToCopy.sliceNumber frameNumber:roiToCopy.frameNumber];
    }
    NSMutableArray *regionGrowingROIs = [self searchAllSlicesForROIClassesWithName:@"SEG-AUTO-RG-COPY"];
    
    //--------------SHAPE DETECTION
    if (edgeDetectionEnable.state == NSControlStateValueOn){
        
        [viewerController needsDisplayUpdate];
        [itkHandler ShapeDetectionLevelSetFilter:slicesFrom.intValue-1 frame:framesFrom.intValue-1 point:segStartPoint];
        
        for(int s = slicesFrom.intValue-1; s<slicesTo.intValue; s++){
            for(int t=framesFrom.intValue-1; t<framesTo.intValue;t++){
                [itkHandler ShapeDetectionLevelSetFilter:s frame:t point:segStartPoint];
            }
        }
        [self createBrushROIFromITKImages:2];
        
        
        //---------ŁĄCZENIE TYCH SAMYCH
        for (int s = 0; s< [self getSlicesCount];s++){
            for (int f = 0; f < [self getFramesCount]; f++){
                [roiHandler mergeAllROIsWithTheSameName:@"SEG-AUTO" slice:s frame:f];
            }
        }
        
        //-----------ROZROST PUNKTU MOIM ALGORYTMEM
        for (int s = 0; s< [self getSlicesCount];s++){
            for (int f = 0; f < [self getFramesCount]; f++){
                ROI *referenceROI = [roiHandler searchROIWithNameInSliceInFrame:@"SEG-AUTO" slice:s frame:f];
                if (referenceROI!=NULL){
                    ROI *newROI = [roiHandler createBrushROISimiliarToBrushROI:referenceROI];
                    
                    float pixWidthROI1 = [referenceROI textureWidth];
                    int textureUpLeftCornerXROI1 = [newROI textureUpLeftCornerX];
                    int textureUpLeftCornerYROI1 = [newROI textureUpLeftCornerY];
                    
                    int y = segStartPoint.y;
                    int x = segStartPoint.x;
                    
                    int position = (y-textureUpLeftCornerYROI1)*pixWidthROI1+(x-textureUpLeftCornerXROI1);
                    [newROI textureBuffer][position] = 0xFF;
                    
                    [roiHandler pointGrow:newROI reference:referenceROI point:segStartPoint];
                    [roiHandler addROI:newROI sliceNumber:s frameNumber:f];
                }
            }
        }
        
        // Check if shape detection didn't wend bad
        for (int curROI = 0; curROI < [regionGrowingROIs count];curROI++){
            ROIClass *regionGrowROI = [regionGrowingROIs objectAtIndex:curROI];
            int slice = regionGrowROI.sliceNumber;
            int frame = regionGrowROI.frameNumber;
            ROI *connectedROI = [roiHandler searchROIWithNameInSliceInFrame:@"PointGrow" slice:slice frame:frame];
            float areaOne = connectedROI.roiArea;
            float areaTwo = regionGrowROI.roi.roiArea;
            float division = areaOne/areaTwo;
            if (division >= 2.5){
                //[connectedROI setName:@"PointGrow-Old"];
                [viewerController deleteROI:connectedROI];
                [regionGrowROI.roi setName:@"PointGrow"];
            }
        }
        
    }


    if (convexHullEnable.state == NSControlStateValueOn){
        [self performConvexHull];
        [self deleteAllROIsWithName:@"SEG-AUTO"];
    }
    
    [self deleteAllROIsWithName:@"PointGrow"];
    [self deleteAllROIsWithName:@"SEG-AUTO-RG-COPY"];
    
    
    if(finalOptimalizationEnable.state == NSControlStateValueOn){
        [self finalOptimalization];
        [self finalOptimalization];
    }
    [self deleteAllROIsWithName:@"Unnamed (a)"];
    [self deleteAllROIsWithName:@"Unnamed (a)"];
    
    [viewerController refresh];
    
    
}

@end
