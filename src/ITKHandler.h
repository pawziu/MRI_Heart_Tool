#import "ROIClass.h"
#import "MathHandler.h"

@interface ITKHandler : NSObject{
    ViewerController *viewerController;
    ViewerController *viewerControllerSeg;
    MathHandler *mathHandler;
    
    long bufferSize;
    
    DCMPix      *firstPix;
    int         times;
    int         slices;
    
    
    NSTextField *regionMultiplier;
    NSTextField *regionIterations;
    NSTextField *regionInitialRadius;
    
    NSTextField *edgeTimeStep;
    NSTextField *edgeIterations;
    NSTextField *edgeConductance;
    NSTextField *edgeSigma;
    NSTextField *edgeAlpha;
    NSTextField *edgeBeta;
    NSTextField *edgeStoppingValue;
    NSTextField *edgePropagationScalling;
    NSTextField *edgeCurvatuveScalling;
    
}

@property (nonatomic, strong) ViewerController *viewerController;
@property (nonatomic, strong) ViewerController *viewerControllerSeg;
@property (nonatomic, strong) MathHandler *mathHandler;

@property (nonatomic, strong) NSTextField *regionMultiplier;
@property (nonatomic, strong) NSTextField *regionIterations;
@property (nonatomic, strong) NSTextField *regionInitialRadius;

@property (nonatomic, strong) NSTextField *edgeTimeStep;
@property (nonatomic, strong) NSTextField *edgeIterations;
@property (nonatomic, strong) NSTextField *edgeConductance;
@property (nonatomic, strong) NSTextField *edgeSigma;
@property (nonatomic, strong) NSTextField *edgeAlpha;
@property (nonatomic, strong) NSTextField *edgeBeta;
@property (nonatomic, strong) NSTextField *edgeStoppingValue;
@property (nonatomic, strong) NSTextField *edgePropagationScalling;
@property (nonatomic, strong) NSTextField *edgeCurvatuveScalling;

/** Init this class with connection to ViewerController from Horos */
- (id) initWithViewerController: (ViewerController*) pointer mathHandler: (MathHandler*) pointerTwo vcs:(ViewerController*) pointerThree;
/** Semi-auto segmentation function for specific slice, time frame, starting point and multiplier value for region growing algorithm */
- (void) performSemiAutoSEG: (int) s frame:(int) t point:(NSPoint) point multiplierValue:(double) multiplier;

/** Morphological closing on ITK image (ViewerControllerSeg) */
- (void) morphoClose: (int) s frame:(int) t;
/** Hole Filling on ITK image */
- (void) holeFilling: (int) s frame:(int) t;

/** Edge detection on specific slice, time frame and point */
- (void) ShapeDetectionLevelSetFilter: (int) s frame:(int) t point:(NSPoint) point;

/** Calculate similarity measures from two rois */
- (NSMutableArray*)getLabelOverlapInfo: (ROI*) sourceROI targetROI:(ROI*) targetROI pwidth:(int) pwidth pheight:(int) pheight;

/** Calculate Convex Hull from set of points*/
- (int) convexHull2D: (NSPoint*) P numberOfPoints: (int) n outputArray:(NSPoint*) H;

@end
