#import "ROIHandler.h"

@implementation ROIHandler

@synthesize viewerController;
@synthesize mathHandler;
@synthesize myROINames;
@synthesize resultROIName;

- (id) initWithViewerController: (ViewerController*) pointer mathHandler: (MathHandler*) pointerTwo{
    if (self = [super init]){
        
        viewerController = pointer;
        mathHandler = pointerTwo;
        
        myROINames = [[NSArray alloc] initWithObjects:@"LVEPI",@"LVENDO",@"RVEPI",@"RVENDO",@"LVEPI (a)",@"LVENDO (a)",@"RVEPI (a)",@"RVENDO (a)",@"SegPoint",@"SECTOR 1",@"SECTOR 7",@"SECTOR 13",@"Imported", nil];
        
    }
    return self;
    
}

- (void) drawROI:(id) sender{
    
    //declare colors
    RGBColor myROIColors[] = {
        {0, 65535, 0},
        {65535, 0, 0},
        {65535, 65535, 0},
        {0, 65535,65535},
    };
    
    // get identifier of sender button
    NSString* buttonIdentifier = [sender identifier];
    
    NSLog(@"Buton Identifier: %@",buttonIdentifier);
    
    short myROINamesIDX = [myROINames indexOfObject:buttonIdentifier];
    
    // get ROI list from current slice and time frame (roiImageList)
    int         curTimeFrame =  [[viewerController imageView] curImage];
    int         curSlice =  [viewerController curMovieIndex];
    NSMutableArray  *roiSeriesList  = [viewerController roiList: curSlice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
    
    // change ROI tool to tcpolygon
    [viewerController setROIToolTag:tCPolygon];
    
    
    // loop over ROIs
    for (int i = 0; i < [roiImageList count]; i++)
    {
        // find selected/drawing roi
        ROI *curROI = [roiImageList objectAtIndex: i];
        if ((([curROI ROImode]==ROI_selected) || ([curROI ROImode]==ROI_selectedModify) || ([curROI ROImode]==ROI_drawing)) && ([curROI type]==tCPolygon)){
            // set ROI name
            [curROI setName: [myROINames objectAtIndex:myROINamesIDX]];
            // ser ROI color
            [curROI setColor: myROIColors[myROINamesIDX]];
            // if roiMode was ROI_drawing, change to ROI_selected
            [curROI setROIMode:ROI_selected];
            // change ROI tool to tcpolygon
            [viewerController setROIToolTag:tCPolygon];
            
            NSLog(@"Selected ROI area: %g, mode: %ld",[curROI roiArea], [curROI ROImode]);
        }
    }
    
}

-(ROI *)    brushROIProduct: (ROI *) ROI1 and: (ROI *) ROI2 and: (NSString *) ROIName and: (float) pixWidth and: (float) pixHeight and: (float) pixelSpacingX and: (float) pixelSpacingY and: (NSPoint) imageOrigin operation:(OperationOnROI) operationType{
    
    unsigned char *texturecurROI = NULL;
    unsigned char *textureROI1 = NULL;
    unsigned char *textureROI2 = NULL;
    
    float pixHeightPRODUCT = pixHeight;
    float pixWidthPRODUCT = pixWidth;
    float sumOfPixelPRODUCT = pixHeightPRODUCT * pixWidthPRODUCT;
    int textureUpLeftCornerXPRODUCT = 0;
    int textureUpLeftCornerYPRODUCT = 0;
    float pixelSpacingXPRODUCT = pixelSpacingX;
    float pixelSpacingYPRODUCT = pixelSpacingY;
    NSPoint imageOriginPRODUCT = imageOrigin;
    
    float pixWidthROI1;
    float pixHeightROI1;
    float sumOfPixelROI1;
    int textureUpLeftCornerXROI1;
    int textureUpLeftCornerYROI1;
    
    float pixWidthROI2;
    float pixHeightROI2;
    float sumOfPixelROI2;
    int textureUpLeftCornerXROI2;
    int textureUpLeftCornerYROI2;
    
    //check if brush
    if (ROI1.type == tPlain){
        
        pixWidthROI1 = [ROI1 textureWidth];
        pixHeightROI1 = [ROI1 textureHeight];
        sumOfPixelROI1 = pixHeightROI1 * pixWidthROI1;
        textureUpLeftCornerXROI1 = [ROI1 textureUpLeftCornerX];
        textureUpLeftCornerYROI1 = [ROI1 textureUpLeftCornerY];
        
        texturecurROI = (unsigned char*)malloc(sumOfPixelROI1*sizeof(unsigned char));
        textureROI1 = (unsigned char*)malloc(sumOfPixelPRODUCT*sizeof(unsigned char));
        
        //create existing one
        for (int x=0; x< sumOfPixelROI1; x++){
            
            texturecurROI[x] = [ROI1 textureBuffer][x];
            
        }
        
        //create new one
        for (int x = 0; x < sumOfPixelPRODUCT; x++){
            textureROI1[x] = 0x00;
        }
        
        for (int y = textureUpLeftCornerYROI1; y < (textureUpLeftCornerYROI1 + pixHeightROI1); y++){
            
            for (int x = textureUpLeftCornerXROI1; x < (textureUpLeftCornerXROI1 + pixWidthROI1); x++){
                
                int positionOfPRODUCT = y*pixWidthPRODUCT + x;
                int positionOfROI1 = ((y-textureUpLeftCornerYROI1)*pixWidthROI1) + (x-textureUpLeftCornerXROI1);
                
                if (texturecurROI[positionOfROI1] == 0xFF){
                    textureROI1[positionOfPRODUCT] = 0xFF;
                }
                
            }
        }
        if (texturecurROI != NULL){
            free(texturecurROI);
        }
    }
    
    //ENDO
    
    if (ROI2.type == tPlain){
        pixWidthROI2 = [ROI2 textureWidth];
        pixHeightROI2 = [ROI2 textureHeight];
        sumOfPixelROI2 = pixHeightROI2 * pixWidthROI2;
        textureUpLeftCornerXROI2 = [ROI2 textureUpLeftCornerX];
        textureUpLeftCornerYROI2 = [ROI2 textureUpLeftCornerY];
        
        
        texturecurROI = (unsigned char*)malloc(sumOfPixelROI2*sizeof(unsigned char));
        textureROI2 = (unsigned char*)malloc(sumOfPixelPRODUCT*sizeof(unsigned char));
        
        //create existing one
        for (int x=0; x< sumOfPixelROI2; x++){
            
            texturecurROI[x] = [ROI2 textureBuffer][x];
            
        }
        
        //create new one
        for (int x = 0; x < sumOfPixelPRODUCT; x++){
            textureROI2[x] = 0x00;
        }
        
        for (int y = textureUpLeftCornerYROI2; y < (textureUpLeftCornerYROI2 + pixHeightROI2); y++){
            
            for (int x = textureUpLeftCornerXROI2; x < (textureUpLeftCornerXROI2 + pixWidthROI2); x++){
                
                int positionOfPRODUCT = y*pixWidthPRODUCT + x;
                int positionOfROI2 = ((y-textureUpLeftCornerYROI2)*pixWidthROI2) + (x-textureUpLeftCornerXROI2);
                
                if (texturecurROI[positionOfROI2] == 0xFF){
                    textureROI2[positionOfPRODUCT] = 0xFF;
                }
            }
        }
        if (texturecurROI != NULL){
            free(texturecurROI);
        }
        
    }
    
    if (textureROI1 != NULL && textureROI2 != NULL){
        
        unsigned char *PRODUCTTextureBuffer = NULL;
        PRODUCTTextureBuffer = (unsigned char*)malloc(sumOfPixelPRODUCT*sizeof(unsigned char));
        
        for (int x=0; x < sumOfPixelPRODUCT; x++)
        {
            PRODUCTTextureBuffer[x] = 0x00;
        }
        
        
        for (int x = 0; x < sumOfPixelPRODUCT; x++){
            
            
            if (operationType == AND){
                if (textureROI1[x] == 0xFF && textureROI2[x] == 0xFF){
                    PRODUCTTextureBuffer[x] = 0xFF;
                }
            }
            if (operationType == XOR){
                if (textureROI1[x] == 0xFF && textureROI2[x] == 0x00){
                    PRODUCTTextureBuffer[x] = 0xFF;
                }
            }
        }
        
        ROI *newROI = [[ROI alloc] initWithTexture:PRODUCTTextureBuffer textWidth:pixWidthPRODUCT  textHeight:pixHeightPRODUCT textName:ROIName positionX:textureUpLeftCornerXPRODUCT  positionY:textureUpLeftCornerYPRODUCT spacingX:pixelSpacingXPRODUCT spacingY:pixelSpacingYPRODUCT imageOrigin:imageOriginPRODUCT];
        
        if (textureROI1 != NULL){
            free(textureROI1);
        }
        if (textureROI2 != NULL){
            free(textureROI2);
        }
        if  (PRODUCTTextureBuffer != NULL){
            free(PRODUCTTextureBuffer);
        }
        return newROI;
    }
    
    //end of creating myo
    if (textureROI1 != NULL){
        free(textureROI1);
    }
    if (textureROI2 != NULL){
        free(textureROI2);
    }
    if (texturecurROI != NULL){
        free(texturecurROI);
    }
    return NULL;

}

- (CellState*) getCellState: (int) sliceNumber frame: (int) frameNumber{
    
    CellState *newCellState = [[CellState alloc] init];
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: sliceNumber];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frameNumber];
    
    for (int i = 0; i < [roiImageList count]; i++)
    {
        // find selected/drawing roi
        ROI *curROI = [roiImageList objectAtIndex: i];
        
        if ([[curROI name ] isEqual: ([myROINames objectAtIndex:0])]){
            newCellState.LVEPIcontour = manual;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:1])]){
            newCellState.LVENDOcontour = manual;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:2])]){
            newCellState.RVEPIcontour = manual;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:3])]){
            newCellState.RVENDOcontour = manual;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:4])]){
            newCellState.LVEPIcontour = automatic;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:5])]){
            newCellState.LVENDOcontour = automatic;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:6])]){
            newCellState.RVEPIcontour = automatic;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:7])]){
            newCellState.RVENDOcontour = automatic;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:8])]){
            newCellState.pointSet = YES;
        }
        else if([[curROI name ] isEqual: ([myROINames objectAtIndex:9])] ||
                [[curROI name ] isEqual: ([myROINames objectAtIndex:10])] ||
                [[curROI name ] isEqual: ([myROINames objectAtIndex:11])]){
            newCellState.sectorsCreated = YES;
        }
        else{
            
        }
        
    }

    return newCellState;
    
}

// this applies only to TCPolygonROIs!
- (NSMutableArray*) searchCurSliceForROISWithNameNumber: (int) ROINameNumber curSlice:(int) slice{
    
    NSMutableArray *whereIsROI = [[NSMutableArray alloc] init];
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    
    for (int curTimeFrame = 0;  curTimeFrame< [roiSeriesList count];   curTimeFrame++){
        
        NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curTimeFrame];
        
        for (int curROINumber = 0;  curROINumber< [roiImageList count];   curROINumber++)
        {
            ROI *curROI = [roiImageList objectAtIndex: curROINumber];
            
            if ([[curROI name] isEqual: [myROINames objectAtIndex:ROINameNumber]] && [curROI type] == tCPolygon){
                
                ROIClass *newROIClass = [[ROIClass alloc] init];
                newROIClass.roi = curROI;
                newROIClass.sliceNumber = slice;
                newROIClass.frameNumber = curTimeFrame;
                
                [whereIsROI addObject:newROIClass];
                
            }
        }
    }
    
    return whereIsROI;
}

- (NSMutableArray*) searchCurFrameForROIsWithName:(NSString*) name curSlice:(int) slice frame:(int) frame{
    
    NSMutableArray *whereIsROI = [[NSMutableArray alloc] init];
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
        
    for (int curROINumber = 0;  curROINumber< [roiImageList count];   curROINumber++)
    {
        ROI *curROI = [roiImageList objectAtIndex: curROINumber];
        
        if ([[curROI name] isEqual:name]){
            
            ROIClass *newROIClass = [[ROIClass alloc] init];
            newROIClass.roi = curROI;
            newROIClass.sliceNumber = slice;
            newROIClass.frameNumber = frame;
            
            //[whereIsROI addObject:newROIClass];
            [whereIsROI addObject:curROI];
            
        }
    }
    return whereIsROI;
}

- (void) mergeAllROIsWithTheSameName:(NSString*)name slice:(int) slice frame:(int) frame{
    
    NSMutableArray *sameROIs = [self searchCurFrameForROIsWithName:name curSlice:slice frame:frame];
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
    
    id sender = [[NSMutableArray alloc] init];
    if ([sameROIs count] > 1){
        [viewerController mergeBrushROI:sender ROIs:sameROIs ROIList:roiImageList];
    }
    
    
}


- (ROI*) createROIWithNameNumber: (int) roiNameNumber type: (long) type color:(int) colorNumber points: (NSMutableArray*) points pixelSpacingX: (double) pixelSpacingX imageOrigin: (NSPoint) imageOrigin{
    
    RGBColor myROIColors[] = {
        {0, 65535, 0},
        {65535, 0, 0},
        {65535, 65535, 0},
        {0, 65535,65535},
    };
    
    ROI *newROI = [[ROI alloc] initWithType:type :pixelSpacingX:imageOrigin];
    [newROI setPoints: points];
    [newROI setName:[myROINames objectAtIndex:roiNameNumber]];
    [newROI setColor:myROIColors[colorNumber]];
    
    return newROI;
}
- (ROI*) createROIWithName:(NSString*) roiName type:(long) type color: (RGBColor) color points:(NSMutableArray*) points pixelSpacingX:(double) pixelSpacingX imageOrigin: (NSPoint) imageOrigin{
    
    ROI *newROI = [[ROI alloc] initWithType:type :pixelSpacingX:imageOrigin];
    [newROI setPoints: points];
    [newROI setName:roiName];
    [newROI setColor:color];
    
    return newROI;
}

- (ROI*) createROISimiliarToROI:(ROI*) referenceROI{
    ROI *newROI = [[ROI alloc] initWithType:[referenceROI type] :[referenceROI pixelSpacingX] :[referenceROI imageOrigin]];
    
    return newROI;
}

- (ROI*) createBrushROISimiliarToBrushROI: (ROI*) referenceROI{
    
    //float pixHeightPRODUCT = [[referenceROI pix] pheight];
    float pixHeightPRODUCT = [referenceROI textureHeight];
    float pixWidthPRODUCT = [referenceROI textureWidth];
    //float pixWidthPRODUCT = [[referenceROI pix] pwidth];
    float sumOfPixelPRODUCT = pixHeightPRODUCT * pixWidthPRODUCT;
    
    
    
    //unsigned char *PRODUCTTextureBuffer = NULL;
    unsigned char *PRODUCTTextureBuffer = (unsigned char*)malloc(sumOfPixelPRODUCT*sizeof(unsigned char));
    
    for (int x=0; x < sumOfPixelPRODUCT; x++)
    {
        PRODUCTTextureBuffer[x] = 0x00;
        
    }
    
    ROI *newROI = [[ROI alloc] initWithTexture:PRODUCTTextureBuffer textWidth:pixWidthPRODUCT  textHeight:pixHeightPRODUCT textName:@"PointGrow" positionX:[referenceROI textureUpLeftCornerX]  positionY:[referenceROI textureUpLeftCornerY] spacingX:[referenceROI pixelSpacingX] spacingY:[referenceROI pixelSpacingY] imageOrigin:[referenceROI imageOrigin]];
    
    return newROI;
    
}
- (ROI*) createBrushROISimiliarToBrushROIWithTextureBuffer: (ROI*) referenceROI{
    
    //float pixHeightPRODUCT = [[referenceROI pix] pheight];
    float pixHeightPRODUCT = [referenceROI textureHeight];
    float pixWidthPRODUCT = [referenceROI textureWidth];
    //float pixWidthPRODUCT = [[referenceROI pix] pwidth];
    float sumOfPixelPRODUCT = pixHeightPRODUCT * pixWidthPRODUCT;
    
    
    
    //unsigned char *PRODUCTTextureBuffer = NULL;
    unsigned char *PRODUCTTextureBuffer = (unsigned char*)malloc(sumOfPixelPRODUCT*sizeof(unsigned char));
    
    for (int x=0; x < sumOfPixelPRODUCT; x++)
    {
        PRODUCTTextureBuffer[x] = 0x00;
        //DODANE
        if (referenceROI.textureBuffer[x] == 0xFF){
            PRODUCTTextureBuffer[x] = 0xFF;
        }
    }
    
    ROI *newROI = [[ROI alloc] initWithTexture:PRODUCTTextureBuffer textWidth:pixWidthPRODUCT  textHeight:pixHeightPRODUCT textName:@"PointGrow" positionX:[referenceROI textureUpLeftCornerX]  positionY:[referenceROI textureUpLeftCornerY] spacingX:[referenceROI pixelSpacingX] spacingY:[referenceROI pixelSpacingY] imageOrigin:[referenceROI imageOrigin]];
    
    return newROI;
    
}



- (void) addROI:(ROI*) roi sliceNumber:(int) slice frameNumber:(int) frame{
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
    [roiImageList addObject:roi];
    
}


- (BOOL) checkIfImageHasROI: (ROI*) roi series:(NSMutableArray*) series{
    
    for (int i = 0; i < [series count]; i++){
        ROI* checkedROI = [series objectAtIndex:i];
        if (checkedROI == roi){
            return YES;
        }
    }
    return NO;
}

- (ROI*) searchROIWithROINameNumberInFrame: (int) ROINameNumber image: (NSMutableArray*) imageList{
    
    for (int i = 0; i < [imageList count]; i++){
        ROI* checkedROI = [imageList objectAtIndex:i];
        if ([[checkedROI name] isEqualTo:[myROINames objectAtIndex:ROINameNumber]]){
            return checkedROI;
        }
    }
    
    return NULL;
}

- (ROI*) searchROIWithNameInSliceInFrame: (NSString*) ROIName slice:(int) slice frame:(int) frame{
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];

    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
        
        for (int curROINumber = 0;  curROINumber< [roiImageList count];   curROINumber++){
            
            ROI *curROI = [roiImageList objectAtIndex: curROINumber];
            
            if ([[curROI name] isEqual: ROIName]){
                
                return curROI;
                
            }
            
        }
    return NULL;
    
}

- (ROI*) copyROIToSliceNumberAndFrameNumber: (ROI*) roi sliceNumber:(int) slice frameNumber:(int) frame{
    
    ROI *newROI = [self createROIWithName:[roi name] type:[roi type] color:[roi rgbcolor] points:[roi points] pixelSpacingX:[roi pixelSpacingX] imageOrigin:[roi imageOrigin]];
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
    [roiImageList addObject:newROI];
    return newROI;
    
}

- (NSPoint) createPointForApproximationFromROI: (ROI*) roi edgePoint: (NSPoint*) edgePoint centerPoint: (NSPoint*) centerPoint directionNumber: (int) direction{
    
    NSMutableArray *splinePoints = [roi splinePoints];
    int size = [splinePoints count];
    NSMutableArray *tableOfAngles = [[NSMutableArray alloc] init];
    NSMutableArray *positionArray = [[NSMutableArray alloc] init];
    
    
    int howmanytimes = 0;
    
    for (int curROIPointNumber = 0; curROIPointNumber < size; curROIPointNumber++){
        
        double pointOfInterestX = [[splinePoints objectAtIndex:curROIPointNumber] x];
        double pointOfInterestY = [[splinePoints objectAtIndex:curROIPointNumber] y];
        
        NSPoint pointOfInterest = NSMakePoint(pointOfInterestX, pointOfInterestY);
        // 0.5, 4
        // case direction one - smaller to bigger
        if (direction == 0){
            
            if ([mathHandler doPointsLayOnTheSameLine:centerPoint second:edgePoint third:&pointOfInterest withPrecisionInLine:0.5 withOverallPrecision:4]){
                [tableOfAngles addObject:[NSValue valueWithPoint:pointOfInterest]];
                [positionArray addObject:[NSNumber numberWithInt:curROIPointNumber]];
                howmanytimes ++;
            }
            
        }
        else{
            // case direction two - bigger to smaller
            //bylo 10 overall
            //bylo 0.5 precisionline
            if ([mathHandler doPointsLayOnTheSameLine:centerPoint second:edgePoint third:&pointOfInterest withPrecisionInLine:0.5 withOverallPrecision:10]){
                [tableOfAngles addObject:[NSValue valueWithPoint:pointOfInterest]];
                [positionArray addObject:[NSNumber numberWithInt:curROIPointNumber]];
                howmanytimes ++;
            }
            
        }
        
    }
    
    if(howmanytimes < 2){
        NSLog(@"problem here! caused by centerPoint %f, %f, edge point %f, %f",centerPoint->x, centerPoint->y, edgePoint->x, edgePoint ->y);
        NSPoint newPoint = NSMakePoint(12,12);
        return newPoint;
    }
    
    double distance = 0;
    int iteration = 0;
    while (distance < 3){
        if([tableOfAngles firstObject] == NULL){
            break;
        }
        NSPoint first = [[tableOfAngles objectAtIndex:0] pointValue];
        NSPoint second = [[tableOfAngles objectAtIndex:iteration] pointValue];
        distance = [mathHandler distanceBetweenTwoPoints:&first second:&second];
        iteration++;
        if (iteration > [tableOfAngles count]){
            distance = 3;
        }
    }
    
    NSPoint workingPointFirst = [[tableOfAngles firstObject] pointValue];
    NSPoint workingPointSecond = [[tableOfAngles objectAtIndex:iteration-1] pointValue];
    
    int workingResult;
    
    //3 is optimal
    if ([mathHandler distanceBetweenTwoPoints:edgePoint second:&workingPointFirst] < 5){
        workingResult = 1;
    }
    else if ([mathHandler distanceBetweenTwoPoints:edgePoint second:&workingPointSecond] < 5){
        workingResult = 2;
    }
    else if ([mathHandler checkIfPointLaysBetweenTwoPoints:centerPoint second:edgePoint middlePoint:&workingPointFirst] == YES){
        workingResult = 1;
    }
    else if ([mathHandler checkIfPointLaysBetweenTwoPoints:centerPoint second:edgePoint middlePoint:&workingPointSecond] == YES){
        workingResult = 2;
    }
    else{
        workingResult = [mathHandler determinePointInDirection:centerPoint edge:edgePoint firstNewPoint:&workingPointFirst secondNewPoint:&workingPointSecond];
    }
    
    int minPosition;
    
    if (workingResult == 1){
        minPosition = [[positionArray firstObject] intValue];
    }
    else{
        minPosition = [[positionArray objectAtIndex:iteration-1] intValue];
    }
    
    NSPoint newPoint = NSMakePoint(0,0);
    newPoint.x = [[splinePoints objectAtIndex:minPosition] x];
    newPoint.y = [[splinePoints objectAtIndex:minPosition] y];
    
    
    return newPoint;
    
}

-(ROI *)    convertPolygonToBrush: (ROI *) roi{
    
    ROI *newPolygonROI = [self createROIWithName:[roi name] type:[roi type] color:[roi rgbcolor] points:[roi splinePoints] pixelSpacingX:[roi pixelSpacingX] imageOrigin:[roi imageOrigin]];
    
    ROI *newBrushROI = [[ROI alloc] init];
    newBrushROI = [viewerController convertPolygonROItoBrush:newPolygonROI];
    
    newBrushROI.name = roi.name;
    newBrushROI.rgbcolor = roi.rgbcolor;
    newBrushROI.imageOrigin = roi.imageOrigin;
    
    return newBrushROI;
    
}

-(ROI *)    convertBrushToPolygon: (ROI *) roi number:(int) num{
    
    ROI *newROI = [[ROI alloc] init];
    newROI = [viewerController convertBrushROItoPolygon:roi numPoints:num];
    
    newROI.name = roi.name;
    newROI.rgbcolor = roi.rgbcolor;
    newROI.imageOrigin = roi.imageOrigin;
    
    return newROI;
    
}

-(void)     setNameForSelectedRoiIfPointType: (ROI*) roi name:(NSString *) name{
    
    if ((([roi ROImode]==ROI_selected) || ([roi ROImode]==ROI_selectedModify) || ([roi ROImode]==ROI_drawing)) && ([roi type]==t2DPoint)){
        
        [roi setName: name];
        
    }
    
}

-(ROI*)     setNameForSelectedRoiIfPointTypeAndGet: (ROI*) roi name:(NSString *) name{
    
    if ((([roi ROImode]==ROI_selected) || ([roi ROImode]==ROI_selectedModify) || ([roi ROImode]==ROI_drawing)) && ([roi type]==t2DPoint)){
        
        [roi setName: name];
        return roi;
        
    }
    
    return NULL;
    
}

-(void)     propagateSelectedPointROIOverSlices: (int) curSlice curTimeFrame: (int) curTimeFrame roiSeriesList: (NSMutableArray *) roiSeriesList roiImageList: (NSMutableArray *) roiImageList slicesNumber: (int) slicesNumber{

    //propagate
    
    NSMutableArray  *selectedROIs = [NSMutableArray  array];
    long i, x;
    
    for(i = 0; i < [roiImageList count]; i++){
        
        ROI *curROI = [roiImageList objectAtIndex: i];
        long mode = [[roiImageList objectAtIndex: i] ROImode];
        long type = [curROI type];
        
        if( (mode == ROI_selected || mode == ROI_selectedModify || mode == ROI_drawing) && (type==t2DPoint))
        {
            [selectedROIs addObject: [roiImageList objectAtIndex: i]];
        }
    }
    if( [selectedROIs count] > 0)
    {
        for( x = 0; x < slicesNumber; x++)
        {
            if( x != curSlice)
            {
                for( i = 0; i < [selectedROIs count]; i++)
                {
                    [[[viewerController roiList:x] objectAtIndex: curTimeFrame] addObject: [selectedROIs objectAtIndex: i]];
                }
                
            }
        }
        
    }
    else
    {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert setMessageText:@"ROIs Propagate Error"];
        [alert setInformativeText:@"No ROI selected to propagate on the series!"];
        [alert setAlertStyle:NSWarningAlertStyle];
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            // OK clicked
        }
    }
    
}

- (ROI*) createROIFromPointsInString: (NSString*) stringWithPoints withNameNumber: (int) nameNumber{
    
    NSMutableArray *points = [[NSMutableArray alloc] init];
    
    DCMView *newView = [viewerController imageView];
    
    NSString *workingString = [[NSString alloc] initWithString:stringWithPoints];
    NSRange resultRange = [workingString rangeOfString:@" "];
    
    while (resultRange.length != 0){
        
        NSString *secondWorkingString = [[NSString alloc] initWithString:[workingString substringFromIndex:resultRange.location]];
        NSRange endOfPointRange = [secondWorkingString rangeOfString:@"\r\n"];
        
        DCMPix *newPix = [newView curDCM];
        //long pixelHeight = [newPix pheight];
        //long pixelWidth = [newPix pwidth];
        
        double pointX = [[workingString substringWithRange:NSMakeRange(0, resultRange.location)] doubleValue];
        double pointY = [[secondWorkingString substringWithRange:NSMakeRange(0, endOfPointRange.location)] doubleValue];
        
        NSPoint newPoint = NSMakePoint(pointX, pointY);
        MyPoint *pointToAdd = [[MyPoint alloc] initWithPoint:newPoint];
        
        [points addObject:pointToAdd];
        
        workingString = [secondWorkingString substringFromIndex:endOfPointRange.location+endOfPointRange.length];
        resultRange = [workingString rangeOfString:@" "];
        
    }

    if ([points count] != 0){
        double pixelSpacingX = [newView pixelSpacingX];
        ROI* newROI =[self createROIWithNameNumber:nameNumber type:tCPolygon color:nameNumber points:points pixelSpacingX:pixelSpacingX imageOrigin:NSMakePoint(1.91601563, -139.4534)];
        return newROI;
    }
    else{
        return nil;
    }
    
}

- (ROI*) createNewROISimpleWithPoints: (NSMutableArray*) points type: (int) roiType nameNumber: (int) nameNumber colorNumber: (int) colorNumber{
    
    RGBColor myROIColors[] = {
        {0, 65535, 0},
        {65535, 0, 0},
        {65535, 65535, 0},
        {0, 65535,65535},
    };
    
    ROI *newROI = [viewerController newROI:roiType];
    
    [[newROI points] removeAllObjects];
    [[newROI splinePoints] removeAllObjects];
    
    for (int i = 0; i < [points count]; i++){
        NSValue *value = [points objectAtIndex:i];
        NSPoint point = value.pointValue;
        if (point.x >0 && point.y >0){
             [newROI addPoint:point];
        }
       
    }
    
    //newROI.points = points;
    
    [newROI setName: myROINames[nameNumber]];
    [newROI setColor: myROIColors[colorNumber]];
    
    return newROI;
    
}

- (ROI*) createNewROIWithCopyPoints: (NSMutableArray*) points type: (int) roiType nameNumber: (int) nameNumber colorNumber: (int) colorNumber{
    
    RGBColor myROIColors[] = {
        {0, 65535, 0},
        {65535, 0, 0},
        {65535, 65535, 0},
        {0, 65535,65535},
    };
    
    ROI *newROI = [viewerController newROI:roiType];
    
    [[newROI points] removeAllObjects];
    [[newROI splinePoints] removeAllObjects];
    
    newROI.points = points;
    
    [newROI setName: myROINames[nameNumber]];
    [newROI setColor: myROIColors[colorNumber]];
    
    return newROI;
    
}

- (void) checkIfROIExistsAndIfSoDeleteItOnFrame: (NSMutableArray*) roiImageList nameNumber: (int) nameNumber{
    
    NSUInteger oldROIidx = [roiImageList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([[(ROI *)obj name] isEqualToString:myROINames[nameNumber]]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    if (oldROIidx!=NSNotFound)
        [viewerController deleteROI:[roiImageList objectAtIndex:oldROIidx]];
    
}

- (NSMutableArray*) createArrayOfCellStatesWithSliceNumber: (int) slices frames: (int) frames{
    
    NSMutableArray* newArray = [[NSMutableArray alloc] init];
    
    for (int curSlice = 0; curSlice < slices; curSlice++){
        
        for (int curFrame = 0; curFrame < frames; curFrame++){
         
            CellState *newState = [self getCellState:curSlice frame:curFrame];
            [newArray addObject:newState];
            
        }
    }
    
    return newArray;
}

- (BOOL) checkIfSliceHasAnyContourAtAnyFrame: (int) sliceNumber{
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: sliceNumber];
    
    for (int curFrame = 0; curFrame < [roiSeriesList count]; curFrame++){
        NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: curFrame];
        
        for (int curROI = 0; curROI < [roiImageList count];curROI++){
            
            ROI *newROI = [roiImageList objectAtIndex:curROI];
            if (newROI.type == tCPolygon){
                return YES;
            }
        }
    }

    return NO;
}

- (NSString*) createDisplayInfoStringForSlice: (int) slice frame: (int) frame{
    
    NSString* newString = [[NSString alloc] initWithFormat:@"[DISPLAY INFO]\n%d %d", slice, frame];
    newString = [newString stringByAppendingString:@" -102 916 262.540881 153.951672 1.900000"];
    
    return newString;
 
}
- (NSMutableArray*) roiPointsForQmassExportSlice: (int) slice frame: (int) frame{
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    
    for (int roiNumber = 0; roiNumber < [roiImageList count]; roiNumber++){
        ROI* curROI = [roiImageList objectAtIndex:roiNumber];
        if (curROI.type == tCPolygon){
            int numberOfROI = [self giveROIsNameNumber:curROI];
            int nameNumber;
            
            switch (numberOfROI){
                case 0:
                    nameNumber = 1;
                    break;
                case 1:
                    nameNumber = 0;
                    break;
                case 2:
                    nameNumber = 2;
                    break;
                case 3:
                    nameNumber = 5;
                    break;
                default:
                    nameNumber = -1;
                    break;
            }
            
            if (nameNumber != -1){
                [newArray addObject:@"[XYCONTOUR]"];
                double point = 1.0;
                NSString* header = [[NSString alloc] initWithFormat:@"%d %d %d  %.1f", [viewerController maxMovieIndex]-slice-1, frame, nameNumber, point];
                [newArray addObject:header];
                NSMutableArray* splinePoints = [curROI splinePoints:0.5];
                [newArray addObject:[NSString stringWithFormat:@"%lu",(unsigned long)[splinePoints count]]];
                for (int i = 0; i < [splinePoints count]; i++){
                    MyPoint* point = [splinePoints objectAtIndex:i];
                    [newArray addObject:[NSString stringWithFormat:@"%.4f %.4f",point.x, point.y]];
                }
                [newArray addObject:@"103"];
                [newArray addObject:@"0 0"];
            }
        }
    }
    
    return newArray;
}

- (int) giveROIsNameNumber: (ROI*) roi{
    int number;
    
    if ([roi.name isEqualTo:[myROINames objectAtIndex:0]]){
        number = 0;
    }
    else if([roi.name isEqualTo:[myROINames objectAtIndex:1]]){
        number = 1;
    }
    else if([roi.name isEqualTo:[myROINames objectAtIndex:2]]){
        number = 2;
    }
    else if([roi.name isEqualTo:[myROINames objectAtIndex:3]]){
        number = 3;
    }
    else{
        number = -1;
    }
    
    return number;
}

- (NSString*) getDicomDataForTag:(NSString*) tagString{
    
    NSArray         *pixList = [viewerController pixList: 0];
    long            curSlice = [[viewerController imageView] curImage];
    DCMPix          *curPix = [pixList objectAtIndex: curSlice];
    NSString        *file_path = [curPix sourceFile];
    
    NSString        *dicomTag = tagString;
    
    DCMObject       *dcmObj = [DCMObject objectWithContentsOfFile:file_path decodingPixelData:NO];
    
    DCMAttributeTag *tag = [DCMAttributeTag tagWithName:dicomTag];
    if (!tag) tag = [DCMAttributeTag tagWithTagString:dicomTag];
    
    NSString        *val;
    DCMAttribute    *attr;
    
    if (tag && tag.group && tag.element)
    {
        attr = [dcmObj attributeForTag:tag];
        
        val = [[attr value] description];
        
    }
    else{
        val = @"";
    }
    
    if ([val isEqualToString:@"(null)"]){
        val = @"";
    }
    if (val == NULL){
        val = @"";
    }
    
    return val;
}

- (NSMutableArray*)           roiSegPointForQmassExportSlice: (int) slice frame: (int) frame{
    
    NSMutableArray  *roiSeriesList  = [viewerController roiList: slice];
    NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: frame];
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    
    for (int curROI = 0; curROI < [roiImageList count]; curROI++){
        
        ROI *roi = [roiImageList objectAtIndex:curROI];
        
        if([roi.name isEqualToString:@"SegPoint"] && roi.type == t2DPoint){
            [newArray addObject:@"[POINT]"];
            [newArray addObject:[NSString stringWithFormat:@"%d %d 0 1.0", slice+1, frame]];
            MyPoint *newpoint = [roi.points objectAtIndex:0];
            [newArray addObject:[NSString stringWithFormat:@"%f %f",newpoint.x, newpoint.y]];
        }
        
    }
    return newArray;
    
}


//CONVEX HULL Start


inline float
isLeft( NSPoint P0, NSPoint P1, NSPoint P2 )
{
    return (P1.x - P0.x)*(P2.y - P0.y) - (P2.x - P0.x)*(P1.y - P0.y);
}

- (int) chainHull2DD: (NSPoint*) P n:(int) n h:(NSPoint*) H{

//int
//chainHull_2D( NSPoint* P, int n, NSPoint* H )
//{
    // the output array H[] will be used as the stack
    int    bot=0, top=(-1);   // indices for bottom and top of the stack
    int    i;                 // array scan index
    
    // Get the indices of points with min x-coord and min|max y-coord
    int minmin = 0, minmax;
    float xmin = P[0].x;
    for (i=1; i<n; i++)
        if (P[i].x != xmin) break;
    minmax = i-1;
    if (minmax == n-1) {       // degenerate case: all x-coords == xmin
        H[++top] = P[minmin];
        if (P[minmax].y != P[minmin].y) // a  nontrivial segment
            H[++top] =  P[minmax];
        H[++top] = P[minmin];            // add polygon endpoint
        return top+1;
    }
    
    // Get the indices of points with max x-coord and min|max y-coord
    int maxmin, maxmax = n-1;
    float xmax = P[n-1].x;
    for (i=n-2; i>=0; i--)
        if (P[i].x != xmax) break;
    maxmin = i+1;
    
    // Compute the lower hull on the stack H
    H[++top] = P[minmin];      // push  minmin point onto stack
    i = minmax;
    while (++i <= maxmin)
    {
        // the lower line joins P[minmin]  with P[maxmin]
        if (isLeft( P[minmin], P[maxmin], P[i])  >= 0 && i < maxmin)
            continue;           // ignore P[i] above or on the lower line
        
        while (top > 0)         // there are at least 2 points on the stack
        {
            // test if  P[i] is left of the line at the stack top
            if (isLeft(  H[top-1], H[top], P[i]) > 0)
                break;         // P[i] is a new hull  vertex
            else
                top--;         // pop top point off  stack
        }
        H[++top] = P[i];        // push P[i] onto stack
    }
    
    // Next, compute the upper hull on the stack H above  the bottom hull
    if (maxmax != maxmin)      // if  distinct xmax points
        H[++top] = P[maxmax];  // push maxmax point onto stack
    bot = top;                  // the bottom point of the upper hull stack
    i = maxmin;
    while (--i >= minmax)
    {
        // the upper line joins P[maxmax]  with P[minmax]
        if (isLeft( P[maxmax], P[minmax], P[i])  >= 0 && i > minmax)
            continue;           // ignore P[i] below or on the upper line
        
        while (top > bot)     // at least 2 points on the upper stack
        {
            // test if  P[i] is left of the line at the stack top
            if (isLeft(  H[top-1], H[top], P[i]) > 0)
                break;         // P[i] is a new hull  vertex
            else
                top--;         // pop top point off  stack
        }
        H[++top] = P[i];        // push P[i] onto stack
    }
    if (minmax != minmin)
        H[++top] = P[minmin];  // push  joining endpoint onto stack
    
    return top+1;
}

- (void) convexHull: (int) s frame:(int) t{
    
    ROI *roi = [self searchROIWithNameInSliceInFrame:@"PointGrow" slice:s frame:t];
    if (roi == nil)
    {
        return;
    }
    NSMutableArray *pointArray = [self textureBufferToPointArray:roi];
    
    int size = [pointArray count];

    NSPoint inputArray[size];
    NSPoint outputArray[size];
    
    /*
    unsigned char *originalTextureBuffer = roi.textureBuffer;
    
    for (int i = 0; i<size; i++){
        if (originalTextureBuffer[i] == 0xFF){
        }
    }
     */
    if ([pointArray count] >0){
        for (int i=0; i< [pointArray count]; i++){
            
            NSValue *point = [pointArray objectAtIndex:i];
            float x = point.pointValue.x;
            float y = point.pointValue.y;
            
            inputArray[i].x = x;
            inputArray[i].y = y;
            
            outputArray[i].x = 0;
            outputArray[i].y = 0;
        }
        [self chainHull2DD:inputArray n:size h:outputArray];
        
        NSMutableArray *outputMutableArray = [NSMutableArray new];
        
        for (int i = 0; i<size; i++){
            [outputMutableArray addObject:[NSValue valueWithPoint:outputArray[i]]];
        }

        ROI* newROI = [self createNewROISimpleWithPoints:outputMutableArray type:tCPolygon nameNumber:0 colorNumber:0];
        NSMutableArray  *roiSeriesList  = [viewerController roiList: s];
        NSMutableArray  *roiImageList = [roiSeriesList objectAtIndex: t];
        ROI* newROITwo = [viewerController convertPolygonROItoBrush:newROI];
        [newROITwo setName:resultROIName.stringValue];
        [roiImageList addObject: newROITwo];
        
    }

}

- (NSMutableArray*) textureBufferToPointArray: (ROI*) roi{
    
    NSMutableArray* pointArray = [[NSMutableArray alloc] init];
    
    unsigned char *texturecurROI = NULL;
    
    float pixWidthROI1;
    float pixHeightROI1;
    float sumOfPixelROI1;
    int textureUpLeftCornerXROI1;
    int textureUpLeftCornerYROI1;
    
    if (roi == nil){
        return NULL;
    }
    
    pixWidthROI1 = [roi textureWidth];
    pixHeightROI1 = [roi  textureHeight];
    sumOfPixelROI1 = pixHeightROI1 * pixWidthROI1;
    textureUpLeftCornerXROI1 = [roi textureUpLeftCornerX];
    textureUpLeftCornerYROI1 = [roi textureUpLeftCornerY];
    
    texturecurROI = (unsigned char*)malloc(sumOfPixelROI1*sizeof(unsigned char));
    
    for (int x=0; x< sumOfPixelROI1; x++){
        
        texturecurROI[x] = [roi textureBuffer][x];
        
    }
    
    for (int y = textureUpLeftCornerYROI1; y < (textureUpLeftCornerYROI1 + pixHeightROI1); y++){
        
        for (int x = textureUpLeftCornerXROI1; x < (textureUpLeftCornerXROI1 + pixWidthROI1); x++){
            
            int positionOfROI1 = ((y-textureUpLeftCornerYROI1)*pixWidthROI1) + (x-textureUpLeftCornerXROI1);
            
            if (texturecurROI[positionOfROI1] == 0xFF){
                NSPoint point;
                point.x = x;
                point.y = y;
                [pointArray addObject:[NSValue valueWithPoint:point]];
            }
            
        }
    }
    if (texturecurROI != NULL){
        free(texturecurROI);
    }
    
    return pointArray;
}

// Rozrost punktu - moj algorytm


- (void) pointGrow: (ROI*) workingROI reference:(ROI*) referenceROI point:(NSPoint) curPoint{
    
    
    float pixWidthROI1;
    float pixHeightROI1;
    float sumOfPixelROI1;
    int textureUpLeftCornerXROI1;
    int textureUpLeftCornerYROI1;
    
    if (referenceROI == NULL || referenceROI ==nil || workingROI == NULL || workingROI == nil){
        return;
    }
    
    if(referenceROI.textureBuffer == NULL || workingROI.textureBuffer == NULL){
        return;
    }
    
    pixWidthROI1 = [referenceROI textureWidth];
    pixHeightROI1 = [referenceROI textureHeight];
    sumOfPixelROI1 = pixHeightROI1 * pixWidthROI1;
    textureUpLeftCornerXROI1 = [referenceROI textureUpLeftCornerX];
    textureUpLeftCornerYROI1 = [referenceROI textureUpLeftCornerY];

    int y = curPoint.y;
    int x = curPoint.x;
    
    for (int newX = -1; newX <=1; newX++){
        for (int newY = -1; newY <=1; newY++){
            
            NSPoint newPoint;
            newPoint.x = x+newX;
            newPoint.y = y+newY;
            
            int position = (y+newY-textureUpLeftCornerYROI1)*pixWidthROI1+(x+newX-textureUpLeftCornerXROI1);
            
            if ([referenceROI textureBuffer][position] == 0xFF){
                if ([workingROI textureBuffer][position] == 0x00){
                    [workingROI textureBuffer][position] = 0xFF;
                    [self pointGrow:workingROI reference:referenceROI point:newPoint];
                }
            }
        }
    }
    
    
    
}

@end
