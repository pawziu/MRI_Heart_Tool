

#import <Cocoa/Cocoa.h>
#import <CorePlot/CorePlot.h>

@interface MFPController : NSWindowController <NSWindowDelegate>{
    
    IBOutlet CPTGraphHostingView *plotView;
    CPTXYGraph *graph;
    NSArray *plotData;
    int numberOfTimeFrames;
    int numberOfSectors;
    double *plotPoints;
    double maxValue;
    BOOL windowVisible;
    
}

@property (nonatomic, assign) BOOL windowVisible;

@property (nonatomic) int numberOfTimeFrames;
@property (nonatomic) int numberOfSectors;
@property (nonatomic) double maxValue;
@property double *plotPoints;

@property (nonatomic, readwrite, strong) IBOutlet CPTGraphHostingView *plotView;
@property (nonatomic, readwrite, strong) CPTXYGraph *graph;
@property (nonatomic, readwrite, strong) NSArray *plotData;

- (void) updateGraph;

@end
