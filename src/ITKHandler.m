#import "ITKHandler.h"

#include <iomanip>

#include "itkImage.h"
#include "itkImportImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkNumericTraits.h"


#include "itkConfidenceConnectedImageFilter.h"
#include "itkConnectedThresholdImageFilter.h"

//morphological closing
#include "itkGrayscaleMorphologicalClosingImageFilter.h"
#include "itkBinaryBallStructuringElement.h"

//hole filling
#include "itkVotingBinaryHoleFillingImageFilter.h"

//ShapeDetectionLevelSetFilter
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkFastMarchingImageFilter.h"
#include "itkShapeDetectionLevelSetImageFilter.h"

#include "itkLabelOverlapMeasuresImageFilter.h"

#include <itkAdaptiveHistogramEqualizationImageFilter.h>

@implementation ITKHandler

@synthesize viewerController;
@synthesize mathHandler;
@synthesize viewerControllerSeg;

//typedef unsigned char   PixelType;
typedef float			PixelTypeFloat;
typedef itk::Image< PixelTypeFloat, 3 > ImageTypeFloat3D;
typedef itk::ImportImageFilter< PixelTypeFloat, 3 > ImportFilterTypeFloat3D;


typedef     float itkPixelType;
typedef     itk::Image< itkPixelType, 2 > ImageType;
typedef     ImageType::Pointer            ImageTypePointer;
typedef     itk::ImportImageFilter< itkPixelType, 2 > ImportFilterType;
typedef     itk::BinaryThresholdImageFilter<ImageType, ImageType>  BinaryThresholdImageFilterType;

//operacje morfologiczne
typedef itk::BinaryBallStructuringElement<PixelTypeFloat, 2  > StructuringElementType;
typedef itk::GrayscaleMorphologicalClosingImageFilter<ImageType, ImageType, StructuringElementType >  ClosingFilterType;

//hole filling
typedef     itk::VotingBinaryHoleFillingImageFilter<ImageType, ImageType>  HoleFillingFilterType;

//Choose FilterType
typedef     itk::ConfidenceConnectedImageFilter<ImageType, ImageType >  ChoosedImageFilterType;

//ShapeDetectionLevelSetFilter
typedef float InternalPixelType;
const unsigned int Dimension = 2;
typedef itk::Image<InternalPixelType,Dimension> InternalImageType;
typedef unsigned char OutputPixelType;
typedef itk::Image< OutputPixelType, Dimension > OutputImageType;
typedef itk::BinaryThresholdImageFilter< InternalImageType, OutputImageType > ThresholdingFilterType;
typedef itk::CurvatureAnisotropicDiffusionImageFilter< InternalImageType,InternalImageType > SmoothingFilterType;
typedef itk::GradientMagnitudeRecursiveGaussianImageFilter< InternalImageType,InternalImageType > GradientFilterType;
typedef itk::SigmoidImageFilter< InternalImageType,InternalImageType > SigmoidFilterType;
typedef itk::FastMarchingImageFilter< InternalImageType, InternalImageType > FastMarchingFilterType;
typedef itk::ShapeDetectionLevelSetImageFilter<InternalImageType, InternalImageType > ShapeDetectionFilterType;
typedef FastMarchingFilterType::NodeContainer NodeContainer;
typedef FastMarchingFilterType::NodeType NodeType;


typedef unsigned int PixelTypeLO;
typedef     itk::Image<PixelTypeLO, 2> ImageTypeLO;
typedef     itk::ImportImageFilter< PixelTypeLO, 2 > ImportFilterTypeLO;

typedef itk::LabelOverlapMeasuresImageFilter<ImageTypeLO> LabelOverlap;

typedef itk::AdaptiveHistogramEqualizationImageFilter< ImageType > AdaptiveHistogramEqualizationImageFilterType;


@synthesize regionMultiplier;
@synthesize regionIterations;
@synthesize regionInitialRadius;

@synthesize edgeTimeStep;
@synthesize edgeIterations;
@synthesize edgeConductance;
@synthesize edgeSigma;
@synthesize edgeAlpha;
@synthesize edgeBeta;
@synthesize edgeStoppingValue;
@synthesize edgePropagationScalling;
@synthesize edgeCurvatuveScalling;

- (id) initWithViewerController: (ViewerController*) pointer mathHandler: (MathHandler*) pointerTwo vcs:(ViewerController*) pointerThree{
    if (self = [super init]){
        
        viewerController = pointer;
        viewerControllerSeg = pointerThree;
        mathHandler = pointerTwo;
        bufferSize = 0;
        
       firstPix = NULL;
        times = 0;
        slices = 0;
        
        regionMultiplier.doubleValue = 3.0;
        regionIterations.integerValue = 1;
        regionInitialRadius.integerValue = 4;
        
        edgeTimeStep.doubleValue = 0.125;
        edgeIterations.integerValue = 5;
        edgeConductance.doubleValue = 9.0;
        edgeSigma.doubleValue = 1.0;
        edgeAlpha.doubleValue = -0.5;
        edgeBeta.doubleValue = 3.0;

        
    }
    return self;
    
}

- (ImportFilterType::Pointer) importFilter: (DCMPix*) pix{
    
    firstPix = pix;
    // we will loop over slice and time dimention (in 4d viewer)
    times = [[viewerControllerSeg pixList] count];
    slices = [viewerControllerSeg maxMovieIndex];

    ImportFilterType::Pointer importFilter = ImportFilterType::New();
    
    ImportFilterType::IndexType start;
    start[0] = 0;
    start[1] = 0;
    
    ImportFilterType::SizeType size;
    size[0] = [firstPix pwidth];
    size[1] = [firstPix pheight];
    bufferSize = size[0] * size[1];
    
    ImportFilterType::RegionType region;
    double  origin[2];
    double  voxelSpacing[2];
    
    origin[0] = [firstPix originX];
    origin[1] = [firstPix originY];
    
    voxelSpacing[0] = [firstPix pixelSpacingX];
    voxelSpacing[1] = [firstPix pixelSpacingY];
    
    region.SetIndex(start);
    region.SetSize(size);
    
    // apply settings to ITK import filter
    importFilter->SetRegion(region);
    importFilter->SetOrigin(origin);
    importFilter->SetSpacing(voxelSpacing);
    importFilter->SetImportPointer([pix fImage], bufferSize, false);
    
    importFilter->Update();
    
    return importFilter;

}

-(ImportFilterTypeLO::Pointer)createITKImageFromBrushROI:(ROI*) roi width:(int)imageWidth height:(int)imageHeight{
    
    ImportFilterTypeLO::Pointer importFilter = ImportFilterTypeLO::New();
    
    ImportFilterType::IndexType start;
    start[0] = 0;
    start[1] = 0;
    
    ImportFilterType::SizeType size;
    size[0] = imageWidth;
    size[1] = imageHeight;
    
    ImportFilterType::RegionType region;
    double  origin[2];
    double  voxelSpacing[2];
    
    bufferSize = size[0] * size[1];
    
    DCMPix *testPix = [[viewerController pixList] objectAtIndex:0];
    origin[0] = testPix.originX;;
    origin[1] = testPix.originY;
    
    voxelSpacing[0] = roi.pixelSpacingX;
    voxelSpacing[1] = roi.pixelSpacingY;
    
    region.SetIndex(start);
    region.SetSize(size);
    
    importFilter->SetRegion(region);
    importFilter->SetOrigin(origin);
    importFilter->SetSpacing(voxelSpacing);
    
    int curPos = imageWidth * imageHeight;

    unsigned int *newbuffer = new unsigned int [curPos];
    for (int iter = 0; iter < curPos; iter++){
        newbuffer[iter] = 0;
    }
    for (int bufferPos = 0; bufferPos < roi.textureWidth*roi.textureHeight; bufferPos++){
        if(roi.textureBuffer[bufferPos] == 0xFF){
            int textWidth = roi.textureWidth;
            int times = bufferPos/textWidth;
            int minus = textWidth*times;
            int textBuffX = bufferPos - minus;
            int textBuffY = bufferPos/roi.textureWidth;
            int newPositionX = textBuffX + roi.textureUpLeftCornerX;
            int newPositionY = textBuffY + roi.textureUpLeftCornerY;
            int newPosition = newPositionX + newPositionY*imageWidth;
            newbuffer[newPosition] = 256;
        }
    }

    unsigned int *pointer = &newbuffer[0];
    importFilter->SetImportPointer(pointer, bufferSize, false);
    importFilter->Update();
    return importFilter;
}

- (void) performSemiAutoSEG: (int) s frame:(int) t point:(NSPoint) point multiplierValue:(double) multiplier{
   
    // ITK initialization
    ChoosedImageFilterType::Pointer thresholdFilter = ChoosedImageFilterType::New();
    
    NSMutableArray *PixList = [viewerController pixList:s];
    DCMPix *pix = [PixList objectAtIndex:t];
    
    long mem = bufferSize * sizeof(float);
    
    ImageType::IndexType index;
    index[0] = point.x;  index[1] = point.y;
    
    
    ImportFilterType::Pointer importFilter = [self importFilter:pix];
    
    importFilter->Update();
    importFilter->Update();
    
    // HISTOGRAM EQUALIZATION - unused - too long to calculate
    
    //AdaptiveHistogramEqualizationImageFilterType::Pointer adaptiveHistogramEqualizationImageFilter = AdaptiveHistogramEqualizationImageFilterType::New();
    
    //adaptiveHistogramEqualizationImageFilter->SetAlpha( 1.0 );
    //adaptiveHistogramEqualizationImageFilter->SetBeta( 1.0 );
    //radius.Fill( 1 );
    //adaptiveHistogramEqualizationImageFilter->SetRadius( radius );
    //adaptiveHistogramEqualizationImageFilter->SetInput( importFilter->GetOutput() );
    //adaptiveHistogramEqualizationImageFilter->Update();
    
    //-------------------
    
    if (multiplier <=1){
        thresholdFilter->SetMultiplier( regionMultiplier.doubleValue );
    }else{
        thresholdFilter->SetMultiplier( multiplier );
    }
    
    thresholdFilter->SetNumberOfIterations( regionIterations.intValue );
    thresholdFilter->SetInitialNeighborhoodRadius ( regionInitialRadius.intValue );
    thresholdFilter->SetReplaceValue( 255 );
    thresholdFilter->AddSeed( index );
    thresholdFilter->SetInput(importFilter->GetOutput());
    thresholdFilter->Update();
    thresholdFilter->Update();
    
    float* resultBuff = thresholdFilter->GetOutput()->GetBufferPointer();
    
    memcpy( [[[viewerControllerSeg pixList:s] objectAtIndex:t] fImage], resultBuff, mem);

    [viewerControllerSeg needsDisplayUpdate];
}

- (void) ShapeDetectionLevelSetFilter: (int) s frame:(int) t point:(NSPoint) point{
    
    NSMutableArray *PixList = [viewerController pixList:s];
    DCMPix *pix = [PixList objectAtIndex:t];
    long mem = bufferSize * sizeof(float);
    
    ImportFilterType::Pointer importFilter = [self importFilter:pix];

    importFilter->Update();
    
    ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();

    thresholder->SetLowerThreshold( -50.0 );
    thresholder->SetUpperThreshold( 0.0 );
    
    thresholder->SetOutsideValue( 0 );
    thresholder->SetInsideValue( 255 );
    
    SmoothingFilterType::Pointer smoothing = SmoothingFilterType::New();
    GradientFilterType::Pointer gradientMagnitude = GradientFilterType::New();

    smoothing->SetTimeStep( edgeTimeStep.doubleValue );
    smoothing->SetNumberOfIterations( edgeIterations.intValue );
    smoothing->SetConductanceParameter( edgeConductance.doubleValue );
    double sigma = 1.0;
    sigma = edgeSigma.doubleValue;
    gradientMagnitude->SetSigma( sigma );
    
    smoothing->SetInput( importFilter->GetOutput() );
    gradientMagnitude->SetInput( smoothing->GetOutput() );
    gradientMagnitude->Update();

    float* resultBuff = gradientMagnitude->GetOutput()->GetBufferPointer();
    
    memcpy( [[[viewerControllerSeg pixList:s] objectAtIndex:t] fImage], resultBuff, mem);
    
    [viewerControllerSeg needsDisplayUpdate];
    
}

- (void) morphoClose: (int) s frame:(int) t{
    
    ClosingFilterType::Pointer closingFilter = ClosingFilterType::New();
    StructuringElementType  structuringElement;
    structuringElement.SetRadius( 3 );
    structuringElement.CreateStructuringElement();
    
    closingFilter->SetKernel(  structuringElement );
    
    NSMutableArray *PixList = [viewerControllerSeg pixList:s];
    
    long mem = bufferSize * sizeof(float);
    
    DCMPix *pix = [PixList objectAtIndex:t];
    
    ImportFilterType::Pointer importFilter = [self importFilter:pix];
    
    closingFilter->SetInput(importFilter->GetOutput());
    closingFilter->Update();
    
    float* resultBuff = closingFilter->GetOutput()->GetBufferPointer();
    
    memcpy( [[[viewerControllerSeg pixList:s] objectAtIndex:t] fImage], resultBuff, mem);
    
    [viewerControllerSeg needsDisplayUpdate];

    
}

- (void) holeFilling: (int) s frame:(int) t{
    
    HoleFillingFilterType::Pointer holeFilter = HoleFillingFilterType::New();
    const unsigned int radiusX = 3;
    const unsigned int radiusY = 3;
    // Software Guide : BeginCodeSnippet
    ImageType::SizeType indexRadius;
    indexRadius[0] = radiusX;
    indexRadius[1] = radiusY;
    
    holeFilter->SetRadius( indexRadius );
    holeFilter->SetBackgroundValue(   0 );
    holeFilter->SetForegroundValue( 255 );
    holeFilter->SetMajorityThreshold( 2 );
    
    NSMutableArray *PixList = [viewerControllerSeg pixList:s];
    
    long mem = bufferSize * sizeof(float);
    
    DCMPix *pix = [PixList objectAtIndex:t];
    
    ImportFilterType::Pointer importFilter = [self importFilter:pix];

    holeFilter->SetInput(importFilter->GetOutput());
    holeFilter->Update();
    
    float* resultBuff = holeFilter->GetOutput()->GetBufferPointer();
    
    memcpy( [[[viewerControllerSeg pixList:s] objectAtIndex:t] fImage], resultBuff, mem);
    
    [viewerControllerSeg needsDisplayUpdate];

    
}

- (int) convexHull2D: (NSPoint*) P numberOfPoints: (int) n outputArray:(NSPoint*) H{
    

    int    bot=0, top=(-1);
    int    i;
    

    int minmin = 0, minmax;
    float xmin = P[0].x;
    
    for (i=1; i<n; i++){
        if (P[i].x != xmin) break;
    }
    
    minmax = i-1;
    
    if (minmax == n-1) {
        H[++top] = P[minmin];
        if (P[minmax].y != P[minmin].y)
            H[++top] =  P[minmax];
        H[++top] = P[minmin];
        return top+1;
    }
    
    
    int maxmin, maxmax = n-1;
    float xmax = P[n-1].x;
    for (i=n-2; i>=0; i--)
        if (P[i].x != xmax) break;
    maxmin = i+1;
    
    
    H[++top] = P[minmin];
    i = minmax;
    while (++i <= maxmin)
    {
        
        
        if ([mathHandler isLeft:P[minmin] point2:P[maxmin] point3:P[i]]  >= 0 && i < maxmin)
            continue;
        
        while (top > 0)
        {
            
            if ([mathHandler isLeft:H[top-1] point2:H[top] point3:P[i]])
                break;
            else
                top--;
        }
        H[++top] = P[i];
    }
    
   
    if (maxmax != maxmin)
        H[++top] = P[maxmax];
    bot = top;
    i = maxmin;
    while (--i >= minmax)
    {
        
        
        if ([mathHandler isLeft:P[maxmax] point2:P[minmax] point3:P[i]]  >= 0 && i > minmax)
            continue;
        
        while (top > bot)
        {
            
           
            if ([mathHandler isLeft:H[top-1] point2:H[top] point3:P[i]] > 0)
                break;
            else
                top--;
        }
        H[++top] = P[i];
    }
    if (minmax != minmin)
        H[++top] = P[minmin];  
    
    return top+1;
}

- (NSMutableArray*)LabelOverlapCount: (ImportFilterTypeLO::Pointer) sourceROI targetROI:(ImportFilterTypeLO::Pointer) targetROI{
    
    typename LabelOverlap::Pointer labelOverlapFilter = LabelOverlap::New();
    labelOverlapFilter->SetSourceImage(sourceROI->GetOutput());
    labelOverlapFilter->SetTargetImage(targetROI->GetOutput());
    labelOverlapFilter->Update();
    
    NSMutableArray *results = [NSMutableArray arrayWithCapacity:6];
    
    [results addObject:[NSNumber numberWithDouble:double(labelOverlapFilter->GetTotalOverlap())]];
    [results addObject:[NSNumber numberWithDouble:double(labelOverlapFilter->GetUnionOverlap())]];
    [results addObject:[NSNumber numberWithDouble:double(labelOverlapFilter->GetMeanOverlap())]];
    [results addObject:[NSNumber numberWithDouble:double(labelOverlapFilter->GetVolumeSimilarity())]];
    [results addObject:[NSNumber numberWithDouble:double(labelOverlapFilter->GetFalseNegativeError())]];
    [results addObject:[NSNumber numberWithDouble:double(labelOverlapFilter->GetFalsePositiveError())]];
    
    
    std::cout << "                                          "
    << "************ All Labels *************" << std::endl;
    std::cout << std::setw( 10 ) << "   "
    << std::setw( 17 ) << "Total"
    << std::setw( 17 ) << "Union (jaccard)"
    << std::setw( 17 ) << "Mean (dice)"
    << std::setw( 17 ) << "Volume sim."
    << std::setw( 17 ) << "False negative"
    << std::setw( 17 ) << "False positive" << std::endl;
    std::cout << std::setw( 10 ) << "   ";
    std::cout << std::setw( 17 ) << labelOverlapFilter->GetTotalOverlap();
    std::cout << std::setw( 17 ) << labelOverlapFilter->GetUnionOverlap();
    std::cout << std::setw( 17 ) << labelOverlapFilter->GetMeanOverlap();
    std::cout << std::setw( 17 ) << labelOverlapFilter->GetVolumeSimilarity();
    std::cout << std::setw( 17 ) << labelOverlapFilter->GetFalseNegativeError();
    std::cout << std::setw( 17 ) << labelOverlapFilter->GetFalsePositiveError();
    std::cout << std::endl;

    return results;
}

- (NSMutableArray*)getLabelOverlapInfo: (ROI*) sourceROI targetROI:(ROI*) targetROI pwidth:(int) pwidth pheight:(int) pheight{
    
    ImportFilterTypeLO::Pointer sourceROIImage = [self createITKImageFromBrushROI: sourceROI width:pwidth height:pheight];
    ImportFilterTypeLO::Pointer targetROIImage = [self createITKImageFromBrushROI: targetROI width:pwidth height:pheight];
    NSMutableArray *results = [self LabelOverlapCount:sourceROIImage targetROI:targetROIImage];
    
    return results;
}


@end

