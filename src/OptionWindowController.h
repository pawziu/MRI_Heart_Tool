#import <Cocoa/Cocoa.h>
#import "OperationHandler.h"

@interface OptionWindowController : NSWindowController <NSWindowDelegate>{
    
    OperationHandler *operationHandler;
    ITKHandler *itkHandler;

    
    BOOL windowVisible;
    
    IBOutlet NSTextField *slicesFrom;
    IBOutlet NSTextField *slicesTo;
    IBOutlet NSTextField *framesFrom;
    IBOutlet NSTextField *framesTo;
    
    IBOutlet NSTextField *regionMultiplier;
    IBOutlet NSTextField *regionIterations;
    IBOutlet NSTextField *regionInitialRadius;
    
    IBOutlet NSTextField *edgeTimeStep;
    IBOutlet NSTextField *edgeIterations;
    IBOutlet NSTextField *edgeConductance;
    IBOutlet NSTextField *edgeSigma;
    IBOutlet NSTextField *edgeAlpha;
    IBOutlet NSTextField *edgeBeta;
    IBOutlet NSTextField *edgeStoppingValue;
    IBOutlet NSTextField *edgePropagationScalling;
    IBOutlet NSTextField *edgeCurvatuveScalling;
    
    IBOutlet NSButton *regionGrowingOptimalization;
    IBOutlet NSButton *edgeDetectionEnable;
    IBOutlet NSButton *convexHullEnable;
    IBOutlet NSButton *finalOptimalizationEnable;
    
    IBOutlet NSTextField *resultROIName;
    IBOutlet NSTextField *groundTruthROIName;
    
    
    NSButton *edgeDetectionStart;
    NSButton *convexHull;
}

@property (nonatomic, assign) BOOL windowVisible;
@property (nonatomic, assign) OperationHandler *operationHandler;
@property (nonatomic, assign) ITKHandler *itkHandler;

- (void) additionalInit;

@end
