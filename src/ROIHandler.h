#import "ROIClass.h"
#import "MathHandler.h"

typedef enum Operation{
    AND,
    XOR
}OperationOnROI;

@interface ROIHandler : NSObject{

    ViewerController *viewerController;
    MathHandler *mathHandler;
    
    NSArray *myROINames;
    
    NSTextField *resultROIName;
    
}

@property (nonatomic, strong) ViewerController *viewerController;
@property (nonatomic, strong) MathHandler *mathHandler;
@property (nonatomic) NSArray *myROINames;
@property (nonatomic, strong) NSTextField *resultROIName;

//////////////////Calculation Methods/////////////////////////
- (void) drawROI:(id) sender;

////////////////////////////////////////////////////////////////////////////
/** Init function for Viewer Controller Assignment */
- (id) initWithViewerController: (ViewerController*) pointer mathHandler: (MathHandler*) pointerTwo;

/** Converts specific Polygon ROI to Brush Type */
-(ROI *)    convertPolygonToBrush: (ROI *) roi;
/** Converts specific Brush ROI to Polygon with specific point number */
-(ROI *)    convertBrushToPolygon: (ROI *) roi number:(int) num;

/** Calculates common part of two rois */
-(ROI *)    brushROIProduct: (ROI *) ROI1 and: (ROI *) ROI2 and: (NSString *) ROIName and: (float) pixWidth and: (float) pixHeight and: (float) pixelSpacingX and: (float) pixelSpacingY and: (NSPoint) imageOrigin operation:(OperationOnROI) operationType;

/** Changes name of specific roi */
-(void)     setNameForSelectedRoiIfPointType: (ROI*) roi name:(NSString *) name;
/** Changes name of specific roi and returns this roi */
-(ROI *)     setNameForSelectedRoiIfPointTypeAndGet: (ROI*) roi name:(NSString *) name;

/** Copies roi over selected slice */
-(void)     propagateSelectedPointROIOverSlices: (int) curSlice curTimeFrame: (int) curTimeFrame roiSeriesList: (NSMutableArray *) roiSeriesList roiImageList: (NSMutableArray *) roiImageList slicesNumber: (int) slicesNumber;

/** Gets cell state for table preparation */
- (CellState*)          getCellState: (int) sliceNumber frame: (int) frameNumber;
/** Creates Array for Table creation*/
- (NSMutableArray*)     createArrayOfCellStatesWithSliceNumber: (int) slices frames: (int) frames;
/** Checks if specific slice has any contour (ROI Closed Polygon) at any frame of this slice*/
- (BOOL)                checkIfSliceHasAnyContourAtAnyFrame: (int) sliceNumber;
/** Creates Info String for each Slice and Frame*/
- (NSString*)           createDisplayInfoStringForSlice: (int) slice frame: (int) frame;
/** Get Point Roi information for Qmass Export*/
- (NSMutableArray*)     roiPointsForQmassExportSlice: (int) slice frame: (int) frame;
/** Get Seg Point Roi information for Qmass Export*/
- (NSMutableArray*)     roiSegPointForQmassExportSlice: (int) slice frame: (int) frame;

/** Return Number that corresponds to name of specific roi*/
- (int)                 giveROIsNameNumber: (ROI*) roi;
/** Gets information from dicom tags*/
- (NSString*)           getDicomDataForTag:(NSString*) tagString;

/** Searches Slice for ROIs with specific name that corresponds to given number*/
- (NSMutableArray*) searchCurSliceForROISWithNameNumber: (int) ROINameNumber curSlice:(int) slice;
/** Searches Frame for ROIs with specific name that corresponds to given number*/
- (NSMutableArray*) searchCurFrameForROIsWithName:(NSString*) name curSlice:(int) slice frame:(int) frame;
/** Merges all ROIs with the same name*/
- (void) mergeAllROIsWithTheSameName:(NSString*)name slice:(int) slice frame:(int) frame;
/** Searches Frame for one ROI with specific name that corresponds to given number*/
- (ROI*)    searchROIWithROINameNumberInFrame: (int) ROINameNumber image: (NSMutableArray*) imageList;
/** Searches Slice for one ROI with specific name that corresponds to given number*/
- (ROI*)    earchROIWithNameInSliceInFrame: (NSString*) ROIName slice:(int) slice frame:(int) frame;

/** Creates ROI with specific name that corresponds to given number*/
- (ROI*)    createROIWithNameNumber: (int) roiNameNumber type: (long) type color:(int) colorNumber points: (NSMutableArray*) points pixelSpacingX: (double) pixelSpacingX imageOrigin: (NSPoint) imageOrigin;
/** Creates ROI with specific name and type*/
- (ROI*)    createROIWithName:(NSString*) roiName type:(long) type color: (RGBColor) color points:(NSMutableArray*) points pixelSpacingX:(double) pixelSpacingX imageOrigin: (NSPoint) imageOrigin;
/** Creates Copy of selected ROI*/
- (ROI*) createROISimiliarToROI:(ROI*) referenceROI;
/** Creates Copy of selected Brush ROI*/
- (ROI*) createBrushROISimiliarToBrushROI: (ROI*) referenceROI;
/** Creates Copy of selected Brush ROI with different Texture Buffer*/
- (ROI*) createBrushROISimiliarToBrushROIWithTextureBuffer: (ROI*) referenceROI;

/** Add selected ROI to specific Slice and Frame*/
- (void)    addROI:(ROI*) roi sliceNumber:(int) slice frameNumber:(int) frame;
/** Checks if image has specific ROI*/
- (BOOL)    checkIfImageHasROI: (ROI*) roi series:(NSMutableArray*) series;
/** Copies selected ROI to given Slice and Frame*/
- (ROI*)    copyROIToSliceNumberAndFrameNumber: (ROI*) roi sliceNumber:(int) slice frameNumber:(int) frame;

/** Creates Points for Aproximation given specific ROI*/
- (NSPoint) createPointForApproximationFromROI: (ROI*) roi edgePoint: (NSPoint*) edgePoint centerPoint: (NSPoint*) centerPoint directionNumber: (int) direction;

/** Creates Closed Polygon ROI from given points*/
- (ROI*) createROIFromPointsInString: (NSString*) stringWithPoints withNameNumber: (int) nameNumber;
/** Creates Closed Polygon ROI from array of points*/
- (ROI*) createNewROISimpleWithPoints: (NSMutableArray*) points type: (int) roiType nameNumber: (int) nameNumber colorNumber: (int) colorNumber;
/** Creates Closed Polygon ROI with points copied from another ROI*/
- (ROI*) createNewROIWithCopyPoints: (NSMutableArray*) points type: (int) roiType nameNumber: (int) nameNumber colorNumber: (int) colorNumber;
/** Delete given ROI on frame*/
- (void) checkIfROIExistsAndIfSoDeleteItOnFrame: (NSMutableArray*) roiImageList nameNumber: (int) nameNumber;
/** Calculate Convex Hull on given slice and frame*/
- (void) convexHull: (int) s frame:(int) t;
/** Perform Point Grow Algorithm on selected ROIs*/
- (void) pointGrow: (ROI*) workingROI reference:(ROI*) referenceROI point:(NSPoint) curPoint;

@end
