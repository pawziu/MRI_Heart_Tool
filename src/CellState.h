#import <Foundation/Foundation.h>

typedef enum State{
    empty,
    manual,
    automatic
}StateOfContourInCell;

@interface CellState : NSObject{
    
    enum State LVEPIcontour;
    enum State LVENDOcontour;
    enum State RVEPIcontour;
    enum State RVENDOcontour;
    BOOL pointSet;
    BOOL sectorsCreated;
    
}

@property (nonatomic) enum State LVEPIcontour;
@property (nonatomic) enum State LVENDOcontour;
@property (nonatomic) enum State RVEPIcontour;
@property (nonatomic) enum State RVENDOcontour;
@property (nonatomic) BOOL pointSet;
@property (nonatomic) BOOL sectorsCreated;

/** Generates string for Table Creation*/
- (NSString*) generateCellStateString;

@end
