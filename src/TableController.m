


#import "TableController.h"


@interface TableController ()

- (void) addRow;
- (NSInteger) numberOfColumns;

@end

@implementation TableController

@synthesize _table;
@synthesize operationHandler;
@synthesize windowVisible;

@synthesize cellSize;

- (id) initWithWindow:(NSWindow *)window{
    self = [super initWithWindow:window];
    if (self){
        _table = [NSTableView new];
        _tableArray = [NSMutableArray new];
        cellSize = 30.0;
        windowVisible = YES;
    }
    return self;
}

- (void) addRow: (int) framesNumber numberOfThisSlice: (int) sliceNumber{
    
    Slice *curSlice = [[Slice alloc]initWithFramesNumber:framesNumber numberOfSlice:sliceNumber];
    curSlice.imageSize = cellSize;
    [_tableArray addObject:curSlice];
    [_table reloadData];
    
}

- (void) addRow{
    
    
    [_tableArray addObject:[[Slice alloc] init]];
    [_table reloadData];
    
}

- (void) addColumn: (NSString*) identifier{
    NSTableColumn *newColumn = [[NSTableColumn alloc] init];
    newColumn.identifier = identifier;
    newColumn.width = cellSize;
    newColumn.title = [identifier stringByReplacingOccurrencesOfString:@"Frame" withString:@""];
    newColumn.editable = NO;
    [_table addTableColumn:newColumn];
}

- (NSInteger) numberOfColumns{
    return [_table numberOfColumns];
}

- (void) reloadTableData{
    [_table reloadData];
}

- (NSInteger) numberOfRowsInTableView:(NSTableView *)tableView{
    
    return [_tableArray count];
}

- (id) tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    
    Slice *curSlice = [_tableArray objectAtIndex:row];
    NSString *identifier = [tableColumn identifier];
    
    if ([identifier isEqualToString:@"thisSliceNumber"]){
        NSString *number = [NSString stringWithFormat:@"%@",[curSlice valueForKey:identifier]];
        NSString *thisCellString = @"Slice ";
        thisCellString =[thisCellString stringByAppendingString:number];
        
        NSTableCellView *cellView = [tableView makeViewWithIdentifier:identifier owner:self];
        cellView.textField.stringValue = thisCellString;
        
        return cellView;
    }
    else{
        NSString *identifierNumber = [identifier stringByReplacingOccurrencesOfString:@"Frame" withString:@""];
        
        NSTableCellView *cellView = [tableView makeViewWithIdentifier:identifier owner:self];
        
        if (cellView == nil){
            cellView = [[NSTableCellView alloc] initWithFrame:NSMakeRect(0, 0, 30, 30)];
            cellView.identifier = identifier;
            
            NSImage *tempImg = [curSlice.sliceContents objectAtIndex:[identifierNumber intValue]-1];
            
            NSImageView *newImageView = [[NSImageView alloc] initWithFrame:[cellView frame]];
            [newImageView setIdentifier:identifier];
            
            [newImageView setImage:tempImg];
            
            [cellView setImageView:newImageView];
            [cellView addSubview:newImageView];
            
            return cellView;
            
        }
        
        NSImage *tempImg = [curSlice.sliceContents objectAtIndex:[identifierNumber intValue]-1];
        
        
        cellView.imageView.objectValue = tempImg;
        
        return cellView;
    }
    
    
}
/*
 - (id) tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
 
 Slice *curSlice = [_tableArray objectAtIndex:row];
 NSString *identifier = [tableColumn identifier];
 
 if ([identifier isEqualToString:@"thisSliceNumber"]){
 NSString *number = [NSString stringWithFormat:@"%@",[curSlice valueForKey:identifier]];
 NSString *thisCellString = @"Slice ";
 thisCellString =[thisCellString stringByAppendingString:number];
 return thisCellString;
 }
 else{
 NSString *identifierNumber = [identifier stringByReplacingOccurrencesOfString:@"Frame" withString:@""];
 
 NSTableCellView *cellView = [tableView makeViewWithIdentifier:identifier owner:self];
 
 if (cellView == nil){
 cellView = [[NSTableCellView alloc] initWithFrame:NSMakeRect(0, 0, 30, 30)];
 }
 
 //NSTableCellView *newcell = [[NSTableCellView alloc] initWithFrame:NSMakeRect(0, 0, 30, 30)];
 NSImage *tempImg = [curSlice.sliceContents objectAtIndex:[identifierNumber intValue]-1];
 
 
 //cellView.imageView.objectValue = tempImg;
 
 cellView.textField = [[NSTextField alloc] init];
 cellView.textField.stringValue = @"t";
 
 return cellView;
 }
 
 }
 */
- (void) setCellsState: (int) frameNumber sliceNumber: (int) sliceNumber cellsState: (CellState*) cellState{
    
    Slice *curSlice = [_tableArray objectAtIndex:sliceNumber];
    [curSlice setObject:frameNumber cellState:cellState];
    
}

- (void)windowDidLoad {
    [super windowDidLoad];
    
    _table.rowHeight = cellSize;
    _table.allowsColumnReordering = NO;
    _table.allowsColumnResizing = NO;
    _table.allowsColumnSelection = YES;
    _table.allowsEmptySelection = NO;
    _table.allowsMultipleSelection = NO;
    _table.allowsTypeSelect = NO;

    
}

- (void) tableViewSelectionIsChanging:(NSNotification *)notification{
    
    NSUInteger slice = [[notification object] selectedRow];
    NSUInteger frame = [[notification object] selectedColumn] - 1;
    
    if (slice < 10000){
        [operationHandler changeSlice:slice];
    }
    if (frame < 10000){
        [operationHandler changeFrame:frame];
    }
    
    
}

- (void) windowWillClose:(NSNotification *)notification{
    windowVisible = NO;
}


@end