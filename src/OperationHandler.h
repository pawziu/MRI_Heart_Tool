#import "ROIHandler.h"
#import "ITKHandler.h"


@interface OperationHandler : NSObject{
    
    ViewerController *viewerController;
    ViewerController *viewerControllerSeg;
    ROIHandler *roiHandler;
    MathHandler *mathHandler;
    ITKHandler *itkHandler;
    
    NSWindow *mainWindow;
    
    NSTextField *LVparams;
    NSTextField *LVBSAparams;
    NSTextField *RVparams;
    NSTextField *RVBSAparams;
    
    NSTextField *ED;
    NSTextField *ES;
    NSTextField *BSAField;
    
    NSTextField *BasalFrom;
    NSTextField *BasalTo;
    NSTextField *MidCavityFrom;
    NSTextField *MidCavityTo;
    NSTextField *ApicalFrom;
    NSTextField *ApicalTo;
    
    //variables
    NSArray *slicesNormalVectors;
    NSArray *sliceOrientation;
    NSArray *sliceThickenss;
    NSArray *areaTableLVENDO;
    NSArray *areaTableLVEPI;
    NSArray *areaTableRVENDO;
    NSArray *areaTableRVEPI;
    NSArray *volumeTableLVENDO;
    NSArray *volumeTableLVEPI;
    NSArray *volumeTableRVENDO;
    NSArray *volumeTableRVEPI;
    
    //used for plot
    double maxSectorMean;
    double *sectorNewArray;
    int numberOfSectors;
    
    //semi-auto seg
    NSPoint segStartPoint;
    
    //multiplier
    NSTextField *SegmentationMultiplier;
    NSTextField *SegmentationFirstSlice;
    
    NSTextField *slicesFrom;
    NSTextField *slicesTo;
    NSTextField *framesFrom;
    NSTextField *framesTo;
    
    NSTextField *resultROIName;
    NSTextField *groundTruthROIName;
    
    NSButton *regionGrowingOptimalization;
    NSButton *edgeDetectionEnable;
    NSButton *convexHullEnable;
    NSButton *finalOptimalizationEnable;
    

}

@property (nonatomic, strong) ViewerController *viewerController;
@property (nonatomic, strong) ViewerController *viewerControllerSeg;
@property (nonatomic) ROIHandler *roiHandler;
@property (nonatomic) MathHandler *mathHandler;
@property (nonatomic) ITKHandler *itkHandler;
@property (nonatomic, strong) NSWindow *mainWindow;

@property (nonatomic, strong) NSTextField *LVparams;
@property (nonatomic, strong) NSTextField *LVBSAparams;
@property (nonatomic, strong) NSTextField *RVparams;
@property (nonatomic, strong) NSTextField *RVBSAparams;
@property (nonatomic, strong) NSTextField *BSAField;

@property (nonatomic, strong) NSTextField *ED;
@property (nonatomic, strong) NSTextField *ES;
@property (nonatomic, strong) NSTextField *BasalFrom;
@property (nonatomic, strong) NSTextField *BasalTo;
@property (nonatomic, strong) NSTextField *MidCavityFrom;
@property (nonatomic, strong) NSTextField *MidCavityTo;
@property (nonatomic, strong) NSTextField *ApicalFrom;
@property (nonatomic, strong) NSTextField *ApicalTo;

@property (nonatomic, assign) double *sectorNewArray;
@property (nonatomic, assign) double maxSectorMean;
@property (nonatomic, assign) int numberOfSectors;

@property (nonatomic, assign) NSPoint segStartPoint;
@property (nonatomic, strong) NSTextField *SegmentationMultiplier;
@property (nonatomic, strong) NSTextField *SegmentationFirstSlice;

@property (nonatomic, strong) NSTextField *slicesFrom;
@property (nonatomic, strong) NSTextField *slicesTo;
@property (nonatomic, strong) NSTextField *framesFrom;
@property (nonatomic, strong) NSTextField *framesTo;

@property (nonatomic, strong) NSTextField *resultROIName;
@property (nonatomic, strong) NSTextField *groundTruthROIName;

@property (nonatomic, strong) NSButton *regionGrowingOptimalization;
@property (nonatomic, strong) NSButton *edgeDetectionEnable;
@property (nonatomic, strong) NSButton *convexHullEnable;
@property (nonatomic, strong) NSButton *finalOptimalizationEnable;

/** Additional init for this class */
- (void) additionalInit;

//////////////////Functions created by Konrad Werys/////////////////////////

/** Function to choose Closed Polygon ROI Type and name created ROI*/
- (void) drawROI:(id) sender;

/** Automatic set of ES and ED values */
- (void) autoSetESED;

/** Calculate Body Surface Area */
- (float) calcBSA;

/** Calculate distance between two slices */
- (double) calcDistanceBetweenSlices:(int) islice0 and: (int) islice1;
/** Calculate Volumes */
- (NSMutableArray *)calcVolumesNew:(NSString*) RoiName;
/** Function to get ROI Area */
- (NSMutableArray *)getAreaArrayFor:(NSString *) RoiName;

/** Main Volumes calculation functions*/
-(void) calcVolumes;
////////////////////////////////////////////////////////////////////////////

/** Init Function with Viewer Conroller assignment */
- (id) initWithViewerController: (ViewerController*) pointer vcs:(ViewerController*) pointer2;

/** Counts number of slices in examination */
- (int) getSlicesCount;
/** Counts number of time frames in examination */
- (int) getFramesCount;

/** Automatically count and set slices names */
- (void) autoSetSegmentationLevels;

//////////////////////////AHA SEGMENTS/////////////////////////////
/** Create masks for ROIs */
-(void) createMasksForPolygons;
/** Create masks for myocardium */
-(void) createMYOMasks;

/** Updated function for sectors */
-(void) createSectorsNew;

/** Main function for creating masks */
-(void) createMasks;
/** Creates sectors from masks created earlier */
-(void) createSectorsFromMask;
/** Sets point for AHA segments  */
-(void) setPointForSegments;
///////////////////////////////////////////////////////

//////////////////////////CONTOUR APPROXIMATION/////////////////////////////
/** Approximation aux function */
-(void) approximate: (NSComboBox*) comboBox copyButton: (NSButton*) copyButton;
/** Main function for approximation */
-(void) approximationMainFunction: (int) curSlice whereIsROI:(NSMutableArray*) whereIsROI roiNameNumber: (int) curROINameNumber directionsNumber: (int) directions;
/** Approximates between two ROIS */
-(void) approximateBetweenTwoROIs: (ROIClass*) ROI1 secondROI: (ROIClass*) ROI2 directionsNumber: (int) directions;
/** Aux function for copying ROI to all other frames*/
- (void) copyROIToEveryOtherFrame: (ROIClass*) roi;

//////////////////////////PLOT FUNCTIONS/////////////////////////////
/** Calculates max mean value of rois representing AHA segments */
- (double) calculateMaxSectorMean;
/** Fills array for plot drawing */
- (void) fillSectorMeanArray;
/** Fills variables needed for plot creation*/
- (void) fillPlotVariables;

//////////////////////////AUX FUNCTIONS/////////////////////////////
/** Delete all rois with type Point */
- (void) deletePointROIs;
/** Delete all rois with type Brush */
- (void) deleteAllBrushROIs;
/** Delete all rois with type Closed Polygon */
- (void) deleteAlltcPolygonROIs;
/** Delete all rois created by approximation function */
- (void) deleteApproximationROIs;
/** Delete all rois with specific name */
- (void) deleteAllROIsWithName: (NSString*) string;

/** Change Slice in Viewer Controller */
- (void) changeSlice: (int) slice;
/** Change Time Frame in Viewer Controller */
- (void) changeFrame: (int) frame;

/** Import ROI Data from Qmass Format */
- (void) importQmass;
/** Export ROI Data to Qmass Format */
- (void) exportQmass;
/** Import ROI Data from LV Segmentation Challenge */
- (void) importTextROI;
/** Creates ROIs from spectific file */
- (void) createROIsFromURL: (NSString*) filePath;
/** Read Qmass file*/
- (NSMutableArray*) checkQmassForSliceContents: (NSString*) dataString;
/** Create Array for Qmass export */
- (NSArray*) exportCreateArrayOfData;

////////////////////SEMI AUTO SEGMENTATION/////////////////////////

/** Region Growing with specific multiplier value*/
- (void) semiAutoSeg: (double) multiplier;
/** Sets starting point for region growing */
- (void) setSegPoint;
/** Begins morphological closing operation */
- (void) morphoClose;
/** Begins hole filling operation */
- (void) holeFilling;
/** Creates brush rois from ViewerControllerSeg */
- (void) createBrushROIFromITKImages:(int) mode;
/** Begins Convex Hull calculation */
- (void) performConvexHull;
/** Main function for Semi-automatic segmentation */
- (void) fullSemiAuto;
/** Main function for edge detection algorithm*/
- (void) shapeDetectionLevelSetFilter;
/** Begins final optimalization method*/
- (void) finalOptimalization;
/** Counts all Label Overlap Measures for created ROIs */
- (void) countDice;

@end
