#import <Foundation/Foundation.h>
#import <math.h>

@interface MathHandler : NSObject

//////////////////Functions created by Konrad Werys/////////////////////////
-(double) dotProduct:(double[3]) v1 and:(double[3]) v2;
-(void) crossProduct:(double[3]) v1 and:(double[3]) v2 result:(double[3]) vR;
-(void) normalize:(double[3]) v1 result:(double[3]) vR;

////////////////////////////////////////////////////////////////////////////
/** Calculates Angle between given points*/
-(double) calculateAngleFromPoints:(double) edgePointX edgePointY:(double) edgePointY centerPointX:(double) centerPointX centerPointY:(double) centerPointY pointOfInterestX: (double) pointOfInterestX pointOfInterestY:(double) pointOfInterestY;
/** Calculates distance between two points*/
-(double) distanceBetweenTwoPoints: (NSPoint*) firstPoint second: (NSPoint*) secondPoint;
/** Checks if two points lay on the same line*/
-(BOOL) doPointsLayOnTheSameLine: (NSPoint*) firstPoint second: (NSPoint*) secondPoint third: (NSPoint*) thirdPoint withPrecisionInLine: (double) precisionFirst withOverallPrecision: (double) precisionSecond;
/** Calculates new point in specific direction*/
-(int)determinePointInDirection: (NSPoint*) centerPoint edge:(NSPoint*) edgePoint firstNewPoint: (NSPoint*) firstPoint secondNewPoint: (NSPoint*) secondPoint;
/** Checks if point lays on a line between two points*/
-(BOOL)checkIfPointLaysBetweenTwoPoints:(NSPoint*) firstPoint second: (NSPoint*) secondPoint middlePoint: (NSPoint*) middlePoint;

/** Creates Closed Polygon ROI from array of points*/
-(NSMutableArray*) calculatePointsbetweenEdgeAndNewPoint: (NSPoint*) edgePoint newPoint: (NSPoint*) newPoint numberOfPoints: (int) numberOfPoints;

/** Simple pitagoras equation*/
-(double) pitagoras:(double)a_x a_Y:(double)a_y b_X:(double)b_x b_Y:(double)b_y;

/** Checks side of specific point*/
-(float) isLeft: (NSPoint) P0 point2: (NSPoint) P1 point3: (NSPoint) P2;

@end
