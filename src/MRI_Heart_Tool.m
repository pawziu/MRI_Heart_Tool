#import "MRI_Heart_Tool.h"


@implementation MRI_Heart_Tool

@synthesize mainWindow;

- (void) initPlugin
{
}


- (long) filterImage:(NSString*) menuName
{
    
    // Initialize Main Window Controller
    mainWindow = [[MainWindowController alloc] initWithWindowNibName:@"MRHeartPanel"];
    [mainWindow showWindow:self];
    
    // Assign Viewer Controller to Main Window Controller
    mainWindow.viewerController = viewerController;
    mainWindow.viewerControllerSeg = [self duplicateCurrent2DViewerWindow];
    
    [mainWindow additionalInit];
    
    //Set ROI Tool Type to Closed Polygon
    [viewerController setROIToolTag:tCPolygon];
    
    return 0;
    
}

@end
