

#import "OperationHandler.h"
#import "TableController.h"
#import "MFPController.h"
#import "OptionWindowController.h"

@interface MainWindowController : NSWindowController {
    
    // ViewerController of OsiriX for passing it to model's objects
    ViewerController *viewerController;
    ViewerController *viewerControllerSeg;
    OperationHandler *operationHandler;
    TableController *table;
    MFPController *plot;
    OptionWindowController *options;
    
    
    
    //textFields - multiline labels
    IBOutlet NSTextField *LVparams;
    IBOutlet NSTextField *LVBSAparams;
    IBOutlet NSTextField *RVparams;
    IBOutlet NSTextField *RVBSAparams;

    IBOutlet NSTextField *ED;
    IBOutlet NSTextField *ES;
    IBOutlet NSTextField *BSAField;
    
    IBOutlet NSTextField *BasalFrom;
    IBOutlet NSTextField *BasalTo;
    IBOutlet NSTextField *MidCavityFrom;
    IBOutlet NSTextField *MidCavityTo;
    IBOutlet NSTextField *ApicalFrom;
    IBOutlet NSTextField *ApicalTo;
    
    IBOutlet NSTextField *SegmentationMultiplier;
    IBOutlet NSTextField *SegmentationFirstSlice;
    
    IBOutlet NSComboBox *approximationAlgorythm;
    IBOutlet NSButton *approximationCopy;
    NSButton *setSEGPoint;
}

@property (nonatomic, strong) ViewerController *viewerController;
@property (nonatomic, strong) ViewerController *viewerControllerSeg;
@property (nonatomic) OperationHandler *operationHandler;


- (void) additionalInit;


@end
