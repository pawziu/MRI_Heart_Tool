#import <Cocoa/Cocoa.h>
#import "Slice.h"
#import "OperationHandler.h"



@interface TableController : NSWindowController <NSTableViewDataSource, NSTableViewDelegate, NSWindowDelegate> {
@private
    
    IBOutlet NSTableView *_table;
    NSMutableArray *_tableArray;
    double cellSize;
    OperationHandler *operationHandler;
    BOOL windowVisible;
}

@property (nonatomic) IBOutlet NSTableView *_table;
@property (nonatomic, strong) OperationHandler *operationHandler;
@property (nonatomic, assign) BOOL windowVisible;

@property (nonatomic) double cellSize;

- (void) addRow: (int) framesNumber numberOfThisSlice: (int) sliceNumber;
- (void) addColumn: (NSString*) identifier;

- (void) setCellsState: (int) frameNumber sliceNumber: (int) sliceNumber cellsState: (CellState*) cellState;

@end
