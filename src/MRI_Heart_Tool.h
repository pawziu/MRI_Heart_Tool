#import <Foundation/Foundation.h>
#import <OsiriXAPI/PluginFilter.h>

#import "MainWindowController.h"

@interface MRI_Heart_Tool : PluginFilter {
    
    MainWindowController *mainWindow;

}

@property (nonatomic,strong) MainWindowController *mainWindow;

/** Init function */
- (long) filterImage:(NSString*) menuName;

@end
