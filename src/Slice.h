#import <Foundation/Foundation.h>
#import "CellState.h"
//@import AppKit;
#import "AppKit/AppKit.h"


@interface Slice : NSObject{
@private
    int framesNumber;
    int thisSliceNumber;
    NSMutableArray *sliceContents;
    double imageSize;
}

@property int framesNumber;
@property int thisSliceNumber;
@property NSMutableArray *sliceContents;
@property double imageSize;

/** Init with specific frames number and slices number*/
- (id) initWithFramesNumber: (int) frames numberOfSlice: (int) sliceNumber;

/** Sets object for table creation*/
- (void) setObject: (int) position cellState: (CellState*) cellState;

@end
