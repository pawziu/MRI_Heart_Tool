#import "MathHandler.h"

@implementation MathHandler

-(double) dotProduct:(double[3]) v1 and:(double[3]) v2 {
    return  (v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]);
}

-(void) crossProduct:(double[3]) v1 and:(double[3]) v2 result:(double[3]) vR {
    vR[0] =   ( (v1[1] * v2[2]) - (v1[2] * v2[1]) );
    vR[1] = - ( (v1[0] * v2[2]) - (v1[2] * v2[0]) );
    vR[2] =   ( (v1[0] * v2[1]) - (v1[1] * v2[0]) );
}

-(void) normalize:(double[3]) v1 result:(double[3]) vR {
    double fMag = sqrt( pow(v1[0], 2) + pow(v1[1], 2) + pow(v1[2], 2));
    vR[0] = v1[0] / fMag;
    vR[1] = v1[1] / fMag;
    vR[2] = v1[2] / fMag;
}

-(double)pitagoras:(double)a_x a_Y:(double)a_y b_X:(double)b_x b_Y:(double)b_y;{
    return sqrt ( (a_x - b_x)*(a_x - b_x) + (a_y - b_y)*(a_y - b_y));
}

-(double) calculateAngleFromPoints:(double) edgePointX edgePointY:(double) edgePointY centerPointX:(double) centerPointX centerPointY:(double) centerPointY pointOfInterestX: (double) pointOfInterestX pointOfInterestY:(double) pointOfInterestY{
    
    double pitagorasA = [self pitagoras:edgePointX a_Y:edgePointY b_X:centerPointX b_Y:centerPointY];
    double pitagorasB = [self pitagoras:pointOfInterestX a_Y:pointOfInterestY b_X:centerPointX b_Y:centerPointY];
    double pitagorasC = [self pitagoras:edgePointY a_Y:edgePointY b_X:pointOfInterestX b_Y:pointOfInterestY];
    
    double divisioncheck = (2*pitagorasA*pitagorasB);
    if (divisioncheck == 0 ){
        divisioncheck = 0.000001;
    }
    
    double testValue = acos ( ((pitagorasA*pitagorasA) + (pitagorasB*pitagorasB) - (pitagorasC*pitagorasC)) / divisioncheck );
    return testValue;
}

-(NSMutableArray*) calculatePointsbetweenEdgeAndNewPoint: (NSPoint*) edgePoint newPoint: (NSPoint*) newPoint numberOfPoints: (int) numberOfPoints{
    
    double minDifferenceOfX = 2;
    double minDifferenceOfY = 2;
    
    NSMutableArray *outputArray = [[NSMutableArray alloc] init];
    
    double workingPitagorasPointX = edgePoint->x;
    double workingPitagorasPointY = newPoint->y;
    
    double workingPitagorasA = [self pitagoras:edgePoint->x a_Y:edgePoint->y b_X:workingPitagorasPointX b_Y:workingPitagorasPointY];
    double workingPitagorasB = [self pitagoras:newPoint->x a_Y:newPoint->y b_X:workingPitagorasPointX b_Y:workingPitagorasPointY];
    double workingPitagorasC = [self pitagoras:edgePoint->x a_Y:edgePoint->y b_X:newPoint->x b_Y:newPoint->y];
    
    //double workingPitagorasA = sqrt ( (edgePoint->x - workingPitagorasPointX)*(edgePoint->x - workingPitagorasPointX) + (edgePoint->y-workingPitagorasPointY)*(edgePoint->y-workingPitagorasPointY) );
    //double workingPitagorasB = sqrt ( (newPoint->x - workingPitagorasPointX)*(newPoint->x - workingPitagorasPointX) + (newPoint->y-workingPitagorasPointY)*(newPoint->y-workingPitagorasPointY) );
    //double workingPitagorasC = sqrt ( (edgePoint->x - newPoint->x)*(edgePoint->x - newPoint->x) + (edgePoint->y-newPoint->y)*(edgePoint->y-newPoint->y) );
    
    double differenceInCLenght = workingPitagorasC / (numberOfPoints+1);
    
    double sinAlpha = workingPitagorasA / workingPitagorasC;
    double cosAlpha = workingPitagorasB / workingPitagorasC;
    
    
    
    for (int i = 0; i < numberOfPoints; i++){
        
        double newCLenght;

        newCLenght = workingPitagorasC - (differenceInCLenght*(i+1));

        double newALenght = sinAlpha * newCLenght;
        double newBLenght = 0;
        newBLenght = cosAlpha * newCLenght;
        
        
        NSPoint approximatedPoint = NSMakePoint(0, 0);
        
        double difference = edgePoint->x - newPoint->x;
        if ((edgePoint->x - newPoint->x) > 0-minDifferenceOfX && difference < (edgePoint->x - newPoint->x)){
            
            approximatedPoint.x = edgePoint->x;
            
            if (edgePoint->y > newPoint->y){
                double newLength = (edgePoint->y - newPoint->y) / (numberOfPoints+1);
                approximatedPoint.y = newPoint->y + (newLength*(i+1));
            }
            else{
                double newLength = (newPoint->y - edgePoint->y) / (numberOfPoints+1);
                approximatedPoint.y = newPoint->y - (newLength*(i+1));
            }
            
        }

        else if ((edgePoint->y-newPoint->y) > 0-minDifferenceOfY && (edgePoint->y-newPoint->y) < minDifferenceOfY){
            
            approximatedPoint.y = edgePoint->y;
            
            if (edgePoint->x > newPoint->x){
                double newLength = (edgePoint->x - newPoint->x) / (numberOfPoints+1);
                approximatedPoint.x = newPoint->x + (newLength*(i+1));
            }
            else{
                double newLength = (newPoint->x - edgePoint->x) / (numberOfPoints+1);
                approximatedPoint.x = newPoint->x - (newLength*(i+1));
            }
            
        }
        
        else{
            
            //przypadek 1
            if ( edgePoint->x > newPoint->x && edgePoint->y > newPoint->y){
                approximatedPoint.x = edgePoint->x-newBLenght;
                approximatedPoint.y = edgePoint->y-newALenght;
            }
            
            //przypadek 2
            else if (edgePoint->x < newPoint->x && edgePoint->y > newPoint->y){
                approximatedPoint.x = edgePoint->x+newBLenght;
                approximatedPoint.y = edgePoint->y-newALenght;
            }
            
            //przypadek 3
            else if (edgePoint->x > newPoint->x && edgePoint->y < newPoint->y){
                approximatedPoint.x = edgePoint->x-newBLenght;
                approximatedPoint.y = edgePoint->y+newALenght;
            }
            
            //przypadek 4
            else if (edgePoint->x < newPoint->x && edgePoint->y < newPoint->y){
                approximatedPoint.x = edgePoint->x+newBLenght;
                approximatedPoint.y = edgePoint->y+newALenght;
                
                
                NSLog(@"");
            }
        }
       
        //NSPoint *new = &approximatedPoint;
        [outputArray addObject:[NSValue valueWithPoint:approximatedPoint]];
        
    }
    
    
    return outputArray;
}

-(double) distanceBetweenTwoPoints: (NSPoint*) firstPoint second: (NSPoint*) secondPoint{
    
    double difference = firstPoint->x - secondPoint->x;
    
    if (difference < 1 && difference > -1){
        double result = sqrt((secondPoint->y - firstPoint->y)*(secondPoint->y - firstPoint->y));
        return result;
    }
    
    difference = firstPoint->y - secondPoint->y;
    
    if (difference < 1 && difference > -1){
        double result = sqrt((secondPoint->x - firstPoint->x)*(secondPoint->x - firstPoint->x));
        return result;
    }
    
    double result = sqrt ( ((secondPoint->x - firstPoint->x)*(secondPoint->x - firstPoint->x)) + ((secondPoint->y - firstPoint->y)*(secondPoint->y - firstPoint->y)));
    return result;
    
}
-(BOOL) doPointsLayOnTheSameLine: (NSPoint*) firstPoint second: (NSPoint*) secondPoint third: (NSPoint*) thirdPoint withPrecisionInLine: (double) precisionFirst withOverallPrecision: (double) precisionSecond{
    
    
    double substractCheckOne = firstPoint->x - secondPoint->x;
    double substractCheckTwo = firstPoint->x - thirdPoint->x;
    
    if (substractCheckOne > 0-precisionFirst && substractCheckOne < precisionFirst && substractCheckTwo > 0-precisionFirst && substractCheckTwo < precisionFirst){
        return YES;
    }
    
    substractCheckOne = firstPoint->y - secondPoint->y;
    substractCheckTwo = firstPoint->y - thirdPoint->y;
    
    if (substractCheckOne > 0-precisionFirst && substractCheckOne < precisionFirst && substractCheckTwo > 0-precisionFirst && substractCheckTwo < precisionFirst){
        return YES;
    }
    
    double comparisonFirst = (secondPoint->y - firstPoint->y) * (thirdPoint->x - secondPoint->x);
    double comparisonSecond = (thirdPoint->y - secondPoint->y) * ( secondPoint->x - firstPoint->x);
    
    double difference = comparisonFirst - comparisonSecond;
    
    if (difference < precisionSecond && difference > 0-precisionSecond){
        return YES;
    }
    else{
        return NO;
    }
    
}

-(int)determinePointInDirection: (NSPoint*) centerPoint edge:(NSPoint*) edgePoint firstNewPoint: (NSPoint*) firstPoint secondNewPoint: (NSPoint*) secondPoint{
    
    double difference = centerPoint->x - edgePoint->x;
    
    if (difference < 1 && difference > -1){
        if (edgePoint->y > centerPoint->y){
            if (firstPoint->y > edgePoint->y){
                return 1;
            }
            else{
                return 2;
            }
        }
        else{
            if (firstPoint->y < edgePoint->y){
                return 1;
            }
            else{
                return 2;
            }
        }
    }
    
    difference = centerPoint->y - edgePoint->y;
    
    if (difference < 1 && difference > -1){
        if (edgePoint->x > centerPoint->x){
            if (firstPoint->x > edgePoint->x){
                return 1;
            }
            else{
                return 2;
            }
        }
        else{
            if (firstPoint->x < edgePoint->x){
                return 1;
            }
            else{
                return 2;
            }
        }
    }
    
    if ( edgePoint->x > centerPoint->x){
        
        if (edgePoint->y > centerPoint->y){
            if (firstPoint->x > edgePoint->x && firstPoint->y > edgePoint->y){
                return 1;
            }
            else{
                return 2;
            }
        }
        else{
            if (firstPoint->x > edgePoint->x && firstPoint->y < edgePoint->y){
                return 1;
            }
            else{
                return 2;
            }
            
        }
        
    }
    else{
        
        if (edgePoint->y < centerPoint->y){
            if (firstPoint->x < edgePoint->x && firstPoint->y < edgePoint->y){
                return 1;
            }
            else{
                return 2;
            }
        }
        else{
            if (firstPoint->x < edgePoint->x && firstPoint->y > edgePoint->y){
                return 1;
            }
            else{
                return 2;
            }
            
        }
        
        
    }
    
    
    
}

-(BOOL)checkIfPointLaysBetweenTwoPoints:(NSPoint*) firstPoint second: (NSPoint*) secondPoint middlePoint: (NSPoint*) middlePoint{
    
    double overalLength = [self distanceBetweenTwoPoints:firstPoint second:secondPoint];
    double firstToMiddle = [self distanceBetweenTwoPoints:firstPoint second:middlePoint];
    double secondToMiddle = [self distanceBetweenTwoPoints:secondPoint second:middlePoint];
    
    double difference = overalLength - firstToMiddle - secondToMiddle;
    
    if (difference > -0.1 && difference < 0.1){
        return YES;
    }
    else{
        return NO;
    }
}

-(float) isLeft: (NSPoint) P0 point2: (NSPoint) P1 point3: (NSPoint) P2{
    return (P1.x - P0.x)*(P2.y - P0.y) - (P2.x - P0.x)*(P1.y - P0.y);
}

@end
