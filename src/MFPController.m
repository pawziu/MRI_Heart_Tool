
#import "MFPController.h"

@interface MFPController ()

- (void) createGraph;
- (void) setPlotPoints: (double) inputArray currentPosition: (int) position;

@end


@implementation MFPController

@synthesize plotData;
@synthesize graph ;
@synthesize plotView;
@synthesize numberOfTimeFrames;
@synthesize numberOfSectors;
@synthesize maxValue;
@synthesize plotPoints;
@synthesize windowVisible;

- (void) awakeFromNib{
    
    plotPoints=(double *)malloc(25*6*sizeof(double));
    
}

- (void)windowDidLoad {
    
    [super windowDidLoad];
    [self createGraph];
    windowVisible = YES;
    

}

- (void) updateGraph{
    [self createGraph];
}

#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return numberOfTimeFrames;
}

-(id)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{

    
    switch (fieldEnum)
    {
            
        case CPTScatterPlotFieldX:
        {
            // inverse numbers, so first (latest) test run is on the right.
            
            int x = numberOfTimeFrames - index;
            return [NSDecimalNumber numberWithInt:x];
        }
        case CPTScatterPlotFieldY:
        {
            for (int i = 1; i <= numberOfSectors ; i++){
                NSString *sectorName = [NSString stringWithFormat:@"SECTOR %i", i];
                if ([plot.identifier isEqual:sectorName]){
                    
                    int x = numberOfTimeFrames - index - 1;
                    
                    
                    double point = 0;
                    
                    point  = plotPoints[(i-1)*numberOfTimeFrames+x];
                    
                    return [NSNumber numberWithDouble:point];
                }
            }
        }
    }
    return nil;
    
    
    
    
}

- (void) setPlotPoints: (double) inputArray currentPosition: (int) position{
    
    plotPoints[position] = inputArray;
}

- (void) createGraph{
    
    // Create graph from theme
    CPTXYGraph *newGraph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme      = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [newGraph applyTheme:theme];
    self.graph = newGraph;
    
    self.plotView.hostedGraph = newGraph;
    
    double yInterval = maxValue/10;
    
    // Setup scatter plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)newGraph.defaultPlotSpace;
    
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:[NSNumber numberWithDouble:-2] length:[NSNumber numberWithDouble:(numberOfTimeFrames+3)]];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:[NSNumber numberWithDouble:(0 - yInterval)] length:[NSNumber numberWithDouble:(maxValue+80)]];
    
    // Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)newGraph.axisSet;
    CPTXYAxis *x          = axisSet.xAxis;
    x.majorIntervalLength         = [NSNumber numberWithInt:1];
    x.minorTicksPerInterval       = 0;
    x.title = @"Frame Number";
    //x.labelOffset = 100;
    x.titleOffset = -30;
    NSNumberFormatter *xFormatter = [[NSNumberFormatter alloc] init];
    [xFormatter setMaximumFractionDigits:0];
    x.labelFormatter = xFormatter;
    //x.orthogonalPosition = [NSNumber numberWithInt:-2];

    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength         = [NSNumber numberWithDouble:yInterval];
    y.minorTicksPerInterval       = 1;
    y.title = @"ROI Mean Value";
    y.titleOffset = 50;
    y.orthogonalPosition = [NSNumber numberWithInt:1];
    
    // Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth = 0.75;
    majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:0.2] colorWithAlphaComponent:0.75];
    
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth = 0.25;
    minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    
    x.majorGridLineStyle    = majorGridLineStyle;
    x.minorGridLineStyle    = minorGridLineStyle;
    
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = minorGridLineStyle;
    
    
    CPTColor *colorArray[6] = {[CPTColor redColor], [CPTColor blueColor], [CPTColor greenColor], [CPTColor yellowColor], [CPTColor magentaColor], [CPTColor whiteColor] };
    
    for (int j = 1; j <= numberOfSectors; j++){
        
        // Create a plot that uses the data source method
        CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
        NSString *sectorName = [NSString stringWithFormat:@"SECTOR %i", j];
        dataSourceLinePlot.identifier = sectorName;
        
        dataSourceLinePlot.interpolation = CPTScatterPlotInterpolationCurved;
        
        CPTMutableLineStyle *lineStyle = [dataSourceLinePlot.dataLineStyle mutableCopy];
        lineStyle.lineWidth              = 1.0;
        lineStyle.lineColor              = colorArray[j-1];
        dataSourceLinePlot.dataLineStyle = lineStyle;
        
        dataSourceLinePlot.dataSource = self;
        
        [newGraph addPlot:dataSourceLinePlot];

    }
    
    graph.legend = [CPTLegend legendWithGraph:graph];
    graph.legend.fill = [CPTFill fillWithColor:[CPTColor darkGrayColor]];
    graph.legend.opacity = 0.8;
    graph.legend.cornerRadius = 5.0;
    graph.legend.swatchSize = CGSizeMake(25.0, 25.0);
    graph.legendAnchor = CPTRectAnchorTopRight;
    //graph.legendDisplacement = CGPointMake(0.0, 12.0);
    
}

- (void) windowWillClose:(NSNotification *)notification{
    windowVisible = NO;
}


@end
